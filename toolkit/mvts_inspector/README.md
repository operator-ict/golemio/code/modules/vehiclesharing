# Vehiclesharing Toolkit - Mapbox vector tile inspector

> A tool for inspecting Mapbox vector tile response from Golemio API and dumping them to a file as JSON

## Requirements

-   [Python 3](https://www.python.org/downloads/)
-   [pip](https://pip.pypa.io/en/stable/installation/)

## Installation

```bash
pip install -r requirements.txt
```

## Usage

```bash
# mvts_inspector.py <server_url> <zoom> <tileX> <tileY>
python3 mvts_inspector.py https://api.golemio.cz 13 4424 2774
```
