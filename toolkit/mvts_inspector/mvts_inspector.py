# Usage:   mvts_inspector.py <server_url> <zoom> <tileX> <tileY>
# Example: mvts_inspector.py https://api.golemio.cz 13 4424 2774

import sys
import json
import datetime
import mapbox_vector_tile
import urllib.request as urllib

# debug on example data
# server_url = "http://localhost:3004"
# zoom = "13"
# tileX = "4424"
# tileY = "2774

server_url = sys.argv[1]
zoom = sys.argv[2]
tileX = sys.argv[3]
tileY = sys.argv[4]

# Download the VTS buffer data
data = urllib.urlopen(server_url + '/v2/vehiclesharing/mvts/free_vehicle_status?zoom=' + zoom + '&tileX=' + tileX + '&tileY=' + tileY).read()

decoded_data = mapbox_vector_tile.decode(data)

# Save as a JSON file
name = 'free_bike_status_' + zoom + '_' + tileX + '_' + tileY + '_'+ datetime.datetime.now().strftime("%d-%m-%YT%H:%M:%S") + '.json'

with open(name, 'w') as outfile:
    json.dump(decoded_data, outfile, ensure_ascii=False, sort_keys=True, indent=2)

print('Written to ' + name)
