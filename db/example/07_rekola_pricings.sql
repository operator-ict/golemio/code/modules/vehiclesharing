
INSERT INTO pricing_plans (id,system_id,url,last_updated,"name",currency,price,is_taxable,description,surge_pricing,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 ('4b72f960-84cf-11ec-a8a3-0242ac120002','d727bb19-9755-40b2-9615-01fbb6180b8b','https://www.rekola.cz/cenik','2023-12-07 16:36:00+01','Jednotlivé jízdy','CZK',0.0,false,'35 Kč za 30 min -- Platíte za každou jízdu samostatně',false,NULL,'2023-06-28 22:48:24.906136+02',NULL,NULL,'2024-07-25 06:58:02.376+02',NULL);


INSERT INTO pricings (pricing_plan_id,pricing_type,pricing_order,"start",rate,"interval","end",start_time_of_period,end_time_of_period,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,id) VALUES
	 ('4b72f960-84cf-11ec-a8a3-0242ac120002','per_min_pricing',0,0,35.0,30,NULL,NULL,NULL,NULL,'2023-06-28 22:48:24.906136+02',NULL,NULL,'2024-07-25 06:58:02.385+02',NULL,'7f9a73df9ac0f331b57df75f87cdd5e5');

