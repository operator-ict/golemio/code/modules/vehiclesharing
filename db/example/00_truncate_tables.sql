TRUNCATE
    "geofencing_zones",
    "vehicle_status",
    "station_status_vehicle_type",
    "vehicle_types",
    "station_status",
    "station_information",
    "pricings",
    "pricing_plan_payment",
    "static_data";

UPDATE system_information
SET rental_app_id='bb622abc-84a1-11ec-a8a3-0242ac120002'
WHERE system_id = 'd727bb19-9755-40b2-9615-01fbb6180b8b';

DELETE FROM pricings WHERE pricing_plan_id LIKE 'nextbike-%';
DELETE FROM pricing_plans WHERE system_id IN (
   '8586cf7d-7fc9-5c3e-8805-e83481f13e3c',
   'ef63ac2e-4c71-4c28-a496-f0dc010d630b',
   'ajo',
   'car4way',
   'd727bb19-9755-40b2-9615-01fbb6180b8b'
);

DELETE FROM system_information WHERE system_id IN (
   '0fec08e8-2335-4157-a690-888131a689a6'
);

DELETE FROM system_information WHERE rental_app_id IN (
   '9f09907a-464e-5c5d-8a6c-76a2948cecd4',
   'mobility-operator-app-nextbike_tg',
   'mobility-operator-app-ajo',
   'mobility-operator-app-hoppygo',
   'ajo_rental_apps',
   'hoppygo_rental_apps',
   'car4way_rental_apps'
);

DELETE FROM "rental_apps" WHERE id IN (
   '012eb469-6d60-4ad4-9249-ccbe7a19f102',
   '3b1be791-dea5-46be-bc91-debf49b4e97a',
   'mobility-operator-app-rekola',
   '9f09907a-464e-5c5d-8a6c-76a2948cecd4',
   'mobility-operator-app-nextbike_tg',
   'mobility-operator-app-ajo',
   'mobility-operator-app-hoppygo',
   'ajo_rental_apps',
   'hoppygo_rental_apps',
   'd0bac98a-616f-5e73-b206-11e5dd1107e2',
   '17dbff1f-f98d-583d-94c6-ad76bc081437',
   '7b9e88c8-7bdb-434c-bd64-89a2bb5d03dc',
   'cc95b1ee-9360-5f41-ae12-3f226177a54f',
   'e92890e6-9c75-5a26-b6f6-79aa5d0d34de',
   '741c2b04-e076-4d2c-b129-c35ee8f177f7',
   'b058a4a9-c215-492a-9c83-673a5886cd89',
   'car4way_rental_apps',
   '9e2e9ef2-e79b-4cef-a777-b3b0f15c8304'
);
