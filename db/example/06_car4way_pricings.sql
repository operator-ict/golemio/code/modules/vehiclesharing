INSERT INTO rental_apps (id,android_store_url,android_discovery_url,ios_store_url,ios_discovery_url,web_url,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 ('car4way_rental_apps','https://play.google.com/store/apps/details?id=cz.car4way.car4way','https://car4way.cz/link/H3Ed','https://itunes.apple.com/us/app/car4way/id1043539052?l=cs&ls=1&mt=8','https://car4way.cz/link/H3Ed','https://www.car4way.cz/',NULL,'2023-06-28 22:50:12.395+02',NULL,NULL,'2024-07-23 06:58:04.385+02',NULL);


INSERT INTO system_information (operator_id,system_id,"language",logo,"name",short_name,"operator",url,purchase_url,start_date,phone_number,email,feed_contact_email,timezone,license_id,license_url,attribution_organization_name,attribution_url,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,terms_of_use_url,rental_app_id) VALUES
	 ('car4way','car4way','cs','','car4way','car4way',NULL,NULL,'https://www.car4way.cz/','2022-10-01 14:00:00+02','+420 601 311 011','info@car4way.cz','info@car4way.cz','Europe/Prague',NULL,'https://iptoict.blob.core.windows.net/storage/Legal/GBFSLicence.txt',NULL,NULL,NULL,'2023-06-28 22:50:13.868+02',NULL,NULL,'2024-07-23 06:58:05.2+02',NULL,'https://www.car4way.cz/Upload/fe_files/VP-CAR4WAY-01-10-2020.pdf','car4way_rental_apps');


INSERT INTO pricing_plans (id,system_id,url,last_updated,"name",currency,price,is_taxable,description,surge_pricing,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 ('car4way_standard','car4way','https://www.car4way.cz/carsharing/cenik','2023-12-07 17:16:00+01','Ceník Car4Way Fabia','CZK',0.0,false,'Cena za vozy Fabia a Fabia Combi',false,NULL,'2023-06-28 22:50:14.35+02',NULL,NULL,'2024-07-23 06:58:06.222+02',NULL);

INSERT INTO pricings (pricing_plan_id,pricing_type,pricing_order,"start",rate,"interval","end",start_time_of_period,end_time_of_period,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,id) VALUES
	 ('car4way_standard','per_min_pricing',0,0,8.5,1,NULL,'mon-sun 9:00','mon-sun 19:59',NULL,'2023-06-28 22:50:14.668+02',NULL,NULL,'2023-12-11 06:58:06.518+01',NULL,'e01572d1674c44de57aee0707b4fc255'),
	 ('car4way_standard','per_min_pricing',0,0,9.5,1,NULL,'mon-sun 20:00','mon-sun 5:59',NULL,'2023-06-28 22:50:14.668+02',NULL,NULL,'2023-12-11 06:58:06.518+01',NULL,'dc19a8c4a9acf83169c29620b057f954'),
	 ('car4way_standard','per_min_pricing',0,0,7.0,1,NULL,'mon-fri 6:00','mon-fri 8:59',NULL,'2023-06-28 22:50:14.668+02',NULL,NULL,'2024-07-22 06:58:01.165+02',NULL,'5a2d0cf539ba4edb18a59e108b01aad2'),
	 ('car4way_standard','per_min_pricing',0,0,8.5,1,NULL,'sat-sun 6:00','sat-sun 8:59',NULL,'2023-12-12 06:58:05.229+01',NULL,NULL,'2024-07-22 06:58:01.165+02',NULL,'5a2d0cf539ba4edb18a59e108b01aad3'),
	 ('car4way_standard','per_min_pricing',0,0,8.5,1,NULL,'mon-sun 9:00','mon-sun 19:59',NULL,'2023-12-12 06:58:05.229+01',NULL,NULL,'2024-07-22 06:58:01.165+02',NULL,'5a2d0cf539ba4edb18a59e108b01aad4'),
	 ('random_id','per_min_pricing',0,0,8.5,1,NULL,'mon-sun 9:00','mon-sun 19:59',NULL,'2023-12-12 06:58:05.229+01',NULL,NULL,'2024-07-22 06:58:01.165+02',NULL,'5a2d0cf539ba4edb18a59e108b01aad69'),
	 ('car4way_standard','per_min_pricing',0,0,10.0,1,NULL,'mon-sun 20:00','mon-sun 5:59',NULL,'2023-12-12 06:58:05.229+01',NULL,NULL,'2024-07-22 06:58:01.165+02',NULL,'5a2d0cf539ba4edb18a59e108b01aad5');

INSERT INTO vehicle_status (id,system_id,point,helmets,passengers,damage_description,description,vehicle_registration,is_reserved,is_disabled,vehicle_type_id,last_reported,current_range_meters,charge_percent,rental_app_id,station_id,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,pricing_plan_id,make,model,color,processed_at) VALUES
	 ('cesky-carsharing-5SB3202','car4way','SRID=4326;POINT (14.42198 50.036594)',NULL,NULL,NULL,'Škoda Fabia','5SB3202',false,false,'carsharing_benzin','2024-07-23 14:40:00.51+02',NULL,NULL,'car4way_rental_apps',NULL,NULL,'2024-06-05 12:02:03.514+02',NULL,NULL,'2024-07-23 14:40:01.713+02',NULL,'car4way_standard','Škoda','Fabia',NULL,'2024-07-23 14:40:00.51+02');
