create table static_data (
    "provider" varchar(30) not null,
    resource_type varchar(30) not null,
    "data" json not null,

     -- audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    constraint static_data_pk primary key ("provider", resource_type)
);
