CREATE PROCEDURE rental_apps_data_retention(INOUT numberofrows integer)
 LANGUAGE plpgsql
 SET search_path FROM CURRENT
AS $procedure$
    DECLARE
        idsForDelete varchar[];
    BEGIN
        SELECT array_agg(ra.id)
        FROM vehiclesharing.rental_apps ra
        LEFT JOIN vehiclesharing.system_information syi
            ON ra.id = syi.rental_app_id
        LEFT JOIN vehiclesharing.station_information sti
            ON ra.id = sti.rental_app_id
        LEFT JOIN vehiclesharing.vehicle_status vs
            ON ra.id = vs.rental_app_id
        WHERE syi.rental_app_id IS NULL
            AND sti.rental_app_id IS NULL
            AND vs.rental_app_id IS NULL
        INTO idsForDelete;

        DELETE FROM vehiclesharing.rental_apps ra
        WHERE ra.id = ANY(idsForDelete);

        SELECT array_length(idsForDelete, 1)
        INTO numberOfRows;
    END;
$procedure$
