ALTER TABLE pricings ADD CONSTRAINT pricings_plan_id_fkey FOREIGN KEY (pricing_plan_id) REFERENCES pricing_plans(id);
ALTER TABLE vehicle_status ADD CONSTRAINT bike_status_pricing_plan_id_fkey FOREIGN KEY (pricing_plan_id) REFERENCES pricing_plans(id);
ALTER TABLE vehicle_status ADD CONSTRAINT bike_status_vehicle_type_id_fkey FOREIGN KEY (vehicle_type_id) REFERENCES vehicle_types(id);
