DROP PROCEDURE rental_apps_data_retention;

CREATE PROCEDURE rental_apps_data_retention()
 LANGUAGE plpgsql
 SET search_path FROM CURRENT
AS $procedure$
    BEGIN
        DELETE FROM vehiclesharing.rental_apps ra
        WHERE ra.id IN (
            SELECT ra.id
            FROM vehiclesharing.rental_apps ra
            LEFT JOIN vehiclesharing.system_information syi
                ON ra.id = syi.rental_app_id
            LEFT JOIN vehiclesharing.station_information sti
                ON ra.id = sti.rental_app_id
            LEFT JOIN vehiclesharing.vehicle_status vs
                ON ra.id = vs.rental_app_id
            WHERE syi.rental_app_id IS NULL
                AND sti.rental_app_id IS NULL
                AND vs.rental_app_id IS NULL
        );
    END;
$procedure$
