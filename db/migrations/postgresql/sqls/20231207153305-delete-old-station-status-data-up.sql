ALTER TABLE station_status ADD COLUMN processed_at timestamptz NOT NULL DEFAULT 'epoch';

ALTER TABLE station_status_vehicle_type ALTER COLUMN processed_at SET DEFAULT 'epoch';
UPDATE station_status_vehicle_type SET processed_at = 'epoch' WHERE processed_at = '-infinity';

ALTER TABLE station_information ALTER COLUMN processed_at SET DEFAULT 'epoch';
UPDATE station_information SET processed_at = 'epoch' WHERE processed_at = '-infinity';

ALTER TABLE vehicle_status ALTER COLUMN processed_at SET DEFAULT 'epoch';
UPDATE vehicle_status SET processed_at = 'epoch' WHERE processed_at = '-infinity';

CREATE OR REPLACE VIEW v_station_status_with_system_id AS
    SELECT ss.*, si.system_id
    FROM station_status ss
    LEFT JOIN station_information si
        ON ss.station_id = si.id;

CREATE OR REPLACE VIEW v_station_status_vehicle_type_with_system_id AS
    SELECT ssvt.*, si.system_id
    FROM station_status_vehicle_type ssvt
    LEFT JOIN station_information si
        ON ssvt.station_id = si.id;
