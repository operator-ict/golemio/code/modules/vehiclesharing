ALTER TABLE pricings DROP CONSTRAINT pricings_pkey;
ALTER TABLE pricings ALTER COLUMN pricing_type DROP NOT NULL;
