ALTER TABLE station_information ADD COLUMN processed_at timestamptz NOT NULL DEFAULT '-infinity';
ALTER TABLE vehicle_status ADD COLUMN processed_at timestamptz NOT NULL DEFAULT '-infinity';

DROP PROCEDURE station_data_retention;

CREATE PROCEDURE station_data_retention(latestProcessedAt timestamptz, systemId varchar(50))
    LANGUAGE plpgsql
    SET search_path FROM CURRENT
AS $PROCEDURE$
    BEGIN
        DELETE FROM station_information si
        WHERE si.id IN (
            SELECT si.id
            FROM station_information si
            WHERE si.system_id = systemId
                AND si.processed_at < latestProcessedAt
        );
    END;
$PROCEDURE$
