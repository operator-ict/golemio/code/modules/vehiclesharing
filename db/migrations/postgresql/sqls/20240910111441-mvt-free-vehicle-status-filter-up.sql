DROP FUNCTION mvt_free_vehicle_status(int, int, int);

CREATE FUNCTION mvt_free_vehicle_status (
    zoom INT,
    tileX INT,
    tileY INT,
    systemId VARCHAR[] DEFAULT NULL
) RETURNS TABLE (
    mvt BYTEA
)
SET search_path FROM current
AS $$
BEGIN
RETURN QUERY
    SELECT
        ST_AsMVT(
            q, 'free_vehicle_status', 4096, 'geometry'
        ) AS mvt
    FROM
    (
        SELECT
            vs.id,
            vs.system_id,
            extract(epoch from date_trunc('second', vs.last_reported))::int as last_reported,
            vs.is_reserved,
            vs.is_disabled,
            ra.android_store_url as android_url,
            ra.ios_store_url as ios_url,
            ra.web_url,
            ST_AsMVTGeom(
                vs.point,
                ST_Transform(ST_TileEnvelope(zoom, tileX, tileY), 4326),
                4096,
                256,
                TRUE
            ) AS geometry
        FROM vehicle_status vs
        LEFT JOIN rental_apps ra ON vs.rental_app_id = ra.id
        WHERE
            ST_Intersects(
                vs.point,
                ST_Transform(ST_TileEnvelope(zoom, tileX, tileY), 4326)
            )
            AND
            (systemId IS NULL OR vs.system_id = ANY(systemId))
        GROUP BY
            vs.id,
            vs.point,
            ra.android_store_url,
            ra.ios_store_url,
            ra.web_url
    ) q;
END;
$$ LANGUAGE plpgsql;
