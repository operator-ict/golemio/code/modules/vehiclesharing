DELETE from pricings WHERE pricing_plan_id IN (
    'car4way_fabia',
    'car4way_karoq',
    'car4way_caddy',
    'car4way_octavia',
    'car4way_scala',
    'autonapul_budget'
);

DELETE from pricing_plans WHERE id IN (
   'car4way_fabia',
   'car4way_karoq',
   'car4way_caddy',
   'car4way_octavia',
   'car4way_scala',
   'autonapul_budget'
);
