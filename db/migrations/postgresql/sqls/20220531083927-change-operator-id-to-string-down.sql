UPDATE system_information SET operator_id = 3 WHERE short_name = 'BeRider';
UPDATE system_information SET operator_id = 1 WHERE name = 'Rekola';
UPDATE system_information SET operator_id = 2 WHERE short_name = 'Next Bike';

ALTER TABLE system_information ALTER COLUMN operator_id TYPE integer USING (operator_id::integer);

