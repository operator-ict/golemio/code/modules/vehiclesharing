ALTER TABLE system_information ALTER COLUMN operator_id TYPE varchar(50);
ALTER TABLE system_information ALTER COLUMN operator_id SET NOT NULL;
ALTER TABLE system_information ALTER COLUMN operator_id DROP DEFAULT;

UPDATE system_information SET operator_id = 'berider' WHERE short_name = 'BeRider';
UPDATE system_information SET operator_id = 'rekola' WHERE name = 'Rekola';
UPDATE system_information SET operator_id = 'nextbike' WHERE short_name = 'Next Bike';
