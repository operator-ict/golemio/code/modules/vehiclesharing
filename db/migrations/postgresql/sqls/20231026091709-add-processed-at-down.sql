ALTER TABLE station_information DROP COLUMN processed_at;
ALTER TABLE vehicle_status DROP COLUMN processed_at;

DROP PROCEDURE station_data_retention;

CREATE PROCEDURE station_data_retention(INOUT numberofrows integer, olderThanTimestamptz timestamptz, systemId varchar(50))
 LANGUAGE plpgsql
 SET search_path FROM CURRENT
AS $procedure$
	declare
		idsForDelete varchar[];
	begin
		select array_agg(si.id) from station_information si
		inner join station_status ss on
			si.id = ss.station_id
		where ss.last_reported < olderThanTimestamptz and si.system_id = systemId
		into idsForDelete;

		delete from station_information where id = ANY(idsForDelete);

		select array_length(idsForDelete,1) into numberOfRows;
	end;
$procedure$
