ALTER TABLE pricings DROP CONSTRAINT pricings_pkey;
ALTER TABLE bike_status ADD COLUMN pricing_plan_id character varying(50);
ALTER TABLE bike_status ADD CONSTRAINT bike_status_pricing_plan_id_fkey FOREIGN KEY (pricing_plan_id) REFERENCES pricing_plans(id);

