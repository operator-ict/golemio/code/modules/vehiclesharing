ALTER TABLE bike_status ADD COLUMN make character varying(50);
ALTER TABLE bike_status ADD COLUMN model character varying(50);
ALTER TABLE bike_status ADD COLUMN color character varying(50);
