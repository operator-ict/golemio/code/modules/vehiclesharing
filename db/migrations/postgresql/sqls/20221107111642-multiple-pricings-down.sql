ALTER TABLE pricings DROP CONSTRAINT pricings_pkey;
ALTER TABLE pricings DROP COLUMN id;
ALTER TABLE pricings ADD CONSTRAINT pricings_pkey PRIMARY KEY (pricing_plan_id, pricing_type, start);
