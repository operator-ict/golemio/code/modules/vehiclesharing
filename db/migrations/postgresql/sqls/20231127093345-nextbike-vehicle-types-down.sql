ALTER TABLE station_status_vehicle_type DROP CONSTRAINT station_status_vehicle_type_pkey;
ALTER TABLE station_status_vehicle_type ADD CONSTRAINT station_status_vehicle_type_station_id_uniq UNIQUE (station_id);
