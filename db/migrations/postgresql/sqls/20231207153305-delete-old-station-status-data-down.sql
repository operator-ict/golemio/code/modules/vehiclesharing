DROP VIEW v_station_status_vehicle_type_with_system_id;
DROP VIEW v_station_status_with_system_id;

ALTER TABLE station_status_vehicle_type ALTER COLUMN processed_at SET DEFAULT '-infinity';
UPDATE station_status_vehicle_type SET processed_at = '-infinity' WHERE processed_at = 'epoch';

ALTER TABLE station_information ALTER COLUMN processed_at SET DEFAULT '-infinity';
UPDATE station_information SET processed_at = '-infinity' WHERE processed_at = 'epoch';

ALTER TABLE vehicle_status ALTER COLUMN processed_at SET DEFAULT '-infinity';
UPDATE vehicle_status SET processed_at = '-infinity' WHERE processed_at = 'epoch';

ALTER TABLE station_status DROP COLUMN processed_at;
