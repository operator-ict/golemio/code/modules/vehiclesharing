import request from "supertest";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { mvtsRouter } from "#og/routers/MVTSRouter";
import { freeVehicleStatusMVTS } from "../fixtures/freeVehicleStatusMVTS";

chai.use(chaiAsPromised);

describe("MVTS Router", () => {
    // Create clean express instance
    const app = express();

    before(() => {
        // Mount the tested router to the express instance
        app.use(mvtsRouter.getPath(), mvtsRouter.getRouter());
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            if (err.status === 406) {
                res.status(406).send(err);
            } else {
                const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
                log.silly("Error caught by the router error handler.");
                res.setHeader("Content-Type", "application/vnd.mapbox-vector-tile");
                res.status(errObject.error_status || 500).send(errObject);
            }
        });
    });

    describe("/v2/vehiclesharing/mvts/free_vehicle_status", () => {
        it("should fail with 400 if query params not provided", (done) => {
            request(app)
                .get("/v2/vehiclesharing/mvts/free_vehicle_status")
                .set("Accept", "application/vnd.mapbox-vector-tile")
                .expect(400)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.headers["content-type"]).to.eq("application/vnd.mapbox-vector-tile; charset=utf-8");
                    expect(JSON.parse(res.text)).to.include({
                        error_message: "Bad request",
                        error_status: 400,
                    });
                    done();
                });
        });

        it("should respond with mvt buffer", (done) => {
            request(app)
                .get("/v2/vehiclesharing/mvts/free_vehicle_status?zoom=13&tileX=4424&tileY=2774")
                .set("Accept", "application/vnd.mapbox-vector-tile")
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.headers["cache-control"]).to.eq("public, s-maxage=60, stale-while-revalidate=5");
                    expect(res.headers["content-type"]).to.eq("application/vnd.mapbox-vector-tile");
                    expect(res.body).to.be.an.instanceOf(Object).and.to.be.empty;
                    expect(res.text).not.to.be.empty;
                    done();
                });
        });

        it("should respond with readable MVTS json", (done) => {
            request(app)
                .get("/v2/vehiclesharing/mvts/free_vehicle_status?zoom=13&tileX=4424&tileY=2774")
                .set("Accept", "application/json")
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.headers["cache-control"]).to.eq("public, s-maxage=60, stale-while-revalidate=5");
                    expect(res.headers["content-type"]).to.eq("application/json; charset=utf-8");
                    expect(res.body).to.have.deep.keys(freeVehicleStatusMVTS);
                    done();
                });
        });

        it("should respond with readable MVTS json with header including acceptable charset", (done) => {
            request(app)
                .get("/v2/vehiclesharing/mvts/free_vehicle_status?zoom=13&tileX=4424&tileY=2774")
                .set("Accept", "application/json;charset=utf-8")
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.headers["content-type"]).to.eq("application/json; charset=utf-8");
                    done();
                });
        });

        it("should respond with 406 on wrong accept header", (done) => {
            request(app)
                .get("/v2/vehiclesharing/mvts/free_vehicle_status?zoom=13&tileX=4424&tileY=2774")
                .set("Accept", "text/html")
                .expect(406, done);
        });
    });
});
