import sinon, { SinonSandbox } from "sinon";
import request from "supertest";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { vehiclesharingRouter } from "#og/routers/VehiclesharingRouter";

chai.use(chaiAsPromised);

describe("Vehiclesharing Router", () => {
    // Create clean express instance
    const app = express();
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        // Mount the tested router to the express instance
        app.use("/vehiclesharing", vehiclesharingRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET vehiclesharing", (done) => {
        request(app).get("/vehiclesharing").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
    });

    it("GET /vehiclesharing should only return bikes or shared mopeds", (done) => {
        request(app)
            .get("/vehiclesharing")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an.instanceOf(Object);
                expect(res.body.features).to.be.an.instanceOf(Array);
                expect(res.body.type).to.be.equal("FeatureCollection");
                expect(res.headers["cache-control"]).to.eq("public, s-maxage=60, stale-while-revalidate=5");
                for (const entry of res.body.features) {
                    expect(entry.properties.type.id).to.be.oneOf([1, 5]);
                }
                done();
            });
    });

    it("GET vehiclesharing?companyName=Rekola should only return Rekola bikes", (done) => {
        request(app)
            .get("/vehiclesharing?companyName=Rekola")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an.instanceOf(Object);
                expect(res.body.features).to.be.an.instanceOf(Array);
                expect(res.body.type).to.be.equal("FeatureCollection");
                expect(res.headers["cache-control"]).to.eq("public, s-maxage=60, stale-while-revalidate=5");
                for (const entry of res.body.features) {
                    expect(entry.properties.company.name).to.be.equal("Rekola");
                }
                done();
            });
    });

    it(
        "GET vehiclesharing?companyNames[]=Rekola&companyNames[]=Nextbike%20Praha should only return Rekola " +
            "and Nextbike bikes",
        (done) => {
            request(app)
                .get("/vehiclesharing?companyNames=Rekola&companyNames[]=Nextbike%20Praha")
                .end((err: any, res: any) => {
                    expect(res.statusCode).to.be.equal(200);
                    expect(res.body).to.be.an.instanceOf(Object);
                    expect(res.body.features).to.be.an.instanceOf(Array);
                    expect(res.body.type).to.be.equal("FeatureCollection");
                    for (const entry of res.body.features) {
                        expect(entry.properties.company.name).to.be.oneOf(["Rekola", "Nextbike Praha"]);
                    }
                    done();
                });
        }
    );

    it("GET with latlng, range and limit should return vehicles within the range", (done) => {
        request(app)
            .get("/vehiclesharing?latlng=50.1088538325,14.4610581741&range=80&limit=5")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an.instanceOf(Object);
                expect(res.body.features).to.be.an.instanceOf(Array);
                expect(res.body.type).to.be.equal("FeatureCollection");
                expect(res.body.features).to.have.length(1);
                done();
            });
    });

    it("GET vehiclesharing?updatedSince=CURRENT_DATE should not return anything", (done) => {
        request(app)
            .get("/vehiclesharing?updatedSince=" + new Date().toISOString())
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an.instanceOf(Object);
                expect(res.body.features).to.be.an.instanceOf(Array);
                expect(res.body.type).to.be.equal("FeatureCollection");
                expect(res.body.features).to.have.length(0);
                done();
            });
    });

    it("GET vehiclesharing/rekola-1926 should return one vehicle", (done) => {
        request(app)
            .get("/vehiclesharing/rekola-1926")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an.instanceOf(Object);
                expect(res.body.geometry.coordinates).to.be.an.instanceOf(Array);
                expect(res.body.type).to.be.equal("Feature");
                expect(res.body.properties.id).to.be.equal("rekola-1926");
                done();
            });
    });

    it("GET vehiclesharing/idk should response with 404", (done) => {
        request(app)
            .get("/vehiclesharing/idk")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(404);
                done();
            });
    });
});
