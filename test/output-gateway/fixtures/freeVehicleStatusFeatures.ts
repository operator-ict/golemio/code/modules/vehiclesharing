export const getFreeVehicleStatusFeatures = (ts: number) => [
    {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [14.430091381072998, 50.10162944170881],
        },
        properties: {
            id: "nextbike-4Vz7XLdKBnw31M4tjzJGbRnPRamMEW89",
            system_id: "8586cf7d-7fc9-5c3e-8805-e83481f13e3c",
            last_reported: ts,
            is_reserved: false,
            is_disabled: false,
            web_url: "https://nxtb.it/p/27582806",
        },
    },
    {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [14.430091381072998, 50.10162944170881],
        },
        properties: {
            id: "nextbike-pdyLwe2j8gWmQ9BcdX5epxnqEvlA5NRZ",
            system_id: "8586cf7d-7fc9-5c3e-8805-e83481f13e3c",
            last_reported: ts,
            is_reserved: false,
            is_disabled: false,
            web_url: "https://nxtb.it/p/27582806",
        },
    },
];
