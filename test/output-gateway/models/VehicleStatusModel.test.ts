import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { VehicleStatusModel } from "#og/models/VehicleStatusModel";
import { VehicleSharingContainer } from "#og/ioc/Di";
import { VectorTile } from "@mapbox/vector-tile";
import Pbf from "pbf";
import { getFreeVehicleStatusFeatures } from "../fixtures/freeVehicleStatusFeatures";

chai.use(chaiAsPromised);

describe("VehicleStatusModel", () => {
    let model: VehicleStatusModel;
    let connector: IDatabaseConnector;

    before(async () => {
        connector = VehicleSharingContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
        model = new VehicleStatusModel();
    });

    it("should return correct GetAllMVTS results", async () => {
        const result: ArrayBuffer = await model.GetAllMVTS(4424, 2774, 13);
        const tile = new VectorTile(new Pbf(new Uint8Array(result)));
        const geojson = [];
        for (let i = 0; i < tile.layers["free_vehicle_status"].length; i++) {
            geojson.push(tile.layers["free_vehicle_status"].feature(i).toGeoJSON(4424, 2774, 13));
        }
        expect(geojson).to.have.length(2);
        const timestamp = geojson[0].properties.last_reported; // dynamic timestamp from example data
        expect(geojson).to.deep.eq(getFreeVehicleStatusFeatures(timestamp));
    });

    it("should return correct GetAllMVTS results with system_id filter", async () => {
        const result: ArrayBuffer = await model.GetAllMVTS(4424, 2774, 13, [
            "8586cf7d-7fc9-5c3e-8805-e83481f13e3c",
            "d727bb19-9755-40b2-9615-01fbb6180b8b",
        ]);
        const tile = new VectorTile(new Pbf(new Uint8Array(result)));
        const geojson = [];
        for (let i = 0; i < tile.layers["free_vehicle_status"].length; i++) {
            geojson.push(tile.layers["free_vehicle_status"].feature(i).toGeoJSON(4424, 2774, 13));
        }
        expect(geojson).to.have.length(2);
        const timestamp = geojson[0].properties.last_reported; // dynamic timestamp from example data
        expect(geojson).to.deep.eq(getFreeVehicleStatusFeatures(timestamp));
    });
});
