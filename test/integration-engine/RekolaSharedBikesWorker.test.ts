import sinon, { SinonSandbox, SinonSpy } from "sinon";
import fs from "fs";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { RekolaSharedBikesWorker } from "#ie/RekolaSharedBikesWorker";

describe("RekolaSharedBikesWorker", () => {
    let worker: RekolaSharedBikesWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: () => ({
                    commit: sandbox.stub(),
                    rollback: sandbox.stub(),
                }),
                query: sandbox.stub().returns([[{}]]),
            })
        );

        worker = new RekolaSharedBikesWorker();

        const trackableData = JSON.parse(
            fs.readFileSync(__dirname + "/data/rekola-trackables-datasource-short.json").toString("utf8")
        );

        sandbox.stub(worker["zonesDatasource"], "getAll");
        sandbox.stub(worker["trackablesDatasource"], "getAll").returns(trackableData);

        sandbox.stub(worker["geofencingTransformation"], "transform").callsFake(() => Promise.resolve([] as any));

        sandbox.stub(worker["stationInformationTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["stationStatusTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["bikeStatusTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["vehicleTypeTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["stationStatusVehicleTypeTransformation"], "transform").callsFake(() => Promise.resolve([] as any));

        sandbox.stub(worker["geofencingZonesModel"], "replace");

        sandbox.stub(worker["vehicleTypesModel"], "bulkSave");
        sandbox.stub(worker["vehicleTypesModel"], "deleteDataBefore");
        sandbox.stub(worker["stationInformationModel"], "bulkSave");
        sandbox.stub(worker["stationStatusModel"], "bulkSave");
        sandbox.stub(worker["stationStatusVehicleTypeModel"], "bulkSave");
        sandbox.stub(worker["stationStatusVehicleTypeModel"], "deleteOrphans");
        sandbox.stub(worker["vehicleStatusModel"], "bulkSave");
        sandbox.stub(worker["oldDataCleanerHelper"], "deleteOldTrackableData");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("refreshRekolaGeofencingData should call the correct methods", async () => {
        await worker.refreshRekolaGeofencingData();
        sandbox.assert.calledOnce(worker["zonesDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["geofencingTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["geofencingZonesModel"].replace as SinonSpy);
    });

    it("refreshRekolaTrackableData should call the correct methods", async () => {
        await worker.refreshRekolaTrackableData();

        sandbox.assert.calledOnce(worker["trackablesDatasource"].getAll as SinonSpy);

        sandbox.assert.calledOnce(worker["stationInformationTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["bikeStatusTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["vehicleTypeTransformation"].transform as SinonSpy);

        sandbox.assert.calledOnce(worker["vehicleTypesModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["vehicleTypesModel"].deleteDataBefore as SinonSpy);
        sandbox.assert.calledOnce(worker["stationInformationModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusVehicleTypeModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["vehicleStatusModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["oldDataCleanerHelper"].deleteOldTrackableData as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusVehicleTypeModel"].deleteOrphans as SinonSpy);
    });
});
