import StationStatusVehicleTypeHelper from "#ie/helpers/StationStatusVehicleTypeHelper";
import { NextbikeSharedBikesWorker } from "#ie/NextbikeSharedBikesWorker";
import { NextbikeDefinitionTransformation } from "#ie/transformations/Nextbike";
import { ISharedVehiclesSystemInformationOutput } from "#sch";
import { config, DataSource } from "@golemio/core/dist/integration-engine";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { LOCK, Transaction } from "@golemio/core/dist/shared/sequelize";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

describe("NextbikeSharedBikesWorker", () => {
    const transaction: Transaction = {
        commit: sinon.stub(),
        rollback: sinon.stub(),
        afterCommit: function (fn: (transaction: Transaction) => void | Promise<void>): void {
            throw new Error("Function not implemented.");
        },
        LOCK: LOCK,
    };

    let worker: NextbikeSharedBikesWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns(transaction),
                query: sandbox.stub().returns([[{}]]),
            })
        );

        sinon.stub(config, "datasources").value({
            NextbikeSharedBikes: {
                headers: {},
                baseUrl: "https://gbfs.nextbike.net/maps/gbfs/v2/",
                sourceIds: ["nextbike_tg"],
            },
        });

        worker = new NextbikeSharedBikesWorker();

        sandbox.stub(worker["dataSourceFactory"], "getDataSourceDefinition").returns({
            getAll: async () =>
                Promise.resolve({
                    last_updated: 1655289551,
                    ttl: 60,
                    data: {
                        cs: {
                            feeds: [
                                {
                                    name: "system_information",
                                    url: "https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_tg/cs/system_information.json",
                                },
                            ],
                        },
                    },
                    version: "2.2",
                }),
        } as unknown as DataSource);

        sandbox.stub(worker["dataSourceFactory"], "getDataSourceData").returns({
            getAll: async () => {},
        } as unknown as DataSource);

        sandbox.stub(worker["transformationFactory"], "getDefinitionTransformation").returns({
            transform: () => [
                {
                    name: "system_information",
                    url: "https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_tg/cs/system_information.json",
                },
            ],
        } as unknown as NextbikeDefinitionTransformation);

        sandbox.stub(worker["transformationFactory"], "getDataTransformation").returns({
            transform: () => ({
                systemInfo: {
                    system_id: "nextbike_tg",
                } as ISharedVehiclesSystemInformationOutput,
            }),
        } as any);

        sandbox.stub(StationStatusVehicleTypeHelper, "mapTransformationResults").returns([]);
        sandbox.stub(worker["systemInformationModel"], "bulkSave");
        sandbox.stub(worker["pricingPlansModel"], "bulkSave");
        sandbox.stub(worker["pricingModel"], "bulkSave");
        sandbox.stub(worker["rentalAppsModel"], "save");
        sandbox.stub(worker["vehicleTypesModel"], "save");
        sandbox.stub(worker["vehicleStatusModel"], "bulkSave");
        sandbox.stub(worker["stationStatusModel"], "bulkSave");
        sandbox.stub(worker["stationStatusVehicleTypeModel"], "bulkSave");
        sandbox.stub(worker["stationInformationModel"], "bulkSave");
        sandbox.stub(worker["stationStatusModel"], "deleteDataBefore");
        sandbox.stub(worker["stationStatusVehicleTypeModel"], "deleteDataBefore");
        sandbox.stub(worker["vehicleTypesModel"], "deleteOrphans");
        sandbox.stub(worker["oldDataCleanerHelper"], "deleteOldTrackableData");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("refreshNextbikeData should call the correct methods", async () => {
        await worker.refreshNextbikeData();
        sandbox.assert.calledOnce(worker["dataSourceFactory"].getDataSourceDefinition as SinonSpy);
        sandbox.assert.calledOnce(worker["transformationFactory"].getDefinitionTransformation as SinonSpy);

        sandbox.assert.calledOnce(worker["dataSourceFactory"].getDataSourceData as SinonSpy);
        sandbox.assert.calledOnce(worker["transformationFactory"].getDataTransformation as SinonSpy);

        sandbox.assert.calledOnce(StationStatusVehicleTypeHelper.mapTransformationResults as SinonSpy);
        sandbox.assert.calledOnce(worker["systemInformationModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["pricingPlansModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["pricingModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["rentalAppsModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["vehicleTypesModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["vehicleStatusModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusVehicleTypeModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["stationInformationModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusModel"].deleteDataBefore as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusVehicleTypeModel"].deleteDataBefore as SinonSpy);
        sandbox.assert.calledOnce(worker["vehicleTypesModel"].deleteOrphans as SinonSpy);
        sandbox.assert.calledOnce(worker["oldDataCleanerHelper"].deleteOldTrackableData as SinonSpy);
    });
});
