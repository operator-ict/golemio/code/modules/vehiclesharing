import sinon, { SinonSandbox, SinonSpy } from "sinon";
import fs from "fs";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { CeskyCarsharingVehiclesWorker } from "#ie/CeskyCarsharingVehiclesWorker";

describe("CeskyCarsharingVehiclesWorker", () => {
    let worker: CeskyCarsharingVehiclesWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub(),
                query: sandbox.stub().returns([[{}]]),
            })
        );

        worker = new CeskyCarsharingVehiclesWorker();

        const trackableData = JSON.parse(
            fs.readFileSync(__dirname + "/data/cesky-carsharing-cars-datasource.json").toString("utf8")
        );

        sandbox.stub(worker["dataSource"], "getAll").returns(trackableData);
        sandbox.stub(worker["vehicleStatusTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["vehicleStatusModel"], "bulkSave");
        sandbox.stub(worker["oldDataCleanerHelper"], "deleteOldTrackableData");
        sandbox.stub(worker["staticDataRepository"], "getResourceData");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("refreshCeskyCarsharingVehiclesData should call the correct methods", async () => {
        await worker.refreshCeskyCarsharingVehiclesData();
        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["staticDataRepository"].getResourceData as SinonSpy);
        sandbox.assert.calledOnce(worker["vehicleStatusTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["vehicleStatusModel"].bulkSave as SinonSpy);
        sandbox.assert.callCount(worker["oldDataCleanerHelper"].deleteOldTrackableData as SinonSpy, 4);
    });
});
