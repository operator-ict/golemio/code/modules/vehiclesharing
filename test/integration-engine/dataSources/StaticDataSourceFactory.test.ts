import { StaticDataSourceFactory } from "#ie/dataSources/staticData/StaticDataSourceFactory";
import { ModuleContainerToken } from "#ie/ioc";
import { StaticDataProvider } from "#sch/definitions/models/helpers/StaticDataProviderEnum";
import { StaticDataResourceType } from "#sch/definitions/models/helpers/StaticDataResourceTypeEnum";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("StaticDataSourceFactory", () => {
    let sandbox: SinonSandbox;
    let factory: StaticDataSourceFactory;
    let container = IntegrationEngineContainer.createChildContainer();

    before(async () => {
        sandbox = sinon.createSandbox();

        container
            .registerSingleton(
                ModuleContainerToken.StaticDataHelper,
                class DummyHelper {
                    getBaseUrl() {
                        return "http://test.com/";
                    }
                }
            )
            .registerSingleton(ModuleContainerToken.StaticDataSourceFactory, StaticDataSourceFactory);
    });

    beforeEach(() => {
        factory = container.resolve<StaticDataSourceFactory>(ModuleContainerToken.StaticDataSourceFactory);
    });

    afterEach(async () => {
        sandbox.restore();
        container.clearInstances();
    });

    it("should generate datasource for Rekola", () => {
        const dataSource: any = factory.getDataSource(StaticDataProvider.Rekola, StaticDataResourceType.Pricings);
        expect(dataSource.protocolStrategy.connectionSettings.url).to.equal("http://test.com/Rekola/pricings.json");
    });

    it("should generate datasource for Cesky Carsharing", () => {
        const dataSource: any = factory.getDataSource(
            StaticDataProvider.CeskyCarsharing,
            StaticDataResourceType.SystemInformation
        );

        expect(dataSource.protocolStrategy.connectionSettings.url).to.equal(
            "http://test.com/CeskyCarsharing/systemInformation.json"
        );
    });

    it("should throw error for unknown provider", () => {
        expect(() => factory.getDataSource("blbost" as StaticDataProvider, StaticDataResourceType.SystemInformation)).to.throw(
            "Unknown provider: blbost"
        );
    });

    it("should throw error for unknown resource type", () => {
        expect(() => factory.getDataSource(StaticDataProvider.CeskyCarsharing, "blbost" as StaticDataResourceType)).to.throw(
            "Unknown resource type: blbost"
        );
    });
});
