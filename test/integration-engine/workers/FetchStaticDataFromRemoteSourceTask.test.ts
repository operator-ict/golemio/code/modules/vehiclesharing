import { ModuleContainerToken, SharedVehiclesContainer } from "#ie/ioc";
import { StaticDataRepository } from "#ie/repositories/StaticDataRepository";
import { FetchStaticDataFromRemoteSourceTask } from "#ie/workers/tasks/FetchStaticDataFromRemoteSourceTask";
import { StaticDataProvider } from "#sch/definitions/models/helpers/StaticDataProviderEnum";
import { StaticDataResourceType } from "#sch/definitions/models/helpers/StaticDataResourceTypeEnum";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("StaticDataWorker - FetchStaticDataFromRemoteSourceTask", () => {
    let sandbox: SinonSandbox;
    let task: FetchStaticDataFromRemoteSourceTask;
    let postgresConnector: IDatabaseConnector;
    let container = IntegrationEngineContainer.createChildContainer();

    before(async () => {
        sandbox = sinon.createSandbox();
        postgresConnector = container.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        container
            .registerSingleton(
                ModuleContainerToken.StaticDataHelper,
                class DummyHelper {
                    getStaticDataResources() {
                        return {
                            [StaticDataProvider.Rekola]: {
                                "0": StaticDataResourceType.Pricings,
                            },
                        };
                    }
                }
            )
            .registerSingleton(
                ModuleContainerToken.StaticDataSourceFactory,
                class DummyFactory {
                    getDataSource() {
                        return {
                            getAll() {
                                return Promise.resolve({
                                    id: "test-id",
                                    pricing_plan_id: "test-id-plan",
                                    pricing_type: "per_min_pricing",
                                    pricing_order: 0,
                                    start: 0,
                                    rate: 30,
                                    interval: 30,
                                    end: null,
                                    start_time_of_period: null,
                                    end_time_of_period: null,
                                });
                            },
                        };
                    }
                }
            )
            .registerSingleton(ModuleContainerToken.StaticDataRepository, StaticDataRepository)
            .registerSingleton(ModuleContainerToken.FetchStaticDataFromRemoteSourceTask, FetchStaticDataFromRemoteSourceTask);
    });

    beforeEach(() => {
        task = container.resolve<FetchStaticDataFromRemoteSourceTask>(ModuleContainerToken.FetchStaticDataFromRemoteSourceTask);
    });

    afterEach(async () => {
        sandbox.restore();
        container.clearInstances();
    });

    it("should fetch data", async () => {
        await task["execute"]();

        const repository = container.resolve<StaticDataRepository>(ModuleContainerToken.StaticDataRepository);
        const data = await repository.getData(StaticDataProvider.Rekola, [StaticDataResourceType.Pricings]);
        expect(data).to.have.property("pricings");
        expect(data.pricings).to.have.lengthOf(1);
        expect(data.pricings[0]).to.have.property("id", "test-id");
    });
});
