import { ModuleContainerToken } from "#ie/ioc";
import { RefreshStaticDataInDbTask } from "#ie/workers/tasks/RefreshStaticDataInDbTask";
import { StaticDataProvider } from "#sch/definitions/models/helpers/StaticDataProviderEnum";
import { StaticDataResourceType } from "#sch/definitions/models/helpers/StaticDataResourceTypeEnum";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("StaticDataWorker - RefreshStaticDataInDbTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshStaticDataInDbTask;
    let postgresConnector: IDatabaseConnector;
    let container = IntegrationEngineContainer.createChildContainer();
    let stubs: Record<string, SinonStub> = {};

    before(async () => {
        sandbox = sinon.createSandbox();
        stubs = {
            ceskyCarsharing: sandbox.stub().resolves(),
            hoppyGo: sandbox.stub().resolves(),
            rekola: sandbox.stub().resolves(),
        };

        postgresConnector = container.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();

        container
            .registerSingleton(
                ModuleContainerToken.StaticDataHelper,
                class DummyHelper {
                    getStaticDataResources() {
                        return {
                            [StaticDataProvider.CeskyCarsharing]: {
                                "0": StaticDataResourceType.Pricings,
                            },
                            [StaticDataProvider.Rekola]: {
                                "0": StaticDataResourceType.Pricings,
                            },
                        };
                    }
                }
            )
            .registerSingleton(
                ModuleContainerToken.StaticDataRepository,
                class DummyRepository {
                    getData() {
                        switch (arguments[0]) {
                            case StaticDataProvider.CeskyCarsharing:
                                return Promise.resolve({
                                    [StaticDataResourceType.Pricings]: [
                                        {
                                            id: "test-id",
                                            pricing_plan_id: "test-id-plan",
                                            pricing_type: "per_min_pricing",
                                            pricing_order: 0,
                                            start: 0,
                                            rate: 15,
                                            interval: 60,
                                            end: null,
                                            start_time_of_period: null,
                                            end_time_of_period: null,
                                        },
                                    ],
                                });
                            case StaticDataProvider.Rekola:
                                return Promise.resolve({
                                    [StaticDataResourceType.Pricings]: [
                                        {
                                            id: "test-id",
                                            pricing_plan_id: "test-id-plan",
                                            pricing_type: "per_min_pricing",
                                            pricing_order: 0,
                                            start: 0,
                                            rate: 30,
                                            interval: 30,
                                            end: null,
                                            start_time_of_period: null,
                                            end_time_of_period: null,
                                        },
                                    ],
                                });
                        }
                    }
                }
            )
            .registerSingleton(
                ModuleContainerToken.CeskyCarsharingStaticDataService,
                class DummyService {
                    saveSystemsStaticData() {
                        return stubs.ceskyCarsharing();
                    }
                }
            )
            .registerSingleton(
                ModuleContainerToken.HoppyGoStaticDataService,
                class DummyService {
                    saveSystemBasics() {
                        return stubs.hoppyGo();
                    }
                }
            )
            .registerSingleton(
                ModuleContainerToken.RekolaStaticDataService,
                class DummyService {
                    saveRekolaStaticData() {
                        return stubs.rekola();
                    }
                }
            )
            .registerSingleton(ModuleContainerToken.RefreshStaticDataInDbTask, RefreshStaticDataInDbTask);
    });

    beforeEach(() => {
        task = container.resolve<RefreshStaticDataInDbTask>(ModuleContainerToken.RefreshStaticDataInDbTask);
    });

    afterEach(async () => {
        sandbox.restore();
        container.clearInstances();
    });

    it("should refresh static data for each provider", async () => {
        await task["execute"]();

        expect(stubs.ceskyCarsharing.calledOnce).to.be.true;
        expect(stubs.hoppyGo.calledOnce).to.be.false;
        expect(stubs.rekola.calledOnce).to.be.true;
    });
});
