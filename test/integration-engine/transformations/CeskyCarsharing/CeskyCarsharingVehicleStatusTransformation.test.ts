import { CeskyCarsharingVehicleStatusTransformation } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingVehicleStatusTransformation";
import { ICeskyCarsharingVehicles } from "#sch/datasources";
import { expect } from "chai";
import fs from "fs";
import { CeskyCarsharingCarsFixture } from "../../data/CeskyCarsharingCars.fixture";
import { IPricingPlanMap } from "#sch/definitions/PricingPlansMap";
import { CeskyCarsharingPricingPlanMatcher } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingPricingPlanMatcher";

describe("CeskyCarsharingVehicleStatusTransformation", async () => {
    const transformationDate = new Date("2023-10-31T11:40:00+02:00");
    const transformation = new CeskyCarsharingVehicleStatusTransformation(new CeskyCarsharingPricingPlanMatcher());

    it("transforms data correctly", async () => {
        const testSourceData: ICeskyCarsharingVehicles = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/cesky-carsharing-cars-datasource.json").toString("utf8")
        );
        const pricingPlansMap: IPricingPlanMap[] = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/cesky-carsharing-pricingPlanMap.json").toString("utf8")
        );
        const actualResult = await transformation.transform({
            cars: testSourceData.cars,
            pricingPlansMap,
            transformationDate,
        });

        expect(actualResult).to.deep.equal(CeskyCarsharingCarsFixture);
    });
});
