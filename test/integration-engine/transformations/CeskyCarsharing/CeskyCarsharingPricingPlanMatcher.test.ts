import { CeskyCarsharingPricingPlanMatcher } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingPricingPlanMatcher";
import { ICeskyCarsharingCar } from "#sch/datasources";
import { IPricingPlanMap } from "#sch/definitions/PricingPlansMap";
import fs from "fs";
import { expect } from "chai";

describe("CeskyCarsharingPricingPlanMatcher", () => {
    const matcher = new CeskyCarsharingPricingPlanMatcher();
    const pricingPlansMap: IPricingPlanMap[] = JSON.parse(
        fs.readFileSync(__dirname + "/../../data/cesky-carsharing-pricingPlanMap.json").toString("utf8")
    );

    it("matches pricing plans corrctly", () => {
        const car: ICeskyCarsharingCar = {
            car_name: "Škoda Scala",
            company_email: "info@car4way.cz",
            company_id: 5,
            company_name: "CAR4WAY",
            company_phone: "+420 601 311 011",
            company_web: "https://www.car4way.cz",
            fuel: 1,
            fuel_type: "benzín",
            latitude: 50.105687,
            longitude: 14.465196,
            res_url: "https://www.car4way.cz/go_car4way/Car_map/Default?rz=5ST0420",
            rz: "5ST0420",
            status: "1",
        };

        const result = matcher.getPricingPlanId(car, pricingPlansMap);
        expect(result).to.eql("car4way_optimum");
    });

    it("throws when pricing plan not matched", () => {
        const car: ICeskyCarsharingCar = {
            car_name: "Škoda Felicia",
            company_email: "info@car4way.cz",
            company_id: 5,
            company_name: "CAR4WAY",
            company_phone: "+420 601 311 011",
            company_web: "https://www.car4way.cz",
            fuel: 1,
            fuel_type: "benzín",
            latitude: 50.105687,
            longitude: 14.465196,
            res_url: "https://www.car4way.cz/go_car4way/Car_map/Default?rz=5ST0420",
            rz: "MBC 22-60",
            status: "1",
        };

        expect(() => matcher.getPricingPlanId(car, pricingPlansMap)).to.throw();
    });
});
