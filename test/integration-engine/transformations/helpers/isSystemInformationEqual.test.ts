import { NextbikeSystemInformationFixture } from "../../data/NextbikeSystemInformation.fixture";
import { expect } from "chai";
import { isSystemInformationEqual } from "#ie/transformations/helpers/isSystemInformationEqual";

describe("isSystemInformationEqual", () => {
    it("compares SystemInformation correctly", () => {
        expect(isSystemInformationEqual(NextbikeSystemInformationFixture, NextbikeSystemInformationFixture)).to.be.true;

        let differentSystemInformation = JSON.parse(JSON.stringify(NextbikeSystemInformationFixture));
        differentSystemInformation.short_name = "NotToBeRider";

        expect(isSystemInformationEqual(NextbikeSystemInformationFixture, differentSystemInformation)).to.be.false;
    });
});
