import { RekolaVehicleStatusTransformation } from "#ie/transformations/Rekola";
import { expect } from "chai";
import fs from "fs";
import { rekolaVehicleStatusOutputFixture } from "../../data/rekolaVehicleStatusOutputFixture";

describe("RekolaVehicleStatusTransformation", async () => {
    const transformationDate = new Date("2023-10-31T11:40:00+02:00");

    it("transforms input data correctly", async () => {
        const testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/rekola-trackables-datasource-short.json").toString("utf8")
        );

        const transformation = new RekolaVehicleStatusTransformation();
        const transformedData = await transformation.transform({
            trackables: testSourceData,
            transformationDate,
        });

        expect(transformedData).to.deep.equal(rekolaVehicleStatusOutputFixture);
    });
});
