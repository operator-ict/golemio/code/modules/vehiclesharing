import fs from "fs";
import { expect } from "chai";
import { RekolaStationStatusVehicleTypeTransformation } from "#ie/transformations/Rekola/RekolaStationStatusVehicleTypeTransformation";
import { rekolaStationStatusVehicleTypeOutputFixture } from "../../data/rekolaStationStatusVehicleTypeOutput.fixture";

describe("RekolaStationStatusVehicleTypeTransformation", () => {
    it("transforms input data correctly", async () => {
        const testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/rekola-trackables-datasource-short.json").toString("utf8")
        );
        const transformation = new RekolaStationStatusVehicleTypeTransformation();
        const transformedData = await transformation.transform(testSourceData.racks);

        expect(transformedData).to.deep.equal(rekolaStationStatusVehicleTypeOutputFixture);
    });
});
