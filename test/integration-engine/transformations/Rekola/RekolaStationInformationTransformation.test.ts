import { RekolaStationInformationTransformation } from "#ie/transformations/Rekola";
import { expect } from "chai";
import fs from "fs";
import { rekolaStationInformationOutputFixture } from "../../data/rekolaStationInformationOutput.fixture";

describe("RekolaStationInformationTransformation", async () => {
    const transformationDate = new Date("2023-10-31T11:40:00+02:00");

    it("transforms input data correctly", async () => {
        const testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/rekola-trackables-datasource-short.json").toString("utf8")
        );
        const stationInformationTransformation = new RekolaStationInformationTransformation();

        const transformedData = await stationInformationTransformation.transform({
            trackables: testSourceData,
            transformationDate,
        });

        expect(transformedData).to.deep.equal(rekolaStationInformationOutputFixture);
    });
});
