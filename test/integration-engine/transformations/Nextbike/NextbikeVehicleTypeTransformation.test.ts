import { NextbikeVehicleTypeTransformation } from "#ie/transformations";
import { INextbikeVehicleTypeInput } from "#sch/datasources";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { nextbikeTransformedDataFixture } from "../../data/nextbikeTransformedData.fixture";

chai.use(chaiAsPromised);

describe("NextbikeVehicleTypeTransformation", () => {
    let transformation: NextbikeVehicleTypeTransformation;
    let testSourceData: INextbikeVehicleTypeInput;

    beforeEach(() => {
        transformation = new NextbikeVehicleTypeTransformation("TestId");
        testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/nextbike-vehicle-types-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SharedBikesNextbikeVehicleTypeDataSourceTestIdTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform({ datasourceData: testSourceData });
        expect(data).to.deep.equal(nextbikeTransformedDataFixture.vehicleTypes);
    });
});
