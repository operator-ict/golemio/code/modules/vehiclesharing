import { NextbikeStationStatusTransformation } from "#ie/transformations";
import { INextbikeStationStatusInput } from "#sch/datasources";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { nextbikeTransformedDataFixture } from "../../data/nextbikeTransformedData.fixture";

chai.use(chaiAsPromised);

describe("NextbikeStationStatusTransformation", () => {
    const transformationDate = new Date("2023-10-31T11:40:00+02:00");
    let transformation: NextbikeStationStatusTransformation;
    let testSourceData: INextbikeStationStatusInput;

    beforeEach(() => {
        transformation = new NextbikeStationStatusTransformation("TestId");
        testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/nextbike-station-status-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SharedBikesNextbikeStationStatusDataSourceTestIdTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform({ datasourceData: testSourceData, transformationDate });
        expect(data).to.deep.equal(nextbikeTransformedDataFixture.stationStatus);
    });
});
