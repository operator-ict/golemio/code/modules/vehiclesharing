import { NextbikeStationInformationTransformation } from "#ie/transformations";
import { INextbikeStationInformationInput } from "#sch/datasources";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { nextbikeTransformedDataFixture } from "../../data/nextbikeTransformedData.fixture";

chai.use(chaiAsPromised);

describe("NextbikeStationInformationTransformation", () => {
    const transformationDate = new Date("2023-10-31T11:40:00+02:00");
    let transformation: NextbikeStationInformationTransformation;
    let testSourceData: INextbikeStationInformationInput;

    beforeEach(() => {
        transformation = new NextbikeStationInformationTransformation("TestId");
        testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/nextbike-station-information-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SharedBikesNextbikeStationInfoDataSourceTestIdTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform({ datasourceData: testSourceData, transformationDate });
        expect(data.stationInfo).to.deep.equal(nextbikeTransformedDataFixture.stationInformation);
        expect(nextbikeTransformedDataFixture.rentalApps).to.include.deep.members(data.rentalApps);
    });
});
