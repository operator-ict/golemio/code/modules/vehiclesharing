import { HoppyGoVehicleStatusTransformation } from "#ie/transformations/HoppyGo/HoppyGoVehicleStatusTransformation";
import { expect } from "chai";
import { hoppyGoDatasourceFixture } from "../../data/HoppyGoDatasource.fixture";
import { hoppyGoBikeStatusTransformationFixture } from "../../data/HoppyGoTransformedBikeStatus.fixture";

describe("HoppyGoVehicleStatusTransformation", async () => {
    const bikeStatusTransformation = new HoppyGoVehicleStatusTransformation();
    const transformationDate = new Date("2023-10-31T11:40:00+02:00");

    it("should have name", () => {
        expect(bikeStatusTransformation.name).not.to.be.undefined;
        expect(bikeStatusTransformation.name).is.equal("HoppyGoVehiclesDataSourceVehicleStatusTransformation");
    });

    it("should correctly transform data", async () => {
        const actualData = await bikeStatusTransformation.transform({
            cars: hoppyGoDatasourceFixture,
            transformationDate,
        });

        expect(actualData).to.deep.equal(hoppyGoBikeStatusTransformationFixture(transformationDate));
    });
});
