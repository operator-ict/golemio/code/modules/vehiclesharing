import { expect } from "chai";
import { HoppyGoPricingPlansTransformation } from "#ie/transformations/HoppyGo/HoppyGoPricingPlansTransformation";
import { hoppyGoDatasourceFixture } from "../../data/HoppyGoDatasource.fixture";
import { hoppyGoPricingPlanTransformationFixture } from "../../data/HoppyGoTransformedPricingPlans.fixture";

describe("HoppyGoPricingPlansTransformation", async () => {
    const pricingPlansTransformation = new HoppyGoPricingPlansTransformation();
    const transformationDate = new Date();

    it("should have name", () => {
        expect(pricingPlansTransformation.name).not.to.be.undefined;
        expect(pricingPlansTransformation.name).is.equal("HoppyGoVehiclesDataSourcePricingPlansTransformation");
    });

    it("should correctly transform data", async () => {
        const actualData = await pricingPlansTransformation.transform({
            cars: hoppyGoDatasourceFixture,
            transformationDate,
        });

        expect(actualData).to.deep.equal(hoppyGoPricingPlanTransformationFixture(transformationDate));
    });
});
