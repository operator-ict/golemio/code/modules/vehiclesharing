import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { MobilityOperatorTransformation } from "#ie/transformations/MobilityOperator/MobilityOperatorTransformation";
import { expect } from "chai";
import { MobilityOperatorMapFixture } from "../../data/MobilityOperatorMap.fixture";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { FakeDataSource } from "./FakeDataSource";

describe("MobilityOperatorMapProvider", () => {
    const mobilityOperatorMapProvider = new MobilityOperatorRentalAppProvider(
        new FakeDataSource() as any as DataSource,
        new MobilityOperatorTransformation()
    );

    it("returns MobilityOperatorMap", async () => {
        const actualResult = await mobilityOperatorMapProvider.getMobilityOperatorRentalApps();
        expect(actualResult).to.deep.equal(MobilityOperatorMapFixture);
    });
});
