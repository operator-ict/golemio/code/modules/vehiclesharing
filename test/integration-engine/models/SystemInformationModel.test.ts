import { SystemInformationModel } from "#ie/models/SystemInformationModel";
import { HOPPYGO_SYSTEM_ID } from "#ie/transformations/HoppyGo/transformConstants";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { NextbikeSystemInformationFixture } from "../data/NextbikeSystemInformation.fixture";

describe("SystemInformationModel", () => {
    let systemInformationModel: SystemInformationModel;
    let sandbox: SinonSandbox;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        await PostgresConnector.connect();
        systemInformationModel = new SystemInformationModel();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("getOne() gets correct row", async () => {
        const row = await systemInformationModel.GetOne(HOPPYGO_SYSTEM_ID);
        expect(row.system_id).to.eq(HOPPYGO_SYSTEM_ID);
        // compare some data...
        expect(row.name).to.eq("HoppyGo");
        expect(row.email).to.eq("info@hoppygo.com");
    });

    it("deleteBySystemId() deletes given row", async () => {
        const systemId = "systemToDelete";

        let systemInformationToDelete = structuredClone(NextbikeSystemInformationFixture);
        systemInformationToDelete.system_id = systemId;
        systemInformationToDelete.operator_id = systemId;

        await systemInformationModel.save([systemInformationToDelete]);
        await systemInformationModel.deleteBySystemId(systemId);
        const deletedRow = await systemInformationModel.GetOne(systemId);
        expect(deletedRow).to.be.null;
        const notDeletedRow = await systemInformationModel.GetOne(HOPPYGO_SYSTEM_ID);
        expect(notDeletedRow).not.to.be.null;
    });
});
