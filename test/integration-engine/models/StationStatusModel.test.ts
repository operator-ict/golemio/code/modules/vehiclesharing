import { StationInformationModel } from "#ie/models/StationInformationModel";
import { StationStatusModel } from "#ie/models/StationStatusModel";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("StationStatusModel", () => {
    let stationStatusModel: StationStatusModel;
    let stationInformationModel: StationInformationModel;
    let sandbox: SinonSandbox;
    let postgresConnector = IntegrationEngineContainer.createChildContainer().resolve<IDatabaseConnector>(
        CoreToken.PostgresConnector
    );

    const system_id = "d727bb19-9755-40b2-9615-01fbb6180b8b";

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        await postgresConnector.connect();
        stationStatusModel = new StationStatusModel();
        stationInformationModel = new StationInformationModel();

        await stationInformationModel.save([
            {
                id: "rekola-92",
                system_id,
                name: "Test StationStatusVehicleTypeModel",
                point: {
                    type: "Point",
                    coordinates: [14.4610581741, 50.1088538325],
                },
                address: null,
                post_code: null,
                cross_street: null,
                region_id: null,
                rental_methods: null,
                is_virtual_station: true,
                station_area: null,
                capacity: null,
                vehicle_capacity: null,
                vehicle_type_capacity: null,
                is_valet_station: null,
                rental_app_id: null,
            },
            {
                id: "rekola-93",
                system_id,
                name: "Test StationStatusVehicleTypeModel 2",
                point: {
                    type: "Point",
                    coordinates: [14.4610581741, 50.1088538325],
                },
                address: null,
                post_code: null,
                cross_street: null,
                region_id: null,
                rental_methods: null,
                is_virtual_station: true,
                station_area: null,
                capacity: null,
                vehicle_capacity: null,
                vehicle_type_capacity: null,
                is_valet_station: null,
                rental_app_id: null,
            },
        ]);
        await stationStatusModel.save([
            {
                station_id: "rekola-92",
                num_bikes_available: 0,
                num_bikes_disabled: null,
                num_docks_available: null,
                is_installed: true,
                is_renting: false,
                is_returning: true,
                last_reported: "2023-09-21T09:29:00.000Z",
                processed_at: new Date("2020-02-07T02:57:32.410Z"),
            },
            {
                station_id: "rekola-93",
                num_bikes_available: 1,
                num_bikes_disabled: null,
                num_docks_available: null,
                is_installed: true,
                is_renting: false,
                is_returning: true,
                last_reported: "2023-09-21T09:29:00.000Z",
                processed_at: new Date("2020-02-07T02:59:32.410Z"),
            },
        ]);
    });

    afterEach(async () => {
        sandbox.restore();
        await stationStatusModel.query(
            "delete from vehiclesharing.station_status WHERE station_id IN ('rekola-92', 'rekola-93')"
        );
        await stationInformationModel.query(
            "delete from vehiclesharing.station_information WHERE id IN ('rekola-92', 'rekola-93')"
        );
    });

    it("deleteDataBefore should delete the proper rows", async () => {
        const connection = await postgresConnector.connect();
        const t = await connection.transaction();
        const deletedRows = await stationStatusModel.deleteDataBefore(new Date("2020-02-07T02:58:32.410Z"), system_id, t);
        await t.commit();
        expect(deletedRows).to.eq(1);
    });
});
