import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { VehicleStatusModel } from "#ie/models/VehicleStatusModel";
import { ISharedVehicleStatusOutput } from "#sch";
import { REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola";

chai.use(chaiAsPromised);

describe("BikeStatusModel", () => {
    let model: VehicleStatusModel;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        PostgresConnector.connect();
        model = new VehicleStatusModel();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("deleteDataBefore should delete", async () => {
        const input: Array<Partial<ISharedVehicleStatusOutput>> = [
            {
                id: "rekola-bikestatus-test",
                system_id: REKOLA_SYSTEM_ID,
                point: {
                    type: "Point",
                    coordinates: [14.4418911111, 50.1071377778],
                },
                description: "",
                is_reserved: false,
                is_disabled: false,
                vehicle_type_id: "rekola-bike",
                rental_app_id: null,
                processed_at: new Date("2020-02-07T02:57:32.410Z"),
            },
            {
                id: "rekola-bikestatus-test-2",
                system_id: REKOLA_SYSTEM_ID,
                point: {
                    type: "Point",
                    coordinates: [14.4618911112, 50.1071377779],
                },
                description: "",
                is_reserved: false,
                is_disabled: false,
                vehicle_type_id: "rekola-bike",
                rental_app_id: null,
                processed_at: new Date("2020-02-07T02:59:32.410Z"),
            },
        ];

        await model.save(input);
        const deletedRows = await model.deleteDataBefore(new Date("2020-02-07T02:58:32.410Z"), REKOLA_SYSTEM_ID);
        expect(deletedRows).to.eq(1);
    });
});
