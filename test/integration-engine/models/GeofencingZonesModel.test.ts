import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { GeofencingZonesModel } from "#ie/models/GeofencingZonesModel";
import { RekolaGeofencingFixture } from "../data/RekolaGeofencing.fixture";

chai.use(chaiAsPromised);

describe("GeofencingZonesModel", () => {
    let model: GeofencingZonesModel;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        PostgresConnector.connect();
        model = new GeofencingZonesModel();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should save data", async () => {
        const data = await model.replace([RekolaGeofencingFixture], RekolaGeofencingFixture.system_id);
        expect(data[0].getDataValue("id")).to.be.equal("rekola-test");
    });
});
