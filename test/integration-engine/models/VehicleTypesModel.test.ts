import { PricingPlansModel } from "#ie/models/PricingPlansModel";
import { StationInformationModel } from "#ie/models/StationInformationModel";
import { StationStatusModel } from "#ie/models/StationStatusModel";
import { StationStatusVehicleTypeModel } from "#ie/models/StationStatusVehicleTypeModel";
import { VehicleStatusModel } from "#ie/models/VehicleStatusModel";
import { VehicleTypesModel } from "#ie/models/VehicleTypesModel";
import { NEXTBIKE_ID_PREFIX, NEXTBIKE_VEHICLE_TYPE_ID_PREFIX } from "#ie/transformations/Nextbike";
import { Vehiclesharing } from "#sch";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("VehicleTypesModel", () => {
    let postgresConnector = IntegrationEngineContainer.createChildContainer().resolve<IDatabaseConnector>(
        CoreToken.PostgresConnector
    );
    let pricingPlansModel: PricingPlansModel;
    let stationStatusVehicleTypeModel: StationStatusVehicleTypeModel;
    let stationStatusModel: StationStatusModel;
    let stationInformationModel: StationInformationModel;
    let vehicleStatusModel: VehicleStatusModel;
    let vehicleTypesModel: VehicleTypesModel;
    let sandbox: SinonSandbox;

    const system_id = "d727bb19-9755-40b2-9615-01fbb6180b8b";
    const station_id = NEXTBIKE_ID_PREFIX + "TestId-Station92";

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        await postgresConnector.connect();
        pricingPlansModel = new PricingPlansModel();
        stationStatusVehicleTypeModel = new StationStatusVehicleTypeModel();
        stationStatusModel = new StationStatusModel();
        stationInformationModel = new StationInformationModel();
        vehicleStatusModel = new VehicleStatusModel();
        vehicleTypesModel = new VehicleTypesModel();

        await stationInformationModel.save([
            {
                id: station_id,
                system_id,
                name: "Test VehicleTypesModel",
                point: {
                    type: "Point",
                    coordinates: [14.4610581741, 50.1088538325],
                },
                address: null,
                post_code: null,
                cross_street: null,
                region_id: null,
                rental_methods: null,
                is_virtual_station: true,
                station_area: null,
                capacity: null,
                vehicle_capacity: null,
                vehicle_type_capacity: null,
                is_valet_station: null,
                rental_app_id: null,
            },
        ]);
        await vehicleTypesModel.save([
            {
                id: "nextbike-bike",
                name: "TEST",
                form_factor: "bicycle",
                propulsion_type: "human",
                max_range_meters: null,
            },
            {
                id: NEXTBIKE_VEHICLE_TYPE_ID_PREFIX + "TestId-198",
                name: "3-rychlostní SMARTbike 2.0.",
                form_factor: "bicycle",
                propulsion_type: "human",
                max_range_meters: null,
            },
            {
                id: NEXTBIKE_VEHICLE_TYPE_ID_PREFIX + "TestId-199",
                name: "3-rychlostní SMARTbike",
                form_factor: "bicycle",
                propulsion_type: "human",
                max_range_meters: null,
            },
            {
                id: NEXTBIKE_VEHICLE_TYPE_ID_PREFIX + "TestId-200",
                name: "8-rychlostní SMARTbike",
                form_factor: "bicycle",
                propulsion_type: "human",
                max_range_meters: 10000,
            },
        ]);
        await stationStatusModel.save([
            {
                station_id,
                num_bikes_available: 0,
                num_bikes_disabled: null,
                num_docks_available: null,
                is_installed: true,
                is_renting: false,
                is_returning: true,
                last_reported: "2023-09-21T09:29:00.000Z",
            },
        ]);
        await stationStatusVehicleTypeModel.save([
            {
                station_id,
                vehicle_type_id: NEXTBIKE_VEHICLE_TYPE_ID_PREFIX + "TestId-198",
                count: 1,
                num_bikes_available: 1,
                num_bikes_disabled: null,
                vehicle_docks_available: null,
            },
        ]);
        await pricingPlansModel.save([
            {
                id: NEXTBIKE_ID_PREFIX + "TestId-Rate0338",
                system_id,
                url: "https://www.nextbikeczech.com/kompletni-cenik/",
                last_updated: "2022-06-17T10:13:47.000Z",
                name: "CZK 30/30 min, CZK 300/24h",
                currency: "CZK",
                price: 0,
                is_taxable: false,
                description: "CZK 30/30 min, CZK 300/24h",
                surge_pricing: false,
            },
        ]);
        await vehicleStatusModel.save([
            {
                id: NEXTBIKE_ID_PREFIX + "TestId-Bike1094",
                system_id,
                point: {
                    type: "Point",
                    coordinates: [14.45876, 50.06821],
                },
                helmets: null,
                passengers: null,
                damage_description: null,
                description: null,
                vehicle_registration: null,
                is_reserved: false,
                is_disabled: false,
                vehicle_type_id: NEXTBIKE_VEHICLE_TYPE_ID_PREFIX + "TestId-199",
                last_reported: "2022-06-17T10:12:45.000Z",
                current_range_meters: null,
                charge_percent: null,
                rental_app_id: "d0bac98a-616f-5e73-b206-11e5dd1107e2",
                station_id,
                pricing_plan_id: NEXTBIKE_ID_PREFIX + "TestId-Rate0338",
                make: null,
                model: null,
                color: null,
                processed_at: new Date("2023-10-31T11:40:00+02:00"),
            },
        ]);
    });

    afterEach(async () => {
        sandbox.restore();
        const deleteFrom = async (model: PostgresModel, table: string, where: string) =>
            model.query(`DELETE FROM "${Vehiclesharing.pgSchema}"."${table}" WHERE ${where}`);
        await deleteFrom(stationStatusModel, "station_status", `station_id = '${station_id}'`);
        await deleteFrom(stationStatusVehicleTypeModel, "station_status_vehicle_type", `station_id = '${station_id}'`);
        await deleteFrom(vehicleStatusModel, "vehicle_status", `station_id = '${station_id}'`);
        await deleteFrom(pricingPlansModel, "pricing_plans", `id = '${NEXTBIKE_ID_PREFIX + "TestId-Rate0338"}'`);
        await deleteFrom(vehicleTypesModel, "vehicle_types", `id = '${NEXTBIKE_VEHICLE_TYPE_ID_PREFIX + "TestId-198"}'`);
        await deleteFrom(vehicleTypesModel, "vehicle_types", `id = '${NEXTBIKE_VEHICLE_TYPE_ID_PREFIX + "TestId-199"}'`);
        await deleteFrom(stationInformationModel, "station_information", `id = '${station_id}'`);
    });

    it("deleteOrphans should delete the proper rows", async () => {
        const result = await vehicleTypesModel.deleteOrphans(NEXTBIKE_VEHICLE_TYPE_ID_PREFIX.slice(0, -1));
        expect(result).to.equal(2);
    });
});
