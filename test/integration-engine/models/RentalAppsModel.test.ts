import { RentalAppsModel } from "#ie/models/RentalAppsModel";
import { StationInformationModel } from "#ie/models/StationInformationModel";
import { SystemInformationModel } from "#ie/models/SystemInformationModel";
import { VehicleStatusModel } from "#ie/models/VehicleStatusModel";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { RentalAppsModelFixture } from "../data/RentalAppsModel.fixture";

chai.use(chaiAsPromised);

describe("RentalAppsModel", () => {
    let rentalAppsModel: RentalAppsModel;
    let stationInformationModel: StationInformationModel;
    let systemInformationModel: SystemInformationModel;
    let vehicleStatusModel: VehicleStatusModel;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        PostgresConnector.connect();
        rentalAppsModel = new RentalAppsModel();
        stationInformationModel = new StationInformationModel();
        systemInformationModel = new SystemInformationModel();
        vehicleStatusModel = new VehicleStatusModel();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("deleteOldData should not delete anything when there is nothing to delete", async () => {
        const { count: initCount } = await rentalAppsModel.findAndCountAll({ limit: 1e4 });

        await rentalAppsModel.deleteOldData();

        const { count: postDeleteCount } = await rentalAppsModel.findAndCountAll({ limit: 1e4 });
        expect(postDeleteCount).to.eq(initCount);
    });

    it("deleteOldData should delete data not used in other tables", async () => {
        const { count: initCount } = await rentalAppsModel.findAndCountAll({ limit: 1e4 });
        await rentalAppsModel.save(RentalAppsModelFixture.rentalApps);
        await systemInformationModel.save(RentalAppsModelFixture.systemInformation);
        await stationInformationModel.save(RentalAppsModelFixture.stationInformation);
        await vehicleStatusModel.save(RentalAppsModelFixture.vehicleStatus);
        const { count: postSaveCount } = await rentalAppsModel.findAndCountAll({ limit: 1e4 });
        expect(postSaveCount).to.eq(initCount + 8);

        await rentalAppsModel.deleteOldData();

        const { count: postDeleteCount, rows: postDeleteRows } = await rentalAppsModel.findAndCountAll({ limit: 1e4 });
        expect(postDeleteCount).to.eq(initCount + 6);
        const postDeleteRowIds = (postDeleteRows as Array<{ getDataValue: (col: string) => string }>).map((row) =>
            row.getDataValue("id")
        );
        expect(postDeleteRowIds)
            .to.be.an("array")
            .that.includes.members([
                "d0bac98a-616f-5e73-b206-11e5dd1107e2",
                "17dbff1f-f98d-583d-94c6-ad76bc081437",
                "7b9e88c8-7bdb-434c-bd64-89a2bb5d03dc",
                "cc95b1ee-9360-5f41-ae12-3f226177a54f",
                "e92890e6-9c75-5a26-b6f6-79aa5d0d34de",
                "9e2e9ef2-e79b-4cef-a777-b3b0f15c8304",
            ])
            .that.does.not.include.members(["741c2b04-e076-4d2c-b129-c35ee8f177f7", "b058a4a9-c215-492a-9c83-673a5886cd89"]);
    });
});
