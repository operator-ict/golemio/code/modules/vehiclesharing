import { StationInformationHistoryModel } from "#ie/models/StationInformationHistoryModel";
import { StationStatusModel } from "#ie/models/StationStatusModel";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola";

chai.use(chaiAsPromised);

describe("StationInformationHistoryModel", () => {
    let stationInformationHistoryModel: StationInformationHistoryModel;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        PostgresConnector.connect();
        stationInformationHistoryModel = new StationInformationHistoryModel();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("deleteNHoursOldData should delete", async () => {
        const { count: initCount } = await stationInformationHistoryModel.findAndCountAll({});
        await stationInformationHistoryModel.save([
            {
                id: "rekola-200078",
                system_id: REKOLA_SYSTEM_ID,
                name: "Zkušební ostrov",
                point: {
                    type: "Point",
                    coordinates: [14.4610581741, 50.1088538325],
                },
                address: null,
                post_code: null,
                cross_street: null,
                region_id: null,
                rental_methods: null,
                is_virtual_station: true,
                station_area: null,
                capacity: null,
                vehicle_capacity: null,
                vehicle_type_capacity: null,
                is_valet_station: null,
                rental_app_id: null,
                processed_at: new Date("2022-05-12T09:08:00.000Z"),
            },
            {
                id: "rekola-200079",
                system_id: REKOLA_SYSTEM_ID,
                name: "Zkušební ostrov 2",
                point: {
                    type: "Point",
                    coordinates: [14.4610581752, 50.1088538333],
                },
                address: null,
                post_code: null,
                cross_street: null,
                region_id: null,
                rental_methods: null,
                is_virtual_station: true,
                station_area: null,
                capacity: null,
                vehicle_capacity: null,
                vehicle_type_capacity: null,
                is_valet_station: null,
                rental_app_id: null,
                processed_at: new Date("2022-05-12T09:08:00.000Z"),
            },
            {
                id: "rekola-200080",
                system_id: REKOLA_SYSTEM_ID,
                name: "Zkušební ostrov 3",
                point: {
                    type: "Point",
                    coordinates: [14.4610581753, 50.1088538334],
                },
                address: null,
                post_code: null,
                cross_street: null,
                region_id: null,
                rental_methods: null,
                is_virtual_station: true,
                station_area: null,
                capacity: null,
                vehicle_capacity: null,
                vehicle_type_capacity: null,
                is_valet_station: null,
                rental_app_id: null,
                processed_at: new Date("2022-05-12T09:10:00.000Z"),
            },
        ]);

        await stationInformationHistoryModel.deleteDataBefore(new Date("2022-05-12T09:09:00.000Z"), REKOLA_SYSTEM_ID);
        const { count: finalCount } = await stationInformationHistoryModel.findAndCountAll({});
        expect(finalCount).to.eq(initCount + 1);
    });
});
