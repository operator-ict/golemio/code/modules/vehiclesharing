import { CeskyCarsharingStaticDataService } from "#ie/CeskyCarsharingStaticDataService";
import { ModuleContainerToken, SharedVehiclesContainer } from "#ie/ioc";
import { PricingModel } from "#ie/models/PricingModel";
import { PricingPlansModel } from "#ie/models/PricingPlansModel";
import { VehicleTypesModel } from "#ie/models/VehicleTypesModel";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { expect } from "chai";
import { describe, it } from "mocha";

describe("CeskyCarsharingStaticDataService", () => {
    let postgresConnector: IDatabaseConnector;
    let container = IntegrationEngineContainer.createChildContainer();

    let ceskyCarsharingStaticDataService: CeskyCarsharingStaticDataService;

    before(async () => {
        postgresConnector = container.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);

        const connection = await postgresConnector.connect();

        ceskyCarsharingStaticDataService = SharedVehiclesContainer.resolve<CeskyCarsharingStaticDataService>(
            ModuleContainerToken.CeskyCarsharingStaticDataService
        );

        const t = await connection.transaction();
        await ceskyCarsharingStaticDataService.cleanOldData(new Date(), t as Transaction);
        await t.commit();
    });

    describe("cleanOldData", () => {
        it("should delete old pricing data for CeskyCarsharing systems (car4way)", async () => {
            const repository = SharedVehiclesContainer.resolve<PricingModel>(ModuleContainerToken.PricingModel);
            const data = await repository.find({});

            const filteredRandomId = data.filter((item: any) => item.pricing_plan_id === "random_id");
            expect(filteredRandomId).to.have.lengthOf(1);

            const filteredCar4wayData = data.filter((item: any) => item.pricing_plan_id === "car4way_standard");
            expect(filteredCar4wayData).to.be.empty;
        });

        it("should delete old pricing plans data", async () => {
            const repository = SharedVehiclesContainer.resolve<PricingPlansModel>(ModuleContainerToken.PricingPlansModel);
            const data = await repository.find({});
            const filteredCar4wayData = data.filter((item: any) => item.system_id === "car4way");
            expect(filteredCar4wayData).to.be.empty;
        });

        it("should delete old vehicle types", async () => {
            const repository = SharedVehiclesContainer.resolve<VehicleTypesModel>(ModuleContainerToken.VehicleTypesModel);
            const data = await repository.find({});
            const filteredCar4wayData = data.filter((item: any) => item.id === "carsharing_benzin");
            expect(filteredCar4wayData).to.be.empty;
        });
    });
});
