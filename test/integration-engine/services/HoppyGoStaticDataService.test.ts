import { HoppyGoStaticDataService } from "#ie/HoppyGoStaticDataService";
import { ModuleContainerToken, SharedVehiclesContainer } from "#ie/ioc";
import { RentalAppsModel } from "#ie/models/RentalAppsModel";
import { SystemInformationModel } from "#ie/models/SystemInformationModel";
import { VehicleTypesModel } from "#ie/models/VehicleTypesModel";
import { HOPPYGO_VEHICLE_TYPE_COMBUSTION, HOPPYGO_VEHICLE_TYPE_ELECTRIC } from "#ie/transformations/HoppyGo/transformConstants";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("HoppyGoStaticDataService", async () => {
    let sandbox: SinonSandbox;
    const testContainer = IntegrationEngineContainer.createChildContainer();
    let postgresConnector: IDatabaseConnector;
    let ieContainer = IntegrationEngineContainer.createChildContainer();
    let hoppyGoStaticDataService: HoppyGoStaticDataService;

    before(async () => {
        postgresConnector = ieContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);

        const connection = await postgresConnector.connect();

        hoppyGoStaticDataService = SharedVehiclesContainer.resolve<HoppyGoStaticDataService>(
            ModuleContainerToken.HoppyGoStaticDataService
        );

        const t = await connection.transaction();
        await hoppyGoStaticDataService.cleanOldData(new Date(), t as Transaction);
        await t.commit();
    });

    beforeEach(async () => {
        sandbox = sinon.createSandbox();

        try {
            const systemInformationModel = new SystemInformationModel();
            await systemInformationModel.deleteBySystemId("ef63ac2e-4c71-4c28-a496-f0dc010d630b-test");
        } catch (err) {}

        testContainer
            .registerSingleton(
                ModuleContainerToken.MobilityOperatorRentalAppProvider,
                class DummyProvider {
                    getMobilityOperatorRentalApps: () => Promise<any> = async () => {
                        return {};
                    };
                }
            )
            .registerSingleton<RentalAppsModel>(ModuleContainerToken.RentalAppsModel, RentalAppsModel)
            .registerSingleton<VehicleTypesModel>(ModuleContainerToken.VehicleTypesModel, VehicleTypesModel)
            .registerSingleton<SystemInformationModel>(ModuleContainerToken.SystemInformationModel, SystemInformationModel)
            .registerSingleton(ModuleContainerToken.HoppyGoStaticDataService, HoppyGoStaticDataService);
    });

    afterEach(() => {
        testContainer.clearInstances();
        sandbox.restore();
    });

    it("saves HoppyGo static data correctly", async () => {
        const systemInformationModel = new SystemInformationModel();
        const vehicleTypesModel = new VehicleTypesModel();
        const rentalAppsModel = new RentalAppsModel();

        const emptySystemInformation = await systemInformationModel.GetOne("ef63ac2e-4c71-4c28-a496-f0dc010d630b-test");
        expect(emptySystemInformation).to.be.null;

        const hoppyGoStaticDataServiceTestContainer = testContainer.resolve<HoppyGoStaticDataService>(
            ModuleContainerToken.HoppyGoStaticDataService
        );
        await hoppyGoStaticDataServiceTestContainer.saveSystemBasics({
            systemInformation: [
                {
                    operator_id: "hoppygo_test",
                    system_id: "ef63ac2e-4c71-4c28-a496-f0dc010d630b-test",
                    language: "cs",
                    logo: "",
                    name: "HoppyGo",
                    short_name: "HoppyGo",
                    operator: null,
                    url: null,
                    purchase_url: "https://hoppygo.com/cs/",
                    start_date: new Date("2021-11-01T00:00:00.000Z"),
                    phone_number: "",
                    email: "",
                    feed_contact_email: "",
                    timezone: "Europe/Prague",
                    license_id: null,
                    license_url: "",
                    attribution_organization_name: null,
                    attribution_url: null,
                    terms_of_use_url: "",
                    rental_app_id: "hoppygo_rental_apps",
                },
            ],
            vehicleTypes: [
                {
                    id: "hoppygo_car_combustion",
                    form_factor: "car",
                    propulsion_type: "combustion",
                    max_range_meters: null,
                    name: "HoppyGo Car Combustion",
                },
            ],
            rentalApps: [
                {
                    id: "hoppygo_rental_apps",
                    android_store_url: "",
                    android_discovery_url: null,
                    ios_store_url: "",
                    ios_discovery_url: null,
                    web_url: null,
                },
            ],
        });

        const systemInformation = await systemInformationModel.GetOne("ef63ac2e-4c71-4c28-a496-f0dc010d630b-test");

        expect(systemInformation.operator_id).to.equal("hoppygo_test");

        const electricVehicleType = await vehicleTypesModel.findOne({ id: HOPPYGO_VEHICLE_TYPE_ELECTRIC, raw: true });
        const combustionVehicleType = await vehicleTypesModel.findOne({ id: HOPPYGO_VEHICLE_TYPE_COMBUSTION, raw: true });

        expect(electricVehicleType).not.to.be.null;
        expect(combustionVehicleType).not.to.be.null;

        const actualRentalApps = await rentalAppsModel.findOne({ id: "hoppygo_rental_apps" });
        expect(actualRentalApps).not.to.be.null;
    });

    it("should delete old vehicle types", async () => {
        const repository = SharedVehiclesContainer.resolve<VehicleTypesModel>(ModuleContainerToken.VehicleTypesModel);
        const data = await repository.find({});

        const hoppyFilteredData = data.filter((item: any) => item.id === "hoppygo_car_electric");
        expect(hoppyFilteredData).to.be.empty;
    });
});
