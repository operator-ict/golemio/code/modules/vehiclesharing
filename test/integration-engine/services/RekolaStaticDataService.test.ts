import { ModuleContainerToken, SharedVehiclesContainer } from "#ie/ioc";
import { PricingModel } from "#ie/models/PricingModel";
import { PricingPlansModel } from "#ie/models/PricingPlansModel";
import { StationStatusVehicleTypeModel } from "#ie/models/StationStatusVehicleTypeModel";
import { VehicleTypesModel } from "#ie/models/VehicleTypesModel";
import { RekolaStaticDataService } from "#ie/transformations/Rekola/RekolaStaticDataService";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { expect } from "chai";
import { describe, it } from "mocha";

describe("RekolaStaticDataService", () => {
    let postgresConnector: IDatabaseConnector;
    let container = IntegrationEngineContainer.createChildContainer();

    let rekolaDataService: RekolaStaticDataService;

    before(async () => {
        postgresConnector = container.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);

        const connection = await postgresConnector.connect();

        rekolaDataService = SharedVehiclesContainer.resolve<RekolaStaticDataService>(
            ModuleContainerToken.RekolaStaticDataService
        );

        const t = await connection.transaction();
        await rekolaDataService.cleanOldData(new Date(), t as Transaction);
        await t.commit();
    });

    describe("cleanOldData", () => {
        it("should delete old pricing data for CeskyCarsharing systems (rekola)", async () => {
            const repository = SharedVehiclesContainer.resolve<PricingModel>(ModuleContainerToken.PricingModel);
            const data = await repository.find({});

            const filteredRandomId = data.filter((item: any) => item.pricing_plan_id === "random_id");
            expect(filteredRandomId).to.have.lengthOf(1);

            const rekolaData = data.filter((item: any) => item.pricing_plan_id === "d727bb19-9755-40b2-9615-01fbb6180b8b");
            expect(rekolaData).to.be.empty;
        });

        it("should delete old pricing plans data", async () => {
            const repository = SharedVehiclesContainer.resolve<PricingPlansModel>(ModuleContainerToken.PricingPlansModel);
            const data = await repository.find({});

            const rekolaData = data.filter((item: any) => item.system_id === "d727bb19-9755-40b2-9615-01fbb6180b8b");
            expect(rekolaData).to.be.empty;
        });
    });
});
