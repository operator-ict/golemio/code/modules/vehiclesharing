import { ISharedVehiclesGeofencingZoneOutput } from "#sch";

export const RekolaGeofencingFixture: ISharedVehiclesGeofencingZoneOutput = {
    id: "rekola-test",
    system_id: "d727bb19-9755-40b2-9615-01fbb6180b8b",
    name: null,
    note: null,
    source: null,
    price: null,
    priority: 0,
    start: null,
    end: null,
    geom: {
        type: "MultiPolygon",
        coordinates: [
            [
                [
                    [14.4817694, 50.1083112],
                    [14.4818661, 50.108183],
                    [14.4822792, 50.1082768],
                    [14.4822483, 50.1084307],
                    [14.4817694, 50.1083112],
                ],
            ],
        ],
    },
    ride_allowed: true,
    ride_through_allowed: true,
    maximum_speed_kph: null,
    parking_allowed: true,
};
