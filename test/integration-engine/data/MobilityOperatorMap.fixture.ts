import { IMobilityOperatorRentalAppMap } from "#sch/datasources/MobilityOperator";

export const MobilityOperatorMapFixture: IMobilityOperatorRentalAppMap = {
    "d727bb19-9755-40b2-9615-01fbb6180b8b": {
        id: "bb622abc-84a1-11ec-a8a3-0242ac120002",
        android_store_url: "https://play.google.com/store/apps/details?id=cz.rekola.app&hl=cs",
        android_discovery_url: null,
        ios_store_url: "https://itunes.apple.com/cz/app/rekola/id888759232?mt=8",
        ios_discovery_url: null,
        web_url: "https://www.rekola.cz",
    },
    "d54cab39-e658-5d1e-aa23-d0d5fe60bbe6": {
        id: "mobility-operator-app-nextbike_tk",
        android_store_url: "https://play.google.com/store/apps/details?id=de.nextbike",
        android_discovery_url: "https://app.nextbike.net/",
        ios_store_url: "https://apps.apple.com/app/id504288371",
        ios_discovery_url: "nextbike://",
        web_url: "https://www.nextbikeczech.com",
    },
};
