import { ISharedVehicleTypeOutput } from "#sch";

export const rekolaVehicleTypeOutputFixture: ISharedVehicleTypeOutput[] = [
    {
        id: "rekola-bike",
        form_factor: "bicycle",
        propulsion_type: "human",
        max_range_meters: null,
        name: "bicycle",
    },
];
