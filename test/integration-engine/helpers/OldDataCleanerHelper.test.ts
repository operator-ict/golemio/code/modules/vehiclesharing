import { OldDataCleanerHelper } from "#ie/helpers/OldDataCleanerHelper";
import { ModuleContainerToken, SharedVehiclesContainer } from "#ie/ioc";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

describe("OldDataCleanerHelper", () => {
    let helper: OldDataCleanerHelper;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub(),
            })
        );

        helper = SharedVehiclesContainer.resolve(ModuleContainerToken.OldDataCleanerHelper);

        sandbox.stub(helper["stationInformationHistoryModel"], "deleteDataBefore");
        sandbox.stub(helper["vehicleStatusModel"], "deleteDataBefore");
        sandbox.stub(helper["rentalAppsModel"], "deleteOldData");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("deleteOldTrackableData should call the correct methods", async () => {
        const processed_at = new Date(123456);
        await helper.deleteOldTrackableData(processed_at, "rekola");
        sandbox.assert.calledOnceWithExactly(
            helper["stationInformationHistoryModel"].deleteDataBefore as SinonSpy,
            processed_at,
            "rekola"
        );
        sandbox.assert.calledOnceWithExactly(helper["vehicleStatusModel"].deleteDataBefore as SinonSpy, processed_at, "rekola");
        sandbox.assert.calledOnceWithExactly(helper["rentalAppsModel"].deleteOldData as SinonSpy);
    });
});
