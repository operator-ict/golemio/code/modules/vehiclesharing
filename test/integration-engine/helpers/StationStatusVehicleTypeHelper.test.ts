import StationStatusVehicleTypeHelper from "#ie/helpers/StationStatusVehicleTypeHelper";
import { expect } from "chai";
import { nextbikeTransformedDataFixture } from "../data/nextbikeTransformedData.fixture";

describe("StationStatusVehicleTypeHelper", () => {
    const transformationDate = new Date("2023-10-31T11:40:00+02:00");

    it("mapTransformationResults should properly connect station_status data with vehicle_status data ", async () => {
        const result = StationStatusVehicleTypeHelper.mapTransformationResults(
            nextbikeTransformedDataFixture.stationStatus,
            nextbikeTransformedDataFixture.bikeStatus,
            transformationDate
        );
        expect(result).to.have.deep.members(nextbikeTransformedDataFixture.stationStatusVehicleType);
    });
});
