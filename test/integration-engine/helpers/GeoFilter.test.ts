import { GeoFilter } from "#ie/helpers/GeoFilter";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("GeoFilter", async () => {
    let sandbox: SinonSandbox;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("test filter with points in and out", () => {
        const vysehradGeo = [50.06586299548668, 14.419671137370544];
        const stillCzechia = [49.61561622185518, 18.550647328590053];
        const asCzechia = [50.25555167522285, 12.178106251599177];
        const dresdenGeo = [51.03458458160594, 13.736888028436612];
        const polandGeo = [50.92698631371991, 16.30310750402458];
        const slovakiaGeo = [48.88807807799409, 18.023651634310173];

        const geoFilter = new GeoFilter();
        expect(geoFilter.isInCzechia(vysehradGeo[0], vysehradGeo[1])).to.be.true;
        expect(geoFilter.isInCzechia(stillCzechia[0], stillCzechia[1])).to.be.true;
        expect(geoFilter.isInCzechia(asCzechia[0], asCzechia[1])).to.be.true;
        expect(geoFilter.isInCzechia(dresdenGeo[0], dresdenGeo[1])).to.be.false;
        expect(geoFilter.isInCzechia(polandGeo[0], polandGeo[1])).to.be.false;
        expect(geoFilter.isInCzechia(slovakiaGeo[0], slovakiaGeo[1])).to.be.false;
    });

    it("perf test ~ check if done in reasonable time", () => {
        const geoFilter = new GeoFilter();
        const vysehradGeo = [50.06586299548668, 14.419671137370544];

        for (let index = 0; index < 10000; index++) {
            expect(geoFilter.isInCzechia(vysehradGeo[0], vysehradGeo[1])).to.be.true;
        }
    });
});
