# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.7.10] - 2025-03-04

### Fixed

- Fix Bolt mobility operator rental app id ([vehiclesharing#11](https://gitlab.com/operator-ict/golemio/code/modules/vehiclesharing/-/issues/11))

## [2.7.9] - 2025-02-25

### Fixed

-   Add Nextbike `station_status` data source property `vehicle_types_available`

## [2.7.8] - 2025-02-18

### Fixed

-   Rekola vehicle type cleaning after save ([p0131#182](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/182))

## [2.7.7] - 2025-02-11

### Fixed

-   nextbike data saving in a transaction ([p0131#159](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/159))
-   Rekola data saving in transaction and docs fix ([p0131#182](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/182))

## [2.7.6] - 2024-12-17

### Fixed

-   MVTS Router accept header fix ([output-gateway#226](https://gitlab.com/operator-ict/golemio/code/output-gateway/-/issues/226))

## [2.7.5] - 2024-12-12

### Fixed

-   Static carsharing data saving ([#8](https://gitlab.com/operator-ict/golemio/code/modules/vehiclesharing/-/issues/8))

## [2.7.4] - 2024-11-05

### Added

-   AsyncAPI documentation ([integration-engine#266](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/266))

## [2.7.3] - 2024-09-26

### Added

-   systemId filter for `/free_vehicles_status` endpoint ([p0131#175](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/175))

## [2.7.1] - 2024-09-19

### Changed

-  mobility operator datasource attribute `termsOfUseUrl` nullable

## [2.7.0] - 2024-08-29

### Added

-   MVTS endpoint for `/free_vehicles_status` ([p0131#175](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/175))

## [2.6.1] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [2.6.0] - 2024-08-01

### Added

-   Removal of static data ([p0131#174](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/158))

## [2.5.2] - 2024-07-25

### Added

-   add cache-control header to all responses ([core#107](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/107))

### Removed

-   remove redis useCacheMiddleware ([core#107](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/107))

## [2.5.1] - 2024-07-17

### Fixed

-   api validations after adding checkExact in core ([core#109](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/109))

## [2.5.0] - 2024-07-10

### Added

-   Bolt cars are added using Český carsharing datasource ([p0131#174](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/174))

## [2.4.7] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [2.4.6] - 2024-04-24

### Fixed

-   Issue with http protocol strategy ([vehiclesharing#7](https://gitlab.com/operator-ict/golemio/code/modules/vehiclesharing/-/issues/7))

## [2.4.5] - 2024-04-08

### Changed

-   axios exchanged for native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

### Fixed

-   API docs inconsistencies (https://gitlab.com/operator-ict/golemio/code/modules/vehiclesharing/-/merge_requests/68)

## [2.4.4] - 2024-03-25

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [2.4.3] - 2024-01-24

### Fixed

-   Skip geofencing locations of unknown compaines from Český Carsharing due to outdated source.

## [2.4.2] - 2024-01-22

### Removed

-   Remove AJO from Český Carsharing due to end of their service ([p0131#157](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/157))

## [2.4.1] - 2024-01-17

### Added

-   Add pricingPlanMap as static data source Český Carsharing plans mapping ([p0131#155](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/155))

## [2.4.1] - 2024-01-15

-   No changelog

## [2.4.0] - 2023-12-13

### Added

-   Nextbike `vehicle_types` data integration from data source ([p0131#133](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/133))

### Changed

-   Periodically delete old Nextbike data from `vehiclesharing.station_status_vehicle_type` ([p0131#133](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/133))
-   Periodically delete old Nextbike data from `vehiclesharing.station_status` ([p0131#133](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/133))
-   Remove Nextbike static data

### Fixed

-   Default `processed_at` values should be `epoch` instead of `-infinity`

## [2.3.10] - 2023-12-11

### Added

-   New worker for fetching and saving providers' static data ([p0131#154](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/154))

### Changed

-   Refresh providers' static data from Azure blob storage instead of local files

## [2.3.9] - 2023-11-13

### Fixed

-   Nextbike data outages when replacing old data with new data ([p0131#150](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/150))

## [2.3.8] - 2023-10-16

### Fixed

-   Docs tag for API versioning ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [2.3.7] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))
-   Integration changes of rekola bikes, display all parkings and racks ([p0131#146](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/146))

## [2.3.6] - 2023-09-06

### Changed

-   Periodically delete old data from `vehiclesharing.rental_apps` ([p0131#92](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/92))

## [2.3.5] - 2023-08-28

### Changed

-   use static data for HoppyGo and Nextbike ([vehiclesharing#3](https://gitlab.com/operator-ict/golemio/code/modules/vehiclesharing/-/issues/3))

## [2.3.4] - 2023-08-09

### Fixed

-   error handling on empty `last_reported` ([vehiclesharing#1](https://gitlab.com/operator-ict/golemio/code/modules/vehiclesharing/-/issues/1))

## [2.3.3] - 2023-08-02

### Changed

-   Station data retention is done in one go by procedure instead of deleting row by row ([vehiclesharing#2](https://gitlab.com/operator-ict/golemio/code/modules/vehiclesharing/-/issues/2))

## [2.3.2] - 2023-07-24

### Fixed

-   Fix HoppyGo rental apps saving order ([p0131#142](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/142))

## [2.3.1] - 2023-07-18

### Fixed

-   hotfix for sql 14 epoch extract recast

## [2.3.0] - 2023-07-10

### Fixed

-   Added color to mobility operator

### Changed

-   Renamed to Vehiclesharing and moved to a new repository ([shared-bikes#15](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/15))

## [2.2.2] - 2023-06-19

### Removed

-   Remove BeRider integration ([p0131#136](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/136))

## [2.2.1] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [2.2.0] - 2023-06-05

### Fixed

-   Configurable GBFS url link ([p0131#132](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/132))

## [2.1.11] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [2.1.10] - 2023-05-29

### Changed

-   Update openapi docs

## [2.1.9] - 2023-04-17

### Changed

-   Deleting old data immediately after provider worker run, instead of periodic by time

## [2.1.8] - 2023-04-05

### Changed

-   Errors from Český carsharing transformation logged as "debug" instead of "error"

## [2.1.7] - 2023-04-03

### Changed

-   DI and json static files Český carsharing
-   updated price plans for Český carsharing

### Fixed

-   Fixed rental app ids of mobilityOperator source

## [2.1.6] - 2023-03-27

### Added

-   Add shared cars router as a replacement of deprecated shared-cars module ([#14](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/14))

## [2.1.5] - 2023-03-22

### Fixed

-   Add filter by vehicle types to legacy endpoint, fix query filter companyNames ([#20](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/20))

### Added

-   Add shared cars router as a replacement of deprecated shared-cars module ([#14](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/14))

## [2.1.4] - 2023-03-20

### Changed

-   data outside of czechia are not saved.

## [2.1.3] - 2023-03-08

### Fixed

-   Include staticData in package

## [2.1.2] - 2023-03-06

### Changed

-   open api docs minor changes

## [2.1.1] - 2023-02-27

### Added

-   Added MobilityOperator data source; used as source for rental_apps
-   Partly implemented DI via tsyringe for Rekola and BeRider

### Changed

-   Moved BeRider constants from worker to contants file
-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [2.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [2.0.16] - 2023-01-10

### Fixed

-   `SharedBikesNextbikeFreeBikeStatusDataSource` failing due to a change of `vehicle_type_id`'s field type

## [2.0.15] - 2023-01-04

### Changed

-   updated reference of terms of use for HoppyGo, NextBike and BeRider

## [2.0.14] - 2022-12-09

### Fixed

-   `SharedBikesNextbikeFreeBikeStatusDataSource` failing due to a new property `vehicle_type_id`

## [2.0.13] - 2022-12-07

### Changed

-   Add id column to pricings to allow multiple pricings of a type being stored in database
-   Update Cesky carsharing pricelists
-   Update Nextbike pricing plans description

## [2.0.12] - 2022-11-14

### Changed

-   Update pricing plans of Český carsharing / Autonapůl
-   Skip cars in Český carsharing feed that cannot be processed

## [2.0.11] - 2022-11-03

### Added

-   New integration: Český carsharing

## [2.0.10] - 2022-10-11

-   Update static data

## [2.0.9] - 2022-09-21

### Changed

-   The Lodash library was replaced with js functions ([core#42](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/42))
-   Removed JOIN on pricing_plans in VehicleStatusModel.GetAllGTFS() as not needed.

### Added

-   Add HoppyGo integration

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [2.0.8] - 2022-08-01

### Added

-   Fix / add bike_status.pricing_plan_id
-   Added make, model and color to BikeStatus

## [2.0.7] - 2022-07-18

### Added

-   New Nextbike integration

### Changed

-   Fix strict validation for Berider (Helmets, isClean)
-   Rekola transformation refactoring

## [2.0.6] - 2022-06-07

### Changed

-   change operator_id from integer to string
-   Typescript version update from 4.4.4 to 4.6.4

## [2.0.5] - 2022-05-19

### Added

-   Add BeRider integration

### Changed

-   refactor: code structure and schema fixes
-   fix OG/IE SystemInformationModel

## [2.0.4] - 2022-05-18

### Fixed

-   Minor fixes in json validation schema to support strict option

## [2.0.3] - 2022-05-02

-   Add transformation of Next Bike api data to GBFS

### Changed

-   fix: remove unused rental_apps_system_info
-   fix: add pricings table primary key
-   refactor: separate StationInformationHistoryModel from generic one
-   fix: change rekola system information language to cs
-   fix: fix schemas implementation

### Added

### Changed

-   update rental app discovery url

## [2.0.2] - 2022-04-13

### Changed

-   Removed `.json` extension from GBFS endpoint urls

## [2.0.1] - 2022-04-07

### Added

-   Implement GBFS output api ([p0131#23](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/23))
-   Add OpenAPI v3.0.3 ([#4](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/4))
-   Add implementation documentation ([#6](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/6))

### Fixed

-   Add time zone to timestamps

## [2.0.0] - 2022-03-01

### Added

-   Implement GBFS format for Rekola trackables and geofencing zones ([p0131#22](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/22))

## Changed

-   Rewrite models from Mongo to Postgres

