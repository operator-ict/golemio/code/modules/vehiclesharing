# Implementační dokumentace modulu _vehiclesharing (Sdílená mobilita)_

## Záměr

Modul slouží k ukládání a poskytování informací o sdílených dopravních prostředcích v Praze.

**Dostupní poskytovatelé**:

-   Rekola
-   Next Bike
-   HoppyGo
-   Car4Way (Český carsharing)
-   Anytime (Český carsharing)
-   Autonapůl (Český carsharing)
-   Bolt (Český carsharing)
-   IPT blob storage (statická data)

**Výstupní formáty**:

-   GeoJSON
-   GBFS

## Vstupní data

### Data nám jsou posílána

Nejsou.

### Data aktivně stahujeme

Data stahujeme pravidelně od HoppyGo, Rekola, z Next Bike api a od Asociace českého carsharingu.
Při transformacích stahujeme zdroj MobilityOperator, ze kterého bereme rental_apps pro system information.

#### Datový zdroj _Next Bike_

-   Next Bike zdroj:
    -   [nextbike_tg](https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_tg/gbfs.json) (Praha)
    -   [nextbike_tq](https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_tq/gbfs.json) (Mladoboleslavsko)
    -   [nextbike_tk](https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_tk/gbfs.json) (Kladno)
    -   [nextbike_td](https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_td/gbfs.json) (Berounsko)
    -   [nextbike_co](https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_co/gbfs.json) (Benešov)
    -   [nextbike_ty](https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_ty/gbfs.json) (Písek)
-   [validační schéma Next Bike](../src/schema-definitions/datasources/Nextbike/*)
-   rabbitmq fronta: `dataplatform.nextbikevehiclesharing.refreshNextbikeData`
-   frekvence stahování: `0 * * * * *`, tzn. každou minutu

#### Datový zdroj _Rekola - trackable data_

-   zdroj dat
    -   [Rekola trackables](https://www.rekola.cz/api/mobile/regions/1/trackables)
    -   [IE config](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/blob/development/config/datasources.template.json#L52)
    -   parametry v security
-   formát dat
    -   HTTP GET
    -   json
    -   [validační schéma](../src/schema-definitions/datasources/RekolaTrackables.ts#L37)
    -   ukázka dat

```json
{
    "isDiff": false,
    "isVehicleBorrowed": false,
    "racks": [
        {
            "id": 5,
            "isVisible": true,
            "name": "Podolí",
            "position": {
                "lat": 50.0516212413,
                "lng": 14.4167971936
            },
            "pin": {
                "width": 42,
                "height": 34,
                "sizes": {
                    "x1": {
                        "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/r-1-ffffff-ff0090-0--bike-gears-x1.png",
                        "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/r-1-f2f2f2-ff0090-0--bike-gears-x1.png"
                    },
                    "x2": {
                        "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/r-1-ffffff-ff0090-0--bike-gears-x2.png",
                        "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/r-1-f2f2f2-ff0090-0--bike-gears-x2.png"
                    },
                    "x3": {
                        "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/r-1-ffffff-ff0090-0--bike-gears-x3.png",
                        "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/r-1-f2f2f2-ff0090-0--bike-gears-x3.png"
                    },
                    "x4": {
                        "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/r-1-ffffff-ff0090-0--bike-gears-x4.png",
                        "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/r-1-f2f2f2-ff0090-0--bike-gears-x4.png"
                    }
                }
            },
            "vehicles": [
                {
                    "id": 5853,
                    "type": "bike",
                    "isVisible": true,
                    "isBorrowed": false,
                    "isBorrowedByMe": false,
                    "name": "Kriminálka Miami",
                    "label": "",
                    "position": {
                        "lat": 50.0516212413,
                        "lng": 14.4167971936
                    },
                    "positionNote": "Podolí",
                    "pin": {
                        "width": 42,
                        "height": 34,
                        "sizes": {
                            "x1": {
                                "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-ffffff-ff0090-x-0-x1.png",
                                "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-f2f2f2-ff0090-x-0-x1.png"
                            },
                            "x2": {
                                "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-ffffff-ff0090-x-0-x2.png",
                                "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-f2f2f2-ff0090-x-0-x2.png"
                            },
                            "x3": {
                                "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-ffffff-ff0090-x-0-x3.png",
                                "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-f2f2f2-ff0090-x-0-x3.png"
                            },
                            "x4": {
                                "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-ffffff-ff0090-x-0-x4.png",
                                "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-f2f2f2-ff0090-x-0-x4.png"
                            }
                        }
                    }
                }
            ]
        }
    ],
    "vehicles": [
        {
            "id": 1011,
            "type": "bike",
            "isVisible": true,
            "isBorrowed": false,
            "isBorrowedByMe": false,
            "name": "Smíchov",
            "label": "",
            "position": {
                "lat": 50.0947822222,
                "lng": 14.4640422222
            },
            "positionNote": null,
            "pin": {
                "width": 42,
                "height": 34,
                "sizes": {
                    "x1": {
                        "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-ffffff-ff0090-x-0-x1.png",
                        "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-f2f2f2-ff0090-x-0-x1.png"
                    },
                    "x2": {
                        "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-ffffff-ff0090-x-0-x2.png",
                        "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-f2f2f2-ff0090-x-0-x2.png"
                    },
                    "x3": {
                        "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-ffffff-ff0090-x-0-x3.png",
                        "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-f2f2f2-ff0090-x-0-x3.png"
                    },
                    "x4": {
                        "normal": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-ffffff-ff0090-x-0-x4.png",
                        "selected": "https://s3.eu-central-1.amazonaws.com/data2.rekola.cz/pins/v020/v-bike-gears-f2f2f2-ff0090-x-0-x4.png"
                    }
                }
            }
        }
    ]
}
```

-   frekvence stahování
    -   [dev](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/cluster_development/golemio_golemio-values.yaml#L387)
        -   `0 */2 * * * *`, tzn. každé 2 minuty
    -   [prod](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/cluster_production/golemio_golemio-values.yaml#L385)
        -   `0 */2 * * * *`, tzn. každé 2 minuty
-   název rabbitmq fronty
    -   `dataplatform.rekolavehiclesharing.refreshRekolaTrackableData`
    -   bez parametrů

#### Datový zdroj _Rekola - geofencing data_

-   zdroj dat
    -   [Rekola zones](https://www.rekola.cz/api/mobile/regions/1/zones)
    -   [IE config](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/blob/development/config/datasources.template.json#L52)
    -   parametry v security
-   formát dat
    -   HTTP GET
    -   json
    -   [validační schéma](../src/schema-definitions/datasources/RekolaGeofencingZones.ts#L3)
    -   ukázka dat

```json
[
    {
        "vehicleType": {
            "type": "bike",
            "name": "kolo",
            "description": "Klasické městské či sportovní kolo",
            "icon": "https://mobile.rekola.cz/static/icons/vehicles/png/bike.png",
            "color": "#ff0090"
        },
        "boundingBox": [
            {
                "lat": 49.9844314403,
                "lng": 14.3591213607
            },
            {
                "lat": 50.1262941842,
                "lng": 14.5812935137
            }
        ],
        "zones": [
            {
                "id": 1000004,
                "boundingBox": [
                    {
                        "lat": 50.1087266492,
                        "lng": 14.4608598632
                    },
                    {
                        "lat": 50.1089810158,
                        "lng": 14.4612564861
                    }
                ],
                "points": [
                    [50.1089810158, 14.4612564861],
                    [50.1087266492, 14.4612564861],
                    [50.1087266492, 14.4608598632],
                    [50.1089810158, 14.4608598632],
                    [50.1089810158, 14.4612564861]
                ]
            }
        ]
    }
]
```

-   frekvence stahování
    -   [dev](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/cluster_development/golemio_golemio-values.yaml#L381)
        -   `0 25 7,13,19,1 * * *`, tzn. vždy v 7:25, 13:25, 19:25 a 1:25
    -   [prod](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/cluster_production/golemio_golemio-values.yaml#L379)
        -   `0 25 7,13,19,1 * * *`, tzn. vždy v 7:25, 13:25, 19:25 a 1:25
-   název rabbitmq fronty
    -   `dataplatform.rekolavehiclesharing.refreshRekolaGeofencingData`
    -   bez parametrů

#### Datový zdroj _HoppyGo_

-   zdroj dat
    -   [HoppyGo](https://0csrj8wr3j.execute-api.eu-west-1.amazonaws.com/api/v3/external/my-prague/fdh98742ljfsd9fsd89H238nvuq)
    -   parametry nejsou
-   formát dat

    -   HTTP GET
    -   json
    -   [validační schéma](../src/schema-definitions/datasources/HoppyGoVehiclesJsonSchema.ts)
    -   [ukázka dat](../test/integration-engine/data/hoppygo-datasource.json)

-   název rabbitmq fronty
    -   `dataplatform.hoppygovehiclesharing.refreshHoppyGoSharedCarsData`
    -   bez parametrů
    -   msg ttl 2 minuty

#### Datový zdroj _Český Carsharing_

-   zdroj dat: 2 endpointy
    -   [vozidla](https://api.ceskycarsharing.cz/iis_cs_api/IIS_API.svc/help/operations/getAllCarsIct)
    -   [polygony](https://api.ceskycarsharing.cz/iis_cs_api_demo/IIS_API.svc/getPolygonsIct)
    -   přístupné pouze z vybraných IP adres
    -   hlavička: `Content-Type: application/json`
    -   request body obsahuje JSON objekt s atributy `guid` a `password`:

```
{
    guid: "asdf-1234",
    password: "loremIpsum"
}
```

-   formát dat:

    -   HTTP POST
    -   JSON
    -   [validační schéma vozidel](../src/schema-definitions/datasources/CeskyCarsharingVehiclesJsonSchema.ts)
    -   [validační schéma polygonů](../src/schema-definitions/datasources/CeskyCarsharingGeofencingJsonSchema.ts)
    -   [ukázka dat vozidel](../test/integration-engine/data/cesky-carsharing-cars-datasource.json)
    -   [ukázka dat polygonů](../test/integration-engine/data/cesky-carsharing-cars-datasource.json)

-   rabbitmq fronty:
    -   vozidla: `dataplatform.ceskycarsharingvehiclesharing.refreshCeskyCarsharingVehiclesData`
    -   polygony: `dataplatform.ceskycarsharingvehiclesharing.refreshCeskyCarsharingGeofencingData`
    -   bez parametrů
    -   msg ttl 2 minuty
    -   frekvence stahování: `0 */2 * * * *`, tzn. každé 2 minuty

#### Datový zdroj _MobilityOperator_

Stahuje se při transformaci operátorů. Není samostatný operátor, jen se jím obohacují data.

-   formát dat:
    -   HTTP GET
    -   json
    -   [validační schéma](../src/schema-definitions/datasources/MobilityOperator.ts)
    -   [ukázka dat](../test/integration-engine/data/MobilityOperator.json)

#### Datový zdroj _IPT blob storage_

-   zdroj dat
    -   url: config.datasources.vehiclesharing.staticData.baseUrl
    -   parametry dotazu: config.datasources.vehiclesharing.staticData.resources
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [iptBlobStorageJsonSchema](../src/schema-definitions/datasources/staticData/IptBlobStorageJsonSchema.ts)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.vehiclesharingstaticdata.fetchStaticDataFromRemoteSource
            -   rabin `0 58 6 * * *`
            -   prod `0 58 6 * * *`
-   názvy rabbitmq front
    -   dataplatform.vehiclesharingstaticdata.fetchStaticDataFromRemoteSource
    -   dataplatform.vehiclesharingstaticdata.refreshStaticDataInDb

## Zpracování dat / transformace

Transformace mapují data na GBFS formát. Data se obohacují ze zdroje MobilityOperator. Z něj se nahrazuje rental_apps
v system_information daného poskytovatele, pokud ve zdroji MobilityOperator existuje.

-   Rekola: 1 worker, stahuje, transformuje a promazává stará data
-   Nextbike: 1 worker, stahuje, transformuje a promazává stará data
-   HoppyGo: 1 worker, stahuje, transformuje a promazává stará data
-   Český carsharing: 2 workery, jeden na vozidla (stahuje, transformuje a promazává stará data), jeden na zóny (polygony)

Interní RabbitMQ fronty jsou popsány v [AsyncAPI](./asyncapi.yaml).

### Worker _RekolaSharedBikesWorker_

[Odkaz](../src/integration-engine/RekolaSharedBikesWorker.ts). Obsahuje dvě metody, která transformuje a ukládá data psql. Stará data maže.

#### Metoda _refreshRekolaTrackableData_

-   vstupní rabbitmq fronta
    -   `dataplatform.rekolasharedbikes.refreshRekolaTrackableData`
    -   bez parametrů
-   datové zdroje
    -   `Rekola - trackable data` (viz výše)
-   transformace
    -   [`RekolaVehicleStatusTransformation`](../src/integration-engine/transformations/Rekola/RekolaVehicleStatusTransformation.ts)
    -   [`RekolaStationInformationTransformation`](../src/integration-engine/transformations/Rekola/RekolaStationInformationTransformation.ts)
    -   [`RekolaStationStatusTransformation`](../src/integration-engine/transformations/Rekola/RekolaStationStatusTransformation.ts)
    -   [`RekolaStationStatusVehicleTypeTransformation`](../src/integration-engine/transformations/Rekola/RekolaStationStatusVehicleTypeTransformation.ts)
    -   [`RekolaVehicleTypeTransformation`](../src/integration-engine/transformations/Rekola/RekolaVehicleTypeTransformation.ts)
    -   transformace pro uložení dynamických dat do GBFS formátu
-   data modely
    -   [VehicleTypesModel](../src/integration-engine/models/VehicleTypesModel.ts)
    -   [StationInformationHistoryModel](../src/integration-engine/models/StationInformationHistoryModel.ts)
    -   [StationStatusVehicleTypesModel](../src/integration-engine/models/StationStatusVehicleTypeModel.ts)
    -   [VehicleStatusModel](../src/integration-engine/models/VehicleStatusModel.ts)
    -   (viz níže)

#### Metoda _refreshRekolaGeofencingData_

-   vstupní rabbitmq fronta
    -   `dataplatform.rekolasharedbikes.refreshRekolaGeofencingData`
    -   bez parametrů
-   datové zdroje
    -   `Rekola - geofencing data` (viz výše)
-   transformace
    -   [`RekolaGeofencingTransformation`](../src/integration-engine/transformations/RekolaGeofencingTransformation.ts)
    -   transformace pro uložení statických dat do GBFS formátu
-   data modely
    -   [GeofencingZonesModel](../src/integration-engine/models/GeofencingZonesModel.ts)
    -   (viz níže)

### Worker _NextbikeSharedBikesWorker_

[Odkaz](../src/integration-engine/NextbikeSharedBikesWorker.ts)
Obsahuje jednu public metodu, která stáhne data ze všech zdrojů, zavolá transformery všech tabulek a uloží data a smaže stará.

### Worker HoppyGo: _HoppyGoSharedCarsWorker_

-   V jedné public metodě Stahuje, transformuje, ukládá data o vozidlech HoppyGo a maže stará data.
-   Předpokládá všechny ceny ve feedu v CZK a všechna auta dostupná k zapůjčení v ČR.
-   Statická data se ukládají jen když v db chybí.
-   Žádná data nejsou v SQL migracích, vše je v jednotlivých transformerech.

### StaticDataWorker

Načítá statická data z IPT blob storage a ukládá je do databáze. Statická data se starším updatedAt než doba posledního importu jsou mazána.

#### task _FetchStaticDataFromRemoteSourceTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.vehiclesharingstaticdata.fetchStaticDataFromRemoteSource
    -   TTL: 59 minut
    -   bez parametrů
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: dataplatform.vehiclesharingstaticdata.refreshStaticDataInDb
-   datové zdroje
    -   IPT blob storage
-   data modely
    -   StaticDataModel `static_data`

#### task _RefreshStaticDataInDbTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.vehiclesharingstaticdata.refreshStaticDataInDb
    -   TTL: 59 minut
    -   bez parametrů
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   IPT blob storage
-   data modely
    -   StaticDataModel `static_data`

## Uložení dat

Před uložením odfiltrujeme data mimo ČR pro bike status a station status. K tomu se používá nejdříve jednoduchý polygon uvnitř ČR složený z pár bodů a poté [polygon pro ČR](../staticData/geojson/czechia-border10km.geojson).
Data se ukládají do postgresql. Struktura databáze by měla vycházet ze standardu GBFS.

### Obecné

-   typ databáze
    -   PSQL
-   datábázové schéma
    -   [diagram](./db_diagram.md)
    -   PSQL
        -   schéma `vehiclesharing`
        -   tabulky:
            -   vehicle_status
            -   geofencing_zones
            -   pricing_plan_payment
            -   pricing_plans
            -   pricings
            -   rental_apps
            -   station_information
            -   station_status
            -   station_status_vehicle_type
            -   system_information
            -   vehicle_types
            -   static_data
        -   migrační skripty [složka](../db/migrations/)
        -   example data [složka](../db/example/) (slouží i pro testy)
-   retence dat
    -   dynamická data se promazávají při každém běhu workeru providera

### _PSQL_ model

-   tabulka `vehiclesharing.vehicle_status`
    -   struktura [VehicleStatus](../src/schema-definitions/definitions/VehicleStatus.ts)
-   tabulka `vehiclesharing.geofencing_zones`
    -   struktura [GeofencingZones](../src/schema-definitions/definitions/GeofencingZones.ts)
-   tabulka `vehiclesharing.pricing_plan_payment`
    -   pouze statická data naplněná migračním skriptem
-   tabulka `vehiclesharing.pricing_plans`
    -   struktura [PricingPlans](../src/schema-definitions/definitions/PricingPlans.ts)
    -   statická data naplněná migračním skriptem
-   tabulka `vehiclesharing.pricings`
    -   struktura [Pricings](../src/schema-definitions/definitions/Pricings.ts)
    -   statická data naplněná migračním skriptem
-   tabulka `vehiclesharing.rental_apps`
    -   struktura [RentalApps](../src/schema-definitions/definitions/RentalApps.ts)
    -   statická data naplněná migračním skriptem
-   tabulka `vehiclesharing.station_information`
    -   struktura [StationInformation](../src/schema-definitions/definitions/StationInformation.ts)
-   tabulka `vehiclesharing.station_status`
    -   struktura [StationStatus](../src/schema-definitions/definitions/StationStatus.ts)
-   tabulka `vehiclesharing.station_status_vehicle_type`
    -   struktura [StationStatusVehicleType](../src/schema-definitions/definitions/StationStatusVehicleType.ts)
-   tabulka `vehiclesharing.system_information`
    -   struktura [SystemInformation](../src/schema-definitions/definitions/SystemInformation.ts)
    -   statická data naplněná migračním skriptem
-   tabulka `vehiclesharing.vehicle_types`
    -   struktura [VehicleTypes](../src/schema-definitions/definitions/VehicleTypes.ts)
-   tabulka `vehiclesharing.static_data`
    -   struktura [StaticDataModel](../src/schema-definitions/definitions/models/StaticDataModel.ts)

## Output API

K dispozici je standardní output geojson api a GBFS api.

**U výsledných GBFS objektů probíhá odmazávání prázdných nebo nullový atributů.**

### Obecné

-   Zdroj pro api
    -   PSQL databáze viz výše
-   API Blueprint / OpenAPI dokumentace
    -   [OpenAPI](./openapi.yaml)
-   veřejné / neveřejné endpointy
    -   částečně veřejná na úrovni filtrování podle poskytovatele dat
-   postman kolekce
    -   TBD

#### _/sharedbikes_

-   zdrojové tabulky
    -   bike_status
    -   system_information
    -   vehicle_type
-   nestandardní dodatečná transformace
    -   transformace z GBFS struktury na GeoJSON
-   query parametry
    -   latlng
    -   range
    -   limit
    -   offset
    -   companyName = `system_information.name`
    -   updatedSince
-   filtr dat
    -   filtrovány pouze data typu `bicycle` a `shared_moped` (z tabulky vehicle_type)

#### _/sharedbikes/{id}_

-   zdrojové tabulky
    -   bike_status
    -   system_information
    -   vehicle_type
-   nestandardní dodatečná transformace
    -   transformace z GBFS struktury na GeoJSON
-   url parametr
    -   id = `bike_status.id`

#### _/sharedbikes/gbfs/systems_list_

-   zdrojové tabulky
    -   system_information

#### _/sharedbikes/gbfs/:system_id/gbfs_

-   zdrojové tabulky
    -   system_information
-   url parametr
    -   system_id = `system_information.system_id`
-   **data jsou staticky v kódu**

#### _/sharedbikes/gbfs/:system_id/gbfs_versions_

-   zdrojové tabulky
    -   system_information
-   url parametr
    -   system_id = `system_information.system_id`
-   **data jsou staticky v kódu**

#### _/sharedbikes/gbfs/:system_id/system_information_

-   zdrojové tabulky
    -   system_information
    -   rental_apps
-   url parametr
    -   system_id = `system_information.system_id`

#### _/sharedbikes/gbfs/:system_id/free_bike_status_

-   zdrojové tabulky
    -   bike_status
    -   system_information
    -   rental_apps
    -   vehicle_types
    -   pricing_plans
-   url parametr
    -   system_id = `system_information.system_id`

#### _/sharedbikes/gbfs/:system_id/vehicle_types_

-   zdrojové tabulky
    -   bike_status
    -   vehicle_types
-   url parametr
    -   system_id = `system_information.system_id`

#### _/sharedbikes/gbfs/:system_id/station_information_

-   zdrojové tabulky
    -   station_information
    -   rental_apps
-   url parametr
    -   system_id = `system_information.system_id`

#### _/sharedbikes/gbfs/:system_id/station_status_

-   zdrojové tabulky
    -   station_status
    -   station_status_vehicle_type
    -   bike_status
-   url parametr
    -   system_id = `system_information.system_id`

#### _/sharedbikes/gbfs/:system_id/system_pricing_plans_

-   zdrojové tabulky
    -   pricing_plans
    -   pricings
-   url parametr
    -   system_id = `system_information.system_id`

#### _/sharedbikes/gbfs/:system_id/geofencing_zones_

-   zdrojové tabulky
    -   geofencing_zones
-   url parametr
    -   system_id = `system_information.system_id`

#### _/sharedcars_

-   **náhrada modulu SharedCars**
-   zdrojové tabulky
    -   bike_status
    -   system_information
    -   vehicle_type
-   nestandardní dodatečná transformace
    -   transformace z GBFS struktury na GeoJSON
-   query parametry
    -   latlng
    -   range
    -   limit
    -   offset
    -   companyNames = `system_information.name`
    -   updatedSince
-   filtr dat
    -   filtrovány pouze data typu `car` (z tabulky vehicle_type)

#### _/sharedcars/{id}_

-   **náhrada modulu SharedCars**
-   zdrojové tabulky
    -   bike_status
    -   system_information
    -   vehicle_type
-   nestandardní dodatečná transformace
    -   transformace z GBFS struktury na GeoJSON
-   url parametr
    -   id = `bike_status.id`

#### _mvts/free_vehicle_status_

-   očekává hlavičku Accept `application/vnd.mapbox-vector-tile`, nebo `application/json`
-   akceptuje pouze charset utf-8 (`application/json; charset=utf-8`)
