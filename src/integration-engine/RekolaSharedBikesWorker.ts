import { RekolaGeofencingZonesDataSourceFactory } from "#ie/dataSources/RekolaGeofencingZonesDataSourceFactory";
import { RekolaTrackablesDataSourceFactory } from "#ie/dataSources/RekolaTrackablesDataSourceFactory";
import { OldDataCleanerHelper } from "#ie/helpers/OldDataCleanerHelper";
import { ModuleContainerToken, SharedVehiclesContainer } from "#ie/ioc";
import { REKOLA_PREFIX, REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola";
import { RekolaStationInformationTransformation } from "#ie/transformations/Rekola/RekolaStationInformationTransformation";
import { RekolaStationStatusTransformation } from "#ie/transformations/Rekola/RekolaStationStatusTransformation";
import { RekolaStationStatusVehicleTypeTransformation } from "#ie/transformations/Rekola/RekolaStationStatusVehicleTypeTransformation";
import { RekolaVehicleStatusTransformation } from "#ie/transformations/Rekola/RekolaVehicleStatusTransformation";
import { RekolaVehicleTypeTransformation } from "#ie/transformations/Rekola/RekolaVehicleTypeTransformation";
import {
    IRekolaTrackablesDatasource,
    ISharedVehiclesStationInformationOutput,
    ISharedVehiclesStationStatusOutput,
    ISharedVehiclesStationStatusVehicleTypeOutput,
    ISharedVehicleStatusOutput,
    ISharedVehicleTypeOutput,
} from "#sch";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { GeofencingZonesModel } from "./models/GeofencingZonesModel";
import { StationInformationModel } from "./models/StationInformationModel";
import { StationStatusModel } from "./models/StationStatusModel";
import { StationStatusVehicleTypeModel } from "./models/StationStatusVehicleTypeModel";
import { VehicleStatusModel } from "./models/VehicleStatusModel";
import { VehicleTypesModel } from "./models/VehicleTypesModel";
import { RekolaGeofencingTransformation } from "./transformations";

interface IRekolaTransformed {
    stationInformation: ISharedVehiclesStationInformationOutput[];
    stationStatus: ISharedVehiclesStationStatusOutput[];
    bikeStatus: ISharedVehicleStatusOutput[];
    vehicleTypes: ISharedVehicleTypeOutput[];
    stationStatusVehicleTypes: ISharedVehiclesStationStatusVehicleTypeOutput[];
}

export class RekolaSharedBikesWorker extends BaseWorker {
    private zonesDatasource: DataSource;
    private trackablesDatasource: DataSource;
    private vehicleTypesModel: VehicleTypesModel;
    private stationInformationModel: StationInformationModel;
    private stationStatusModel: StationStatusModel;
    private vehicleStatusModel: VehicleStatusModel;
    private geofencingZonesModel: GeofencingZonesModel;
    private stationStatusVehicleTypeModel: StationStatusVehicleTypeModel;
    private geofencingTransformation: RekolaGeofencingTransformation;
    private bikeStatusTransformation: RekolaVehicleStatusTransformation;
    private stationInformationTransformation: RekolaStationInformationTransformation;
    private stationStatusTransformation: RekolaStationStatusTransformation;
    private vehicleTypeTransformation: RekolaVehicleTypeTransformation;
    private stationStatusVehicleTypeTransformation: RekolaStationStatusVehicleTypeTransformation;
    private oldDataCleanerHelper: OldDataCleanerHelper;
    private postgresConnector: IDatabaseConnector;

    constructor() {
        super();
        this.zonesDatasource = RekolaGeofencingZonesDataSourceFactory.getDataSource();
        this.trackablesDatasource = RekolaTrackablesDataSourceFactory.getDataSource();
        this.vehicleTypesModel = new VehicleTypesModel();
        this.stationInformationModel = new StationInformationModel();
        this.stationStatusModel = new StationStatusModel();
        this.vehicleStatusModel = new VehicleStatusModel();
        this.geofencingZonesModel = new GeofencingZonesModel();
        this.stationStatusVehicleTypeModel = new StationStatusVehicleTypeModel();
        this.geofencingTransformation = new RekolaGeofencingTransformation();
        this.stationInformationTransformation = new RekolaStationInformationTransformation();
        this.stationStatusTransformation = new RekolaStationStatusTransformation();
        this.bikeStatusTransformation = new RekolaVehicleStatusTransformation();
        this.vehicleTypeTransformation = new RekolaVehicleTypeTransformation();
        this.stationStatusVehicleTypeTransformation = new RekolaStationStatusVehicleTypeTransformation();
        this.oldDataCleanerHelper = SharedVehiclesContainer.resolve<OldDataCleanerHelper>(
            ModuleContainerToken.OldDataCleanerHelper
        );
        this.postgresConnector = SharedVehiclesContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
    }

    /**
     * Worker method - fetch new Rekola geofencing data
     */
    public refreshRekolaGeofencingData = async (): Promise<void> => {
        try {
            const data = await this.zonesDatasource.getAll();
            const zones = await this.geofencingTransformation.transform(data);
            await this.geofencingZonesModel.replace(zones, REKOLA_SYSTEM_ID);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while refreshing geofencing data", this.constructor.name, err);
            }
        }
    };

    /**
     * Worker method - fetch new Rekola trackable data
     */
    public refreshRekolaTrackableData = async (): Promise<void> => {
        const transformationDate = new Date();

        try {
            const trackables: IRekolaTrackablesDatasource = await this.trackablesDatasource.getAll();
            const data = { trackables, transformationDate };

            const transformed: IRekolaTransformed = {
                stationInformation: await this.stationInformationTransformation.transform(data),
                stationStatus: await this.stationStatusTransformation.transform(data),
                bikeStatus: await this.bikeStatusTransformation.transform(data),
                vehicleTypes: await this.vehicleTypeTransformation.transform(trackables),
                stationStatusVehicleTypes: await this.stationStatusVehicleTypeTransformation.transform(trackables.racks),
            };

            await this.saveData(transformed);

            await this.stationStatusVehicleTypeModel.deleteOrphans(REKOLA_PREFIX);
            await this.oldDataCleanerHelper.deleteOldTrackableData(transformationDate, REKOLA_SYSTEM_ID);
            await this.vehicleTypesModel.deleteDataBefore(transformationDate, REKOLA_SYSTEM_ID);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while refreshing trackable data: " + err.message, this.constructor.name, err);
            }
        }
    };

    private saveData = async (data: IRekolaTransformed): Promise<void> => {
        const connection = await this.postgresConnector.getConnection();
        const transaction = await connection.transaction();

        try {
            await this.vehicleTypesModel.bulkSave(data.vehicleTypes, undefined, false, false, transaction);
            await this.stationInformationModel.saveIfInCzechia(data.stationInformation, transaction);
            await this.stationStatusModel.bulkSave(data.stationStatus, undefined, false, false, transaction);
            await this.stationStatusVehicleTypeModel.bulkSave(
                data.stationStatusVehicleTypes,
                undefined,
                false,
                false,
                transaction
            );
            await this.vehicleStatusModel.saveIfInCzechia(data.bikeStatus, transaction);
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw err;
        }
    };
}
