import { CeskyCarsharingGeofencingDataSourceFactory } from "#ie/dataSources/CeskyCarsharingGeofencingDataSourceFactory";
import { BaseWorker, DataSource } from "@golemio/core/dist/integration-engine";

import { ModuleContainerToken, SharedVehiclesContainer } from "#ie/ioc";
import { CeskyCarsharingGeofencingTransformation } from "#ie/transformations";
import { ICeskyCarsharingGeofencingJson, ICeskyCarsharingPolygon } from "#sch/datasources/CeskyCarsharingGeofencingZone";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { GeofencingZonesModel } from "./models/GeofencingZonesModel";

export class CeskyCarsharingGeofencingWorker extends BaseWorker {
    protected readonly name = "CeskyCarsharingGeofencingWorker";

    private dataSource: DataSource;
    private geofencingZonesModel: GeofencingZonesModel;
    private geofencingTransformation: CeskyCarsharingGeofencingTransformation;

    constructor() {
        super();
        this.dataSource = CeskyCarsharingGeofencingDataSourceFactory.getDataSource();
        this.geofencingZonesModel = SharedVehiclesContainer.resolve<GeofencingZonesModel>(
            ModuleContainerToken.GeofencingZonesModel
        );
        this.geofencingTransformation = SharedVehiclesContainer.resolve<CeskyCarsharingGeofencingTransformation>(
            ModuleContainerToken.CeskyCarsharingGeofencingTransformation
        );
    }

    public refreshCeskyCarsharingGeofencingData = async (): Promise<void> => {
        try {
            const zones = await this.getGeofencingZonesFromDataSource();
            const transformedData = await this.geofencingTransformation.transform(zones);
            await this.geofencingZonesModel.save(transformedData);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error refreshing CeskyCarsharing geofencing data", this.constructor.name, err);
            }
        }
    };

    private async getGeofencingZonesFromDataSource(): Promise<ICeskyCarsharingPolygon[]> {
        const allData: ICeskyCarsharingGeofencingJson = await this.dataSource.getAll();
        return allData.polygons;
    }
}
