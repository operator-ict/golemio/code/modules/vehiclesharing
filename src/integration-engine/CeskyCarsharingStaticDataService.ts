import { ModuleContainerToken } from "#ie/ioc";
import {
    ANYTIME_SYSTEM_ID,
    AUTONAPUL_SYSTEM_ID,
    BOLT_SYSTEM_ID,
    CAR4WAY_SYSTEM_ID,
} from "#ie/transformations/CeskyCarsharing/transformConstants";
import { getRentalApps } from "#ie/transformations/CeskyCarsharing/transformRentalApps";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { ISharedVehiclesRentalAppOutput, ISharedVehiclesSystemInformationOutput } from "#sch";
import { IMobilityOperatorRentalAppMap } from "#sch/datasources/MobilityOperator";
import { ResourceTypeDict } from "#sch/definitions/models/interfaces/IStaticDataOutputDto";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { PricingModel } from "./models/PricingModel";
import { PricingPlansModel } from "./models/PricingPlansModel";
import { RentalAppsModel } from "./models/RentalAppsModel";
import { SystemInformationModel } from "./models/SystemInformationModel";
import { VehicleTypesModel } from "./models/VehicleTypesModel";
import { Transaction } from "@golemio/core/dist/shared/sequelize";

@injectable()
export class CeskyCarsharingStaticDataService {
    // eslint-disable-next-line max-len
    private readonly ceskyCarsharingSystemIds = [ANYTIME_SYSTEM_ID, AUTONAPUL_SYSTEM_ID, CAR4WAY_SYSTEM_ID, BOLT_SYSTEM_ID];
    constructor(
        @inject(ModuleContainerToken.MobilityOperatorRentalAppProvider)
        private rentalAppProvider: MobilityOperatorRentalAppProvider,
        @inject(ModuleContainerToken.RentalAppsModel) private rentalAppsModel: RentalAppsModel,
        @inject(ModuleContainerToken.VehicleTypesModel)
        private vehicleTypesModel: VehicleTypesModel,
        @inject(ModuleContainerToken.SystemInformationModel) private systemInformationModel: SystemInformationModel,
        @inject(ModuleContainerToken.PricingModel) private pricingModel: PricingModel,
        @inject(ModuleContainerToken.PricingPlansModel) private pricingPlansModel: PricingPlansModel,
        @inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector
    ) {}

    public async saveSystemsStaticData(staticData: Partial<ResourceTypeDict>): Promise<void> {
        const processedAtDate = new Date();

        const connection = this.databaseConnector.getConnection();
        const transaction = await connection.transaction();

        const staticApps = getRentalApps();
        const mobilityOperatorApps = await this.rentalAppProvider.getMobilityOperatorRentalApps();

        const systems = staticData.systemInformation;
        if (systems === undefined) {
            throw new GeneralError("System information is undefined", this.constructor.name);
        }

        const { vehicleTypes, pricings, pricingPlans } = staticData;

        if (vehicleTypes === undefined || pricings === undefined || pricingPlans === undefined) {
            throw new GeneralError("Vehicle types, pricings or pricing plans are undefined", this.constructor.name);
        }

        const { appsToSave, systemsToSave } = this.createAppAndSystemInformationData(systems, mobilityOperatorApps, staticApps);
        try {
            await this.rentalAppsModel.bulkSave(appsToSave, undefined, false, false, transaction);
            await this.vehicleTypesModel.bulkSave(vehicleTypes, undefined, false, false, transaction);

            await this.systemInformationModel.bulkSave(systemsToSave, undefined, false, false, transaction);

            await this.pricingPlansModel.bulkSave(pricingPlans, undefined, false, false, transaction);
            await this.pricingModel.bulkSave(pricings, undefined, false, false, transaction);

            await this.cleanOldData(processedAtDate, transaction);
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw new GeneralError(
                `Database error: CeskyCarsharingStaticDataService failed to save static data`,
                "CeskyCarsharingStaticDataService",
                err
            );
        }
    }

    public async cleanOldData(processedAtDate: Date, t: Transaction): Promise<void> {
        for (let systemId of this.ceskyCarsharingSystemIds) {
            await this.pricingModel.deleteDataBefore(processedAtDate, systemId, t);
            await this.pricingPlansModel.deleteDataBefore(processedAtDate, systemId, t);
            await this.vehicleTypesModel.deleteDataBefore(processedAtDate, systemId, t);
        }
    }

    private createAppAndSystemInformationData(
        systems: ISharedVehiclesSystemInformationOutput[],
        mobilityOperatorApps: IMobilityOperatorRentalAppMap,
        staticApps: IMobilityOperatorRentalAppMap
    ): { appsToSave: ISharedVehiclesRentalAppOutput[]; systemsToSave: ISharedVehiclesSystemInformationOutput[] } {
        let appsToSave: ISharedVehiclesRentalAppOutput[] = [];
        let systemsToSave: ISharedVehiclesSystemInformationOutput[] = [];

        for (let system of systems) {
            if (mobilityOperatorApps[system.system_id] !== undefined) {
                system.rental_app_id = mobilityOperatorApps[system.system_id].id;
                appsToSave.push(mobilityOperatorApps[system.system_id]);
            } else {
                appsToSave.push(staticApps[system.system_id]);
            }
            systemsToSave.push(system);
        }

        return { appsToSave, systemsToSave };
    }
}
