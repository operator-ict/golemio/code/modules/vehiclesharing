import { injectable } from "@golemio/core/dist/shared/tsyringe";
import fs from "fs";
import { isPointInPolygon } from "geolib";
import { GeolibInputCoordinates } from "geolib/es/types";
import path from "path";

@injectable()
export class GeoFilter {
    private readonly RELATIVE_PATH_GEOJSON = "../../../staticData/geojson/";
    private czechiaCoordinates: GeolibInputCoordinates[];
    private czechiaPreFilterCoordinates: GeolibInputCoordinates[];

    constructor() {
        this.czechiaPreFilterCoordinates = this.loadGeoCoordinates("czechia-preFilter.geojson");
        this.czechiaCoordinates = this.loadGeoCoordinates("czechia-border10km.geojson");
    }

    public isInCzechia = (lat: number, lon: number): boolean => {
        const point = { latitude: lat, longitude: lon };

        return isPointInPolygon(point, this.czechiaPreFilterCoordinates) || isPointInPolygon(point, this.czechiaCoordinates);
    };

    private loadGeoCoordinates(fileName: string) {
        const pathToGeoJson = path.join(__dirname, this.RELATIVE_PATH_GEOJSON, fileName);
        const czechiaMultiPolygon = JSON.parse(fs.readFileSync(pathToGeoJson).toString()).features[0].geometry;

        return czechiaMultiPolygon.coordinates[0][0] as GeolibInputCoordinates[];
    }
}
