import { ModuleContainerToken } from "#ie/ioc";
import { RentalAppsModel } from "#ie/models/RentalAppsModel";
import { StationInformationHistoryModel } from "#ie/models/StationInformationHistoryModel";
import { VehicleStatusModel } from "#ie/models/VehicleStatusModel";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
@injectable()
export class OldDataCleanerHelper {
    constructor(
        @inject(ModuleContainerToken.RentalAppsModel) private rentalAppsModel: RentalAppsModel,
        @inject(ModuleContainerToken.StationInformationHistoryModel)
        private stationInformationHistoryModel: StationInformationHistoryModel,
        @inject(ModuleContainerToken.VehicleStatusModel) private vehicleStatusModel: VehicleStatusModel
    ) {}

    /**
     * Delete data of a given system processed before a given date/time
     *
     * @param processedAt The `processed_at` date/time, prior to which the data should be deleted
     * @param systemId The ID of the system to delete data from
     */
    public async deleteOldTrackableData(processedAt: Date, systemId: string): Promise<void> {
        await this.stationInformationHistoryModel.deleteDataBefore(processedAt, systemId);
        await this.vehicleStatusModel.deleteDataBefore(processedAt, systemId);
        await this.rentalAppsModel.deleteOldData();
    }
}
