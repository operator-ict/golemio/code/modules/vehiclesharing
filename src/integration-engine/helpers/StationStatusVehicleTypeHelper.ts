import {
    ISharedVehicleStatusOutput,
    ISharedVehiclesStationStatusOutput,
    ISharedVehiclesStationStatusVehicleTypeOutput,
} from "#sch";

type VehicleCount = { count: number; reservedCount: number };

/** A `Map` which maps vehicle count by the id of the vehicle type */
type VehicleCountByVehicleType = Map<string, VehicleCount>;
/** A `Map` which maps vehicle count by the id of the station and then by the id of the vehicle type (using a nested `Map`) */
type VehicleCountByStationAndVehicleType = Map<string, VehicleCountByVehicleType>;

export default class StationStatusVehicleTypeHelper {
    /**
     * Create `station_status_vehicle_type` data by mapping data from other transformation results
     *
     * @param stationStatus `station_status` transformation results to use for data mapping
     * @param vehicleStatus `free_bike_status` transformation results to use for data mapping
     * @param transformationDate The date/time of the transformation
     */
    public static mapTransformationResults(
        stationStatus: ISharedVehiclesStationStatusOutput[],
        vehicleStatus: ISharedVehicleStatusOutput[],
        transformationDate: Date
    ): ISharedVehiclesStationStatusVehicleTypeOutput[] {
        const vehicleCountByStationAndVehicleType = this.getVehicleCountByStationAndVehicleType(vehicleStatus);
        const stationStatusVehicleType: ISharedVehiclesStationStatusVehicleTypeOutput[] = [];
        for (const station of stationStatus) {
            stationStatusVehicleType.push(
                ...this.getStationStatusVehicleTypeByStation(station, vehicleCountByStationAndVehicleType, transformationDate)
            );
        }
        return stationStatusVehicleType;
    }

    private static getVehicleCountByStationAndVehicleType(
        vehicles: ISharedVehicleStatusOutput[]
    ): VehicleCountByStationAndVehicleType {
        const vehicleCountByStationAndVehicleType: VehicleCountByStationAndVehicleType = new Map();
        for (const vehicle of vehicles) {
            this.updateVehicleCountByStationAndVehicleType(vehicleCountByStationAndVehicleType, vehicle);
        }
        return vehicleCountByStationAndVehicleType;
    }

    private static updateVehicleCountByStationAndVehicleType(
        vehicleCountByStationAndVehicleType: VehicleCountByStationAndVehicleType,
        vehicle: ISharedVehicleStatusOutput
    ): void {
        if (vehicle.station_id === null) return;
        if (!vehicleCountByStationAndVehicleType.has(vehicle.station_id)) {
            vehicleCountByStationAndVehicleType.set(vehicle.station_id, new Map());
        }
        const vehicleCountByVehicleType = vehicleCountByStationAndVehicleType.get(vehicle.station_id)!;
        let vehicleCount = vehicleCountByVehicleType.get(vehicle.vehicle_type_id);
        if (!vehicleCount) {
            vehicleCount = { count: 0, reservedCount: 0 };
        }
        vehicleCount.count = vehicleCount.count + 1;

        if (vehicle.is_reserved) {
            vehicleCount.reservedCount = vehicleCount.reservedCount + 1;
        }

        vehicleCountByVehicleType.set(vehicle.vehicle_type_id, vehicleCount);
    }

    private static getStationStatusVehicleTypeByStation(
        station: ISharedVehiclesStationStatusOutput,
        vehicleCountByStationAndVehicleType: VehicleCountByStationAndVehicleType,
        transformationDate: Date
    ): ISharedVehiclesStationStatusVehicleTypeOutput[] {
        const vehicleCountByVehicleType = vehicleCountByStationAndVehicleType.get(station.station_id);
        const stationStatusVehicleType: ISharedVehiclesStationStatusVehicleTypeOutput[] = [];
        for (const [vehicleTypeId, vehicleCount] of vehicleCountByVehicleType?.entries() ?? []) {
            stationStatusVehicleType.push(
                this.createStationStatusVehicleType(station, vehicleTypeId, vehicleCount, transformationDate)
            );
        }
        return stationStatusVehicleType;
    }

    private static createStationStatusVehicleType = (
        station: ISharedVehiclesStationStatusOutput,
        vehicleTypeId: string,
        vehicleCount: VehicleCount,
        transformationDate: Date
    ): ISharedVehiclesStationStatusVehicleTypeOutput => ({
        station_id: station.station_id,
        vehicle_type_id: vehicleTypeId,
        count: vehicleCount.count,
        num_bikes_available: vehicleCount.count - vehicleCount.reservedCount,
        vehicle_docks_available: station.num_docks_available,
        num_bikes_disabled: null,
        processed_at: transformationDate,
    });
}
