import { Vehiclesharing } from "#sch";
import { config, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class MobilityOperatorDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            Vehiclesharing.datasources.MobilityOperatorJsonSchema.name,
            new HTTPFetchProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.MobilityOperatorUrl,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                Vehiclesharing.datasources.MobilityOperatorJsonSchema.name,
                Vehiclesharing.datasources.MobilityOperatorJsonSchema.jsonSchema
            )
        );
    }
}
