import { INextbikeDefinitionTransformation } from "#ie/transformations/Nextbike";
import { TNextbikeDataSources, Vehiclesharing } from "#sch";
import { NEXT_BIKE_SOURCES } from "#sch/datasources";
import { DataSource, JSONDataTypeStrategy, config } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class NextbikeDataSourceFactory {
    private config = config.datasources.NextbikeSharedBikes;

    public getDataSourceDefinition(sourceId: string): DataSource {
        return new DataSource(
            Vehiclesharing.datasources.nextbikeDefinitionJsonSchema.name + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: this.config.headers,
                method: "GET",
                url: `${this.config.baseUrl}${sourceId}/gbfs.json`,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                Vehiclesharing.datasources.nextbikeDefinitionJsonSchema.name,
                Vehiclesharing.datasources.nextbikeDefinitionJsonSchema.jsonSchema
            )
        );
    }

    public getDataSourceData({ name, url }: INextbikeDefinitionTransformation): DataSource {
        switch (name) {
            case NEXT_BIKE_SOURCES.free_bike_status:
                return this.getDataSource(url, "nextbikeFreeBikeStatusJsonSchema");
            case NEXT_BIKE_SOURCES.station_information:
                return this.getDataSource(url, "nextbikeStationInfoJsonSchema");
            case NEXT_BIKE_SOURCES.station_status:
                return this.getDataSource(url, "nextbikeStationStatusJsonSchema");
            case NEXT_BIKE_SOURCES.system_information:
                return this.getDataSource(url, "nextbikeSystemInfoJsonSchema");
            case NEXT_BIKE_SOURCES.system_pricing_plans:
                return this.getDataSource(url, "nextbikeSystemPricingPlansJsonSchema");
            case NEXT_BIKE_SOURCES.vehicle_types:
                return this.getDataSource(url, "nextbikeVehicleTypeJsonSchema");
            default:
                throw new Error(`Invalid data source type (${name})`);
        }
    }

    private getDataSource(url: string, schemaName: TNextbikeDataSources): DataSource {
        return new DataSource(
            Vehiclesharing.datasources[schemaName].name,
            new HTTPFetchProtocolStrategy({
                headers: this.config.headers,
                method: "GET",
                url,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                Vehiclesharing.datasources[schemaName].name,
                Vehiclesharing.datasources[schemaName].jsonSchema
            )
        );
    }
}
