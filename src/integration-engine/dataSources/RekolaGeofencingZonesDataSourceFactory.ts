import { Vehiclesharing } from "#sch";
import { config, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class RekolaGeofencingZonesDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            Vehiclesharing.datasources.rekolaGeofencingZones.name,
            new HTTPFetchProtocolStrategy({
                headers: config.datasources.RekolaSharedBikesHeaders,
                method: "GET",
                url: config.datasources.RekolaSharedBikesBaseUrl + "/zones",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                Vehiclesharing.datasources.rekolaGeofencingZones.name,
                Vehiclesharing.datasources.rekolaGeofencingZones.jsonSchema
            )
        );
    }
}
