import { Vehiclesharing } from "#sch";
import { config, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class RekolaTrackablesDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            Vehiclesharing.datasources.rekolaTrackables.name,
            new HTTPFetchProtocolStrategy({
                headers: config.datasources.RekolaSharedBikesHeaders,
                method: "GET",
                url:
                    config.datasources.RekolaSharedBikesBaseUrl +
                    "/trackables?mapLat=0&mapLng=0&mapZoom=0&gpsLat=0&gpsLng=0&gpsAcc=0",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                Vehiclesharing.datasources.rekolaTrackables.name,
                Vehiclesharing.datasources.rekolaTrackables.jsonSchema
            )
        );
    }
}
