import { Vehiclesharing } from "#sch";
import { config, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class HoppyGoDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            Vehiclesharing.datasources.HoppyGoVehiclesJsonSchema.name,
            new HTTPFetchProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.HoppyGoSharedCars,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                Vehiclesharing.datasources.HoppyGoVehiclesJsonSchema.name,
                Vehiclesharing.datasources.HoppyGoVehiclesJsonSchema.jsonSchema
            )
        );
    }
}
