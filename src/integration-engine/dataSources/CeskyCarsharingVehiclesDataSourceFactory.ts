import { Vehiclesharing } from "#sch";
import { config, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class CeskyCarsharingVehiclesDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            Vehiclesharing.datasources.CeskyCarsharingVehiclesJsonSchema.name,
            new HTTPFetchProtocolStrategy({
                headers: config.datasources.CeskyCarsharingRequestHeaders,
                method: "POST",
                body: JSON.stringify(config.datasources.CeskyCarsharingRequestBody),
                url: config.datasources.CeskyCarsharingVehiclesApiUrl,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                Vehiclesharing.datasources.CeskyCarsharingVehiclesJsonSchema.name,
                Vehiclesharing.datasources.CeskyCarsharingVehiclesJsonSchema.jsonSchema
            )
        );
    }
}
