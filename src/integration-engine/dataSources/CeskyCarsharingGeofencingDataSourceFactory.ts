import { Vehiclesharing } from "#sch";
import { config, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class CeskyCarsharingGeofencingDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            Vehiclesharing.datasources.CeskyCarsharingGeofencingJsonSchema.name,
            new HTTPFetchProtocolStrategy({
                headers: config.datasources.CeskyCarsharingRequestHeaders,
                method: "POST",
                body: JSON.stringify(config.datasources.CeskyCarsharingRequestBody),
                url: config.datasources.CeskyCarsharingGeofencingApiUrl,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                Vehiclesharing.datasources.CeskyCarsharingGeofencingJsonSchema.name,
                Vehiclesharing.datasources.CeskyCarsharingGeofencingJsonSchema.jsonSchema
            )
        );
    }
}
