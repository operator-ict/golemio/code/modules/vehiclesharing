import { ModuleContainerToken } from "#ie/ioc";
import { IStaticDataHelper } from "#ie/workers/helpers/interfaces/IStaticDataHelper";
import { StaticDataProvider } from "#sch/definitions/models/helpers/StaticDataProviderEnum";
import { StaticDataResourceType } from "#sch/definitions/models/helpers/StaticDataResourceTypeEnum";
import { ResourceTypeDict } from "#sch/definitions/models/interfaces/IStaticDataOutputDto";
import { DataSource, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IStaticDataSourceFactory } from "./interfaces/IStaticDataSourceFactory";
import { iptBlobStorageJsonSchema } from "#sch/datasources/staticData/IptBlobStorageJsonSchema";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";

@injectable()
export class StaticDataSourceFactory implements IStaticDataSourceFactory {
    private static DATASOURCE_NAME = "StaticDataSource";

    constructor(@inject(ModuleContainerToken.StaticDataHelper) private staticDataHelper: IStaticDataHelper) {}

    public getDataSource<T extends StaticDataProvider, K extends StaticDataResourceType>(
        provider: T,
        resourceType: K
    ): IDataSource<ResourceTypeDict[K]> {
        const baseUrl = this.staticDataHelper.getBaseUrl();
        const url = new URL(`${this.getProviderPath(provider)}/${this.getResourceFileName(resourceType)}`, baseUrl);

        return new DataSource<ResourceTypeDict[K]>(
            provider + resourceType + StaticDataSourceFactory.DATASOURCE_NAME,
            this.getProtocolStrategy(url),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator("IPT blob storage", iptBlobStorageJsonSchema)
        );
    }

    private getProviderPath(provider: StaticDataProvider): string {
        switch (provider) {
            case StaticDataProvider.CeskyCarsharing:
                return "CeskyCarsharing";
            case StaticDataProvider.HoppyGo:
                return "HoppyGo";
            case StaticDataProvider.Rekola:
                return "Rekola";
            default:
                throw new GeneralError(`Unknown provider: ${provider}`);
        }
    }

    private getResourceFileName(resourceType: StaticDataResourceType): string {
        switch (resourceType) {
            case StaticDataResourceType.PricingPlans:
                return "pricingPlans.json";
            case StaticDataResourceType.Pricings:
                return "pricings.json";
            case StaticDataResourceType.RentalApps:
                return "rentalApps.json";
            case StaticDataResourceType.SystemInformation:
                return "systemInformation.json";
            case StaticDataResourceType.VehicleTypes:
                return "vehicleTypes.json";
            case StaticDataResourceType.PricingPlanMap:
                return "pricingPlanMap.json";
            default:
                throw new GeneralError(`Unknown resource type: ${resourceType}`);
        }
    }

    private getProtocolStrategy(url: URL): HTTPFetchProtocolStrategy {
        return new HTTPFetchProtocolStrategy({
            method: "GET",
            url: url.toString(),
            headers: {
                "Content-Type": "application/json",
            },
            timeoutInSeconds: 20000, // 20 seconds
        });
    }
}
