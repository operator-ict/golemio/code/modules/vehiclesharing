import { StaticDataProvider } from "#sch/definitions/models/helpers/StaticDataProviderEnum";
import { StaticDataResourceType } from "#sch/definitions/models/helpers/StaticDataResourceTypeEnum";
import { ResourceTypeDict } from "#sch/definitions/models/interfaces/IStaticDataOutputDto";
import { IDataSource } from "@golemio/core/dist/integration-engine/datasources";

export interface IStaticDataSourceFactory {
    getDataSource<T extends StaticDataProvider, K extends StaticDataResourceType>(
        provider: T,
        resourceType: K
    ): IDataSource<ResourceTypeDict[K]>;
}
