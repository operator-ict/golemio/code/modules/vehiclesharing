import { HoppyGoDataSourceFactory } from "#ie/dataSources/HoppyGoDataSourceFactory";
import { OldDataCleanerHelper } from "#ie/helpers/OldDataCleanerHelper";
import { HoppyGoPricingPlansTransformation } from "#ie/transformations/HoppyGo/HoppyGoPricingPlansTransformation";
import { HoppyGoVehicleStatusTransformation } from "#ie/transformations/HoppyGo/HoppyGoVehicleStatusTransformation";
import { HOPPYGO_SYSTEM_ID } from "#ie/transformations/HoppyGo/transformConstants";
import { IHoppyGoVehicle } from "#sch/datasources";
import { BaseWorker, DataSource } from "@golemio/core/dist/integration-engine";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ModuleContainerToken, SharedVehiclesContainer } from "./ioc";
import { PricingPlansModel } from "./models/PricingPlansModel";
import { VehicleStatusModel } from "./models/VehicleStatusModel";

export class HoppyGoSharedCarsWorker extends BaseWorker {
    private dataSource: DataSource;

    private pricingPlansTransformation: HoppyGoPricingPlansTransformation;
    private pricingPlansModel: PricingPlansModel;

    private bikeStatusTransformation: HoppyGoVehicleStatusTransformation;
    private vehicleStatusModel: VehicleStatusModel;
    private oldDataCleanerHelper: OldDataCleanerHelper;

    constructor() {
        super();
        this.dataSource = HoppyGoDataSourceFactory.getDataSource();
        this.pricingPlansTransformation = new HoppyGoPricingPlansTransformation();
        this.pricingPlansModel = new PricingPlansModel();
        this.bikeStatusTransformation = new HoppyGoVehicleStatusTransformation();
        this.vehicleStatusModel = new VehicleStatusModel();
        this.oldDataCleanerHelper = SharedVehiclesContainer.resolve<OldDataCleanerHelper>(
            ModuleContainerToken.OldDataCleanerHelper
        );
    }

    public refreshHoppyGoSharedCarsData = async (): Promise<void> => {
        const transformationDate = new Date();

        try {
            const cars: IHoppyGoVehicle[] = await this.dataSource.getAll();

            const pricingPlans = await this.pricingPlansTransformation.transform({
                cars,
                transformationDate,
            });
            await this.pricingPlansModel.save(pricingPlans);

            const vehicleStatus = await this.bikeStatusTransformation.transform({
                cars,
                transformationDate,
            });
            await this.vehicleStatusModel.saveIfInCzechia(vehicleStatus);

            await this.oldDataCleanerHelper.deleteOldTrackableData(transformationDate, HOPPYGO_SYSTEM_ID);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while refreshing shared car data", this.constructor.name, err);
            }
        }
    };
}
