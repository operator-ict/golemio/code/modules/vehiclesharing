import { CeskyCarsharingVehiclesDataSourceFactory } from "#ie/dataSources/CeskyCarsharingVehiclesDataSourceFactory";
import { OldDataCleanerHelper } from "#ie/helpers/OldDataCleanerHelper";
import { ModuleContainerToken, SharedVehiclesContainer } from "#ie/ioc";
import { CeskyCarsharingVehicleStatusTransformation } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingVehicleStatusTransformation";
import {
    ANYTIME_SYSTEM_ID,
    AUTONAPUL_SYSTEM_ID,
    BOLT_SYSTEM_ID,
    CAR4WAY_SYSTEM_ID,
} from "#ie/transformations/CeskyCarsharing/transformConstants";
import { ICeskyCarsharingCar, ICeskyCarsharingVehicles } from "#sch/datasources";
import { BaseWorker, DataSource } from "@golemio/core/dist/integration-engine";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { VehicleStatusModel } from "./models/VehicleStatusModel";
import { StaticDataRepository } from "#ie/repositories/StaticDataRepository";
import { StaticDataProvider } from "#sch/definitions/models/helpers/StaticDataProviderEnum";
import { StaticDataResourceType } from "#sch/definitions/models/helpers/StaticDataResourceTypeEnum";

export class CeskyCarsharingVehiclesWorker extends BaseWorker {
    protected readonly name: string = "CeskyCarsharingVehiclesWorker";

    private dataSource: DataSource;
    private vehicleStatusTransformation: CeskyCarsharingVehicleStatusTransformation;
    private vehicleStatusModel: VehicleStatusModel;
    private oldDataCleanerHelper: OldDataCleanerHelper;
    private staticDataRepository: StaticDataRepository;

    constructor() {
        super();
        this.dataSource = CeskyCarsharingVehiclesDataSourceFactory.getDataSource();
        this.vehicleStatusModel = SharedVehiclesContainer.resolve<VehicleStatusModel>(ModuleContainerToken.VehicleStatusModel);
        this.vehicleStatusTransformation = SharedVehiclesContainer.resolve<CeskyCarsharingVehicleStatusTransformation>(
            ModuleContainerToken.CeskyCarsharingVehicleStatusTransformation
        );
        this.oldDataCleanerHelper = SharedVehiclesContainer.resolve<OldDataCleanerHelper>(
            ModuleContainerToken.OldDataCleanerHelper
        );
        this.staticDataRepository = SharedVehiclesContainer.resolve<StaticDataRepository>(
            ModuleContainerToken.StaticDataRepository
        );
    }

    public refreshCeskyCarsharingVehiclesData = async (): Promise<void> => {
        const transformationDate = new Date();

        try {
            const data: ICeskyCarsharingVehicles = await this.dataSource.getAll();
            const cars: ICeskyCarsharingCar[] = data.cars;
            const pricingPlansMap = await this.staticDataRepository.getResourceData(
                StaticDataProvider.CeskyCarsharing,
                StaticDataResourceType.PricingPlanMap
            );

            if (pricingPlansMap === null) {
                throw new GeneralError("No pricingPlansMap for Cesky Carsharing.");
            }

            const vehicleStatus = await this.vehicleStatusTransformation.transform({
                cars,
                pricingPlansMap,
                transformationDate,
            });
            await this.vehicleStatusModel.saveIfInCzechia(vehicleStatus);

            const ceskyCarsharingSystemIds = [ANYTIME_SYSTEM_ID, AUTONAPUL_SYSTEM_ID, CAR4WAY_SYSTEM_ID, BOLT_SYSTEM_ID];

            for (let systemId of ceskyCarsharingSystemIds) {
                await this.oldDataCleanerHelper.deleteOldTrackableData(transformationDate, systemId);
            }
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while refreshing Cesky Carsharing vehicle data", this.constructor.name, err);
            }
        }
    };
}
