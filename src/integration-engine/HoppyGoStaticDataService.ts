import { HOPPYGO_SYSTEM_ID } from "#ie/transformations/HoppyGo/transformConstants";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { ISharedVehiclesRentalAppOutput, ISharedVehiclesSystemInformationOutput } from "#sch";
import { IMobilityOperatorRentalAppMap } from "#sch/datasources/MobilityOperator";
import { ResourceTypeDict } from "#sch/definitions/models/interfaces/IStaticDataOutputDto";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ioc";
import { RentalAppsModel } from "./models/RentalAppsModel";
import { SystemInformationModel } from "./models/SystemInformationModel";
import { VehicleTypesModel } from "./models/VehicleTypesModel";
@injectable()
export class HoppyGoStaticDataService {
    constructor(
        @inject(ModuleContainerToken.MobilityOperatorRentalAppProvider)
        private rentalAppProvider: MobilityOperatorRentalAppProvider,
        @inject(ModuleContainerToken.RentalAppsModel) private rentalAppsModel: RentalAppsModel,
        @inject(ModuleContainerToken.VehicleTypesModel)
        private vehicleTypesModel: VehicleTypesModel,
        @inject(ModuleContainerToken.SystemInformationModel) private systemInformationModel: SystemInformationModel,
        @inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector
    ) {}

    public async saveSystemBasics(staticData: Partial<ResourceTypeDict>): Promise<void> {
        const processedAtDate = new Date();

        const connection = this.databaseConnector.getConnection();
        const transaction = await connection.transaction();

        const mobilityOperatorRentalApps = await this.rentalAppProvider.getMobilityOperatorRentalApps();
        let systemInformation = staticData.systemInformation;
        if (systemInformation === undefined) {
            throw new GeneralError("System information is undefined", this.constructor.name);
        }

        const { appsToSave, systemsToSave } = this.createAppAndSystemInformationData(
            systemInformation,
            mobilityOperatorRentalApps,
            staticData
        );

        const { vehicleTypes } = staticData;
        if (vehicleTypes === undefined) {
            throw new GeneralError("Vehicle types are undefined", this.constructor.name);
        }
        try {
            //we dont want to clean system information
            await this.systemInformationModel.bulkSave(systemsToSave, undefined, false, false, transaction);

            // save all
            await this.vehicleTypesModel.bulkSave(vehicleTypes, undefined, false, false, transaction);
            await this.rentalAppsModel.bulkSave(appsToSave, undefined, false, false, transaction);

            //clean old data
            await this.cleanOldData(processedAtDate, transaction);
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw new GeneralError(
                `Database error: HoppyGoStaticDataService failed to save static data`,
                "HoppyGoStaticDataService",
                err
            );
        }
    }

    private getSystemInformation(
        systemInformations: ISharedVehiclesSystemInformationOutput[],
        rentalAppId?: string
    ): ISharedVehiclesSystemInformationOutput {
        let systemInformation = systemInformations[0];
        if (rentalAppId) {
            systemInformation.rental_app_id = rentalAppId;
        }
        return systemInformation;
    }

    public async cleanOldData(processedAtDate: Date, t: Transaction): Promise<void> {
        await this.vehicleTypesModel.deleteDataBefore(processedAtDate, HOPPYGO_SYSTEM_ID, t);
    }

    private createAppAndSystemInformationData(
        systemInformation: ISharedVehiclesSystemInformationOutput[],
        mobilityOperatorRentalApps: IMobilityOperatorRentalAppMap,
        staticData: Partial<ResourceTypeDict>
    ): { appsToSave: ISharedVehiclesRentalAppOutput[]; systemsToSave: ISharedVehiclesSystemInformationOutput[] } {
        let currentSystemInformation: ISharedVehiclesSystemInformationOutput;

        if (mobilityOperatorRentalApps[HOPPYGO_SYSTEM_ID] !== undefined) {
            const rentalApp = mobilityOperatorRentalApps[HOPPYGO_SYSTEM_ID];
            currentSystemInformation = this.getSystemInformation(systemInformation, rentalApp.id);
            return { appsToSave: [rentalApp], systemsToSave: [currentSystemInformation] };
        } else {
            const rentalApps = staticData.rentalApps;
            if (rentalApps === undefined || !rentalApps.length) {
                throw new GeneralError("Rental apps are undefined", this.constructor.name);
            }

            currentSystemInformation = this.getSystemInformation(systemInformation, rentalApps[0].id);
            return { appsToSave: rentalApps, systemsToSave: [currentSystemInformation] };
        }
    }
}
