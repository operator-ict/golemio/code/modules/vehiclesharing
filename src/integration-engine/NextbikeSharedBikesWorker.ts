import { NextbikeDataSourceFactory } from "#ie/dataSources/NextbikeDataSourceFactory";
import { OldDataCleanerHelper } from "#ie/helpers/OldDataCleanerHelper";
import { ModuleContainerToken, SharedVehiclesContainer } from "#ie/ioc";
import {
    INextBikeFreeBikeStatusOutput,
    INextBikePricingPlansOutput,
    INextBikeStationInformationOutput,
    INextBikeSystemInformationOutput,
} from "#ie/transformations";
import { INextbikeDefinitionTransformation, NEXTBIKE_VEHICLE_TYPE_ID_PREFIX } from "#ie/transformations/Nextbike";
import { NextbikeTransformationFactory } from "#ie/transformations/NextbikeTransformationFactory";
import {
    ISharedVehicleStatusOutput,
    ISharedVehicleTypeOutput,
    ISharedVehiclesPricingOutput,
    ISharedVehiclesPricingPlanOutput,
    ISharedVehiclesRentalAppOutput,
    ISharedVehiclesStationInformationOutput,
    ISharedVehiclesStationStatusOutput,
    ISharedVehiclesStationStatusVehicleTypeOutput,
    ISharedVehiclesSystemInformationOutput,
} from "#sch";
import { NEXT_BIKE_SOURCES } from "#sch/datasources/Nextbike";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { config } from "@golemio/core/dist/integration-engine";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import StationStatusVehicleTypeHelper from "./helpers/StationStatusVehicleTypeHelper";
import { PricingModel } from "./models/PricingModel";
import { PricingPlansModel } from "./models/PricingPlansModel";
import { RentalAppsModel } from "./models/RentalAppsModel";
import { StationInformationModel } from "./models/StationInformationModel";
import { StationStatusModel } from "./models/StationStatusModel";
import { StationStatusVehicleTypeModel } from "./models/StationStatusVehicleTypeModel";
import { SystemInformationModel } from "./models/SystemInformationModel";
import { VehicleStatusModel } from "./models/VehicleStatusModel";
import { VehicleTypesModel } from "./models/VehicleTypesModel";

export interface INextBikeTransformationResult {
    systemInformation: ISharedVehiclesSystemInformationOutput[];
    bikeStatus: ISharedVehicleStatusOutput[];
    stationInformation: ISharedVehiclesStationInformationOutput[];
    stationStatus: ISharedVehiclesStationStatusOutput[];
    stationStatusVehicleType: ISharedVehiclesStationStatusVehicleTypeOutput[];
    vehicleTypes: ISharedVehicleTypeOutput[];
    pricingPlans: ISharedVehiclesPricingPlanOutput[];
    pricings: ISharedVehiclesPricingOutput[];
    rentalApps: ISharedVehiclesRentalAppOutput[];
}

export class NextbikeSharedBikesWorker extends BaseWorker {
    private config = config.datasources.NextbikeSharedBikes;
    private dataSourceFactory: NextbikeDataSourceFactory;
    private transformationFactory: NextbikeTransformationFactory;

    private systemInformationModel: SystemInformationModel;
    private pricingPlansModel: PricingPlansModel;
    private pricingModel: PricingModel;
    private rentalAppsModel: RentalAppsModel;
    private vehicleTypesModel: VehicleTypesModel;
    private vehicleStatusModel: VehicleStatusModel;
    private stationStatusModel: StationStatusModel;
    private stationStatusVehicleTypeModel: StationStatusVehicleTypeModel;
    private stationInformationModel: StationInformationModel;
    private oldDataCleanerHelper: OldDataCleanerHelper;
    private postgresConnector: IDatabaseConnector;

    constructor() {
        super();
        this.dataSourceFactory = new NextbikeDataSourceFactory();
        this.transformationFactory = new NextbikeTransformationFactory();
        this.systemInformationModel = new SystemInformationModel();
        this.pricingPlansModel = new PricingPlansModel();
        this.pricingModel = new PricingModel();
        this.rentalAppsModel = new RentalAppsModel();
        this.vehicleTypesModel = new VehicleTypesModel();
        this.vehicleStatusModel = new VehicleStatusModel();
        this.stationStatusModel = new StationStatusModel();
        this.stationStatusVehicleTypeModel = new StationStatusVehicleTypeModel();
        this.stationInformationModel = new StationInformationModel();
        this.oldDataCleanerHelper = SharedVehiclesContainer.resolve<OldDataCleanerHelper>(
            ModuleContainerToken.OldDataCleanerHelper
        );
        this.postgresConnector = SharedVehiclesContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
    }

    /**
     * Main worker method - fetch new Nextbike data
     */
    public refreshNextbikeData = async (): Promise<void> => {
        for (const sourceId of this.config.sourceIds) {
            const definitionDatasource = this.dataSourceFactory.getDataSourceDefinition(sourceId);
            const definitionTransformation = this.transformationFactory.getDefinitionTransformation(sourceId);

            try {
                const transformedDefinitionData = await definitionTransformation.transform(await definitionDatasource.getAll());
                await this.processDataSource(sourceId, transformedDefinitionData);
            } catch (err) {
                if (err instanceof AbstractGolemioError) {
                    throw err;
                } else {
                    throw new GeneralError(
                        `Error while refreshing Nextbike data for ${sourceId}: ${err.message}`,
                        this.constructor.name,
                        err
                    );
                }
            }
        }
    };

    private async processDataSource(sourceId: string, definitions: INextbikeDefinitionTransformation[]): Promise<void> {
        const transformationDate = new Date();
        const result: INextBikeTransformationResult = {
            systemInformation: [],
            bikeStatus: [],
            stationInformation: [],
            stationStatus: [],
            stationStatusVehicleType: [],
            vehicleTypes: [],
            pricingPlans: [],
            pricings: [],
            rentalApps: [],
        };

        for (const definition of definitions) {
            const datasource = this.dataSourceFactory.getDataSourceData(definition);
            const datasourceData = await datasource.getAll();
            const data = { datasourceData, transformationDate };
            const transformation = this.transformationFactory.getDataTransformation(sourceId, definition.name);
            const transformedData = await transformation.transform(data);

            switch (definition.name) {
                case NEXT_BIKE_SOURCES.free_bike_status:
                    result.bikeStatus.push(...(transformedData as INextBikeFreeBikeStatusOutput).bikeStatus);
                    result.rentalApps.push(...(transformedData as INextBikeFreeBikeStatusOutput).rentalApps);
                    break;
                case NEXT_BIKE_SOURCES.station_information:
                    result.stationInformation.push(...(transformedData as INextBikeStationInformationOutput).stationInfo);
                    result.rentalApps.push(...(transformedData as INextBikeStationInformationOutput).rentalApps);
                    break;
                case NEXT_BIKE_SOURCES.station_status:
                    result.stationStatus.push(...(transformedData as ISharedVehiclesStationStatusOutput[]));
                    break;
                case NEXT_BIKE_SOURCES.system_information:
                    result.systemInformation.push((transformedData as INextBikeSystemInformationOutput).systemInfo);
                    result.rentalApps.push((transformedData as INextBikeSystemInformationOutput).rentalApps);
                    break;
                case NEXT_BIKE_SOURCES.system_pricing_plans:
                    result.pricingPlans.push(...(transformedData as INextBikePricingPlansOutput).pricingPlan);
                    result.pricings.push(...(transformedData as INextBikePricingPlansOutput).pricing);
                    break;
                case NEXT_BIKE_SOURCES.vehicle_types:
                    result.vehicleTypes.push(...(transformedData as ISharedVehicleTypeOutput[]));
                    break;
                default:
                    throw new Error(`Invalid definition (${definition.name})`);
            }
        }

        result.stationStatusVehicleType.push(
            ...StationStatusVehicleTypeHelper.mapTransformationResults(
                result.stationStatus,
                result.bikeStatus,
                transformationDate
            )
        );

        await this.saveData(result);
        await this.saveDataInTransaction(transformationDate, result);
        await this.deleteOldData(transformationDate, result);
    }

    /**
     * Save transformation result data to the database
     *
     * @param result Transformation result data to be saved to the database
     */
    private async saveData(result: INextBikeTransformationResult): Promise<void> {
        await this.rentalAppsModel.save(result.rentalApps);
        await this.systemInformationModel.bulkSave(result.systemInformation);
        await this.stationInformationModel.saveIfInCzechia(result.stationInformation);
        await this.vehicleTypesModel.save(result.vehicleTypes);
        await this.pricingPlansModel.bulkSave(result.pricingPlans);
        await this.vehicleStatusModel.saveIfInCzechia(result.bikeStatus);
        await this.pricingModel.bulkSave(result.pricings);
    }

    private async saveDataInTransaction(processedAt: Date, result: INextBikeTransformationResult): Promise<void> {
        const connection = await this.postgresConnector.getConnection();
        const transaction = await connection.transaction();
        try {
            await this.stationStatusModel.bulkSave(result.stationStatus, undefined, false, false, transaction);
            await this.stationStatusVehicleTypeModel.bulkSave(
                result.stationStatusVehicleType,
                undefined,
                false,
                false,
                transaction
            );

            for (const system of result.systemInformation) {
                await this.stationStatusVehicleTypeModel.deleteDataBefore(processedAt, system.system_id, transaction);
                await this.stationStatusModel.deleteDataBefore(processedAt, system.system_id, transaction);
            }
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw new GeneralError(
                `Database error: NextbikeSharedBikesWorker failed to save data`,
                "NextbikeSharedBikesWorker",
                err
            );
        }
    }

    /**
     * Delete outdated data from the database
     *
     * @param processedAt The `processed_at` date/time, prior to which the data should be deleted
     * @param result Transformation result data to get system IDs to delete from
     */
    private async deleteOldData(processedAt: Date, result: INextBikeTransformationResult): Promise<void> {
        const prefixId = NEXTBIKE_VEHICLE_TYPE_ID_PREFIX.slice(0, -1);
        await this.vehicleTypesModel.deleteOrphans(prefixId);
        for (const system of result.systemInformation) {
            await this.oldDataCleanerHelper.deleteOldTrackableData(processedAt, system.system_id);
        }
    }
}
