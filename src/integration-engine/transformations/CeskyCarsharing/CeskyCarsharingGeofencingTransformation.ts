import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { ISharedVehiclesGeofencingZoneOutput, Vehiclesharing } from "#sch";
import { ICeskyCarsharingPolygon } from "#sch/datasources/CeskyCarsharingGeofencingZone";
import getUuidByString from "uuid-by-string";
import { getSystemIdByCeskyCarsharingCompanyId } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingSystemIdentifier";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class CeskyCarsharingGeofencingTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = Vehiclesharing.datasources.CeskyCarsharingGeofencingJsonSchema.name;
    }

    public transform = (data: ICeskyCarsharingPolygon[]): Promise<ISharedVehiclesGeofencingZoneOutput[]> => {
        const res: ISharedVehiclesGeofencingZoneOutput[] = [];

        for (const polygon of data) {
            const transformedPolygon = this.transformElement(polygon);

            // Skip unknown providers
            if (transformedPolygon.system_id !== "cesky_carsharing") {
                res.push(transformedPolygon);
            }
        }

        return Promise.resolve(res);
    };

    /**
     * Transform zones
     */
    protected transformElement = (element: ICeskyCarsharingPolygon): ISharedVehiclesGeofencingZoneOutput => {
        return {
            id: getUuidByString(this.getPolygonSum(element)),
            system_id: getSystemIdByCeskyCarsharingCompanyId(element.company_id),
            name: null,
            note: null,
            source: null,
            price: null,
            priority: 0,
            start: null,
            end: null,
            geom: {
                type: "MultiPolygon",
                coordinates: [[element.polygon.map((point) => [Number(point.lon), Number(point.lat)])]],
            },
            ride_allowed: true,
            ride_through_allowed: true,
            maximum_speed_kph: null,
            parking_allowed: true,
        };
    };

    private getPolygonSum = (element: ICeskyCarsharingPolygon): string => {
        let polygonSum: string = String(element.company_id);
        element.polygon.forEach((currentPolygon) => {
            polygonSum += currentPolygon.lon + currentPolygon.lat;
        });
        return polygonSum;
    };
}
