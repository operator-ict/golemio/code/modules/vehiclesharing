import { getSystemIdByCeskyCarsharingCompanyId } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingSystemIdentifier";
import {
    ANYTIME_RENTAL_APP_ID,
    AUTONAPUL_RENTAL_APP_ID,
    BOLT_RENTAL_APP_ID,
    CAR4WAY_RENTAL_APP_ID,
    CESKY_CARSHARING_ID_PREFIX,
    CESKY_CARSHARING_VEHICLE_TYPE_DIESEL,
    CESKY_CARSHARING_VEHICLE_TYPE_ELECTRIC,
    CESKY_CARSHARING_VEHICLE_TYPE_HYBRID,
    CESKY_CARSHARING_VEHICLE_TYPE_LPG,
    CESKY_CARSHARING_VEHICLE_TYPE_PETROL,
} from "#ie/transformations/CeskyCarsharing/transformConstants";
import { getWhereGeometry } from "#ie/transformations/helpers/getWhereGeometry";
import { ISharedVehicleStatusOutput, Vehiclesharing } from "#sch";
import { ICeskyCarsharingCar } from "#sch/datasources";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { Point } from "geojson";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { log } from "@golemio/core/dist/integration-engine";
import { CeskyCarsharingPricingPlanMatcher } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingPricingPlanMatcher";
import { IPricingPlanMap } from "#sch/definitions/PricingPlansMap";
import { ModuleContainerToken } from "#ie/ioc";

interface CarMakeModel {
    make: string;
    model: string;
}

@injectable()
export class CeskyCarsharingVehicleStatusTransformation extends BaseTransformation implements ITransformation {
    public name: string = Vehiclesharing.datasources.HoppyGoVehiclesJsonSchema.name + "BikeStatusTransformation";

    constructor(
        @inject(ModuleContainerToken.CeskyCarsharingPricingPlanMatcher)
        private pricingPlanMatcher: CeskyCarsharingPricingPlanMatcher
    ) {
        super();
    }

    public transform = async (data: {
        cars: ICeskyCarsharingCar[];
        pricingPlansMap: IPricingPlanMap[];
        transformationDate: Date;
    }): Promise<ISharedVehicleStatusOutput[]> => {
        const bikeStatus: ISharedVehicleStatusOutput[] = [];

        for (const car of data.cars) {
            try {
                const transformedCar = this.transformElement({
                    car,
                    pricingPlansMap: data.pricingPlansMap,
                    transformationDate: data.transformationDate,
                });
                bikeStatus.push(transformedCar);
            } catch (err) {
                log.debug(err);
            }
        }

        return bikeStatus;
    };

    protected transformElement = (element: {
        car: ICeskyCarsharingCar;
        pricingPlansMap: IPricingPlanMap[];
        transformationDate: Date;
    }): ISharedVehicleStatusOutput => {
        const makeModel = this.getCarMakeModel(element.car);

        return {
            id: CESKY_CARSHARING_ID_PREFIX + element.car.rz,
            system_id: getSystemIdByCeskyCarsharingCompanyId(element.car.company_id),
            point: this.getCarLocation(element.car),
            helmets: null,
            passengers: null,
            damage_description: null,
            description: element.car.car_name,
            vehicle_registration: element.car.rz,
            is_reserved: false,
            is_disabled: false,
            vehicle_type_id: this.getVehicleTypeId(element.car),
            last_reported: element.transformationDate.toISOString(),
            current_range_meters: null,
            charge_percent: null,
            rental_app_id: this.getRentalAppIdByCompanyId(element.car.company_id),
            station_id: null,
            pricing_plan_id: this.getPricingPlanId(element.car, element.pricingPlansMap),
            make: makeModel.make,
            model: makeModel.model,
            color: null,
            processed_at: element.transformationDate,
        };
    };

    private getRentalAppIdByCompanyId(company_id: number): string {
        switch (company_id) {
            case 3:
                return AUTONAPUL_RENTAL_APP_ID;
            case 5:
                return CAR4WAY_RENTAL_APP_ID;
            case 9:
                return ANYTIME_RENTAL_APP_ID;
            case 11:
                return BOLT_RENTAL_APP_ID;
            default:
                return "";
        }
    }

    private getCarLocation = (car: ICeskyCarsharingCar): Point => {
        return getWhereGeometry(Number(car.latitude), Number(car.longitude));
    };

    private getVehicleTypeId = (car: ICeskyCarsharingCar): string => {
        switch (car.fuel) {
            case 1:
                return CESKY_CARSHARING_VEHICLE_TYPE_PETROL;
            case 2:
                return CESKY_CARSHARING_VEHICLE_TYPE_DIESEL;
            case 3:
                return CESKY_CARSHARING_VEHICLE_TYPE_LPG;
            case 4:
                return CESKY_CARSHARING_VEHICLE_TYPE_ELECTRIC;
            case 5:
                return CESKY_CARSHARING_VEHICLE_TYPE_HYBRID;
            default:
                return CESKY_CARSHARING_VEHICLE_TYPE_PETROL;
        }
    };

    private getCarMakeModel = (car: ICeskyCarsharingCar): CarMakeModel => {
        switch (car.car_name) {
            case "Fabia III kombi  ":
                return {
                    make: "Škoda",
                    model: "Fabia III kombi",
                };
            case "Logan MCV kombi  ":
                return {
                    make: "Dacia",
                    model: "Logan MCV",
                };
            case "SUV Sportage  ":
                return {
                    make: "Kia",
                    model: "Sportage SUV",
                };
            case "jiné Sandero":
                return {
                    make: "Dacia",
                    model: "Sandero",
                };
            case "jiné Model":
                return {
                    make: "Tesla",
                    model: "Model 3",
                };
            case "jiné Corolla":
                return {
                    make: "Toyota",
                    model: "Corolla",
                };
            case "jiné i30":
                return {
                    make: "Hyundai",
                    model: "i30",
                };
            default:
                const make = car.car_name.split(" ")[0];
                return {
                    make,
                    model: car.car_name.slice(make.length).trim(),
                };
        }
    };

    private getPricingPlanId = (car: ICeskyCarsharingCar, pricingPlansMap: IPricingPlanMap[]): string => {
        return this.pricingPlanMatcher.getPricingPlanId(car, pricingPlansMap);
    };
}
