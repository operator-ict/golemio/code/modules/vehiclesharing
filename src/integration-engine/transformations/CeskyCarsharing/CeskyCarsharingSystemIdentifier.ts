import {
    ANYTIME_SYSTEM_ID,
    AUTONAPUL_SYSTEM_ID,
    BOLT_SYSTEM_ID,
    CAR4WAY_SYSTEM_ID,
} from "#ie/transformations/CeskyCarsharing/transformConstants";

export function getSystemIdByCeskyCarsharingCompanyId(company_id: number): string {
    switch (company_id) {
        case 3:
            return AUTONAPUL_SYSTEM_ID;
        case 5:
            return CAR4WAY_SYSTEM_ID;
        case 9:
            return ANYTIME_SYSTEM_ID;
        case 11:
            return BOLT_SYSTEM_ID;
        default:
            return "cesky_carsharing";
    }
}
