import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ICeskyCarsharingCar } from "#sch/datasources";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IPricingPlanMap } from "#sch/definitions/PricingPlansMap";

@injectable()
export class CeskyCarsharingPricingPlanMatcher {
    public getPricingPlanId(car: ICeskyCarsharingCar, pricingPlans: IPricingPlanMap[]): string {
        const systemId = this.getSystemId(car);
        const systemPlans = this.getPricingPlansBySystem(systemId, pricingPlans);
        for (const plan of systemPlans) {
            const carRegex = new RegExp(plan.car_name_expression, "i");
            if (carRegex.test(car.car_name)) {
                return plan.pricing_plan_id;
            }
        }
        throw new GeneralError(
            `Could not find pricing plan id for ${car.car_name} of ${systemId}`,
            "CeskyCarsharingPricingPlanMatcher"
        );
    }

    private getPricingPlansBySystem(systemId: string, pricingPlans: IPricingPlanMap[]): IPricingPlanMap[] {
        return pricingPlans.filter((plan) => plan.system_id === systemId);
    }

    private getSystemId(car: ICeskyCarsharingCar): string {
        switch (car.company_id) {
            case 3:
                return "autonapul";
            case 5:
                return "car4way";
            case 9:
                return "anytime";
            case 11:
                return "bolt";
            default:
                return "";
        }
    }
}
