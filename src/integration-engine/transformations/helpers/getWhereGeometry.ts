import { Point } from "geojson";

export function getWhereGeometry(lat: number, lon: number): Point {
    return {
        type: "Point",
        coordinates: [lon, lat],
    };
}
