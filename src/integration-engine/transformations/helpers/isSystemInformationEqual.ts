import { ISharedVehiclesSystemInformationOutput } from "#sch";

export function isSystemInformationEqual(
    objA: ISharedVehiclesSystemInformationOutput,
    objB: ISharedVehiclesSystemInformationOutput
): boolean {
    for (const key in objA) {
        if (objA[key] !== objB[key]) {
            return false;
        }
    }
    return true;
}
