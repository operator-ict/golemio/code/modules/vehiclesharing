import { NEXTBIKE_ID_PREFIX, NEXTBIKE_VEHICLE_TYPE_ID_PREFIX } from "#ie/transformations/Nextbike";
import { getRentalAppsFromUris } from "#ie/transformations/Nextbike/getRentalAppsFromUris";
import { getWhereGeometry } from "#ie/transformations/helpers/getWhereGeometry";
import { ISharedVehicleStatusOutput, ISharedVehiclesRentalAppOutput, Vehiclesharing } from "#sch";
import { INextbikeFreeBikeStatus, INextbikeFreeBikeStatusInput } from "#sch/datasources";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import getUuidByString from "uuid-by-string";

export interface INextBikeFreeBikeStatusOutput {
    bikeStatus: ISharedVehicleStatusOutput[];
    rentalApps: ISharedVehiclesRentalAppOutput[];
}

export class NextbikeVehicleStatusTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly systemId: string;

    constructor(private sourceId: string) {
        super();
        this.name = Vehiclesharing.datasources.nextbikeFreeBikeStatusJsonSchema.name + sourceId + "Transformation";
        this.systemId = getUuidByString(sourceId);
    }

    public transform = async (data: {
        datasourceData: INextbikeFreeBikeStatusInput;
        transformationDate: Date;
    }): Promise<INextBikeFreeBikeStatusOutput> => {
        const bikeStatus: ISharedVehicleStatusOutput[] = [];
        const rentalApps: ISharedVehiclesRentalAppOutput[] = [];

        for (const item of data.datasourceData.data.bikes) {
            const transformedData = this.transformElement({
                item,
                last_updated: data.datasourceData.last_updated,
                transformationDate: data.transformationDate,
            });
            bikeStatus.push(transformedData.bikeStatus);
            rentalApps.push(transformedData.rentalApps);
        }

        return {
            bikeStatus,
            rentalApps,
        };
    };

    protected transformElement = (element: {
        item: INextbikeFreeBikeStatus;
        last_updated: number;
        transformationDate: Date;
    }): {
        bikeStatus: ISharedVehicleStatusOutput;
        rentalApps: ISharedVehiclesRentalAppOutput;
    } => {
        const rentalAppId = getUuidByString(JSON.stringify(element.item.rental_uris));
        const pricingPlanId = `${NEXTBIKE_ID_PREFIX}${this.sourceId}-${element.item.pricing_plan_id}`;

        return {
            bikeStatus: {
                id: NEXTBIKE_ID_PREFIX + element.item.bike_id,
                system_id: this.systemId,
                point: getWhereGeometry(element.item.lat, element.item.lon),
                helmets: null,
                passengers: null,
                damage_description: null,
                description: null,
                vehicle_registration: null,
                is_reserved: element.item.is_reserved,
                is_disabled: element.item.is_disabled,
                vehicle_type_id: NEXTBIKE_VEHICLE_TYPE_ID_PREFIX + element.item.vehicle_type_id,
                last_reported: new Date(element.last_updated * 1000).toISOString(),
                current_range_meters: null,
                charge_percent: null,
                rental_app_id: rentalAppId,
                station_id: element.item.station_id ? NEXTBIKE_ID_PREFIX + element.item.station_id : null,
                pricing_plan_id: pricingPlanId,
                make: null,
                model: null,
                color: null,
                processed_at: element.transformationDate,
            },
            rentalApps: getRentalAppsFromUris(element.item.rental_uris, rentalAppId),
        };
    };
}
