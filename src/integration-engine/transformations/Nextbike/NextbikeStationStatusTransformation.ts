import { NEXTBIKE_ID_PREFIX } from "#ie/transformations/Nextbike";
import { ISharedVehiclesStationStatusOutput, Vehiclesharing } from "#sch";
import { INextbikeStationStatus, INextbikeStationStatusInput } from "#sch/datasources";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";

export class NextbikeStationStatusTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor(sourceId: string) {
        super();
        this.name = Vehiclesharing.datasources.nextbikeStationStatusJsonSchema.name + sourceId + "Transformation";
    }

    public transform = async (data: {
        datasourceData: INextbikeStationStatusInput;
        transformationDate: Date;
    }): Promise<ISharedVehiclesStationStatusOutput[]> => {
        return data.datasourceData.data.stations.map((item) =>
            this.transformElement({ item, transformationDate: data.transformationDate })
        );
    };

    protected transformElement = (element: {
        item: INextbikeStationStatus;
        transformationDate: Date;
    }): ISharedVehiclesStationStatusOutput => ({
        station_id: NEXTBIKE_ID_PREFIX + element.item.station_id,
        num_bikes_available: element.item.num_bikes_available,
        num_bikes_disabled: null,
        num_docks_available: element.item.num_docks_available,
        is_installed: element.item.is_installed,
        is_renting: element.item.is_renting,
        is_returning: element.item.is_returning,
        last_reported: new Date(element.item.last_reported * 1000).toISOString(),
        processed_at: element.transformationDate,
    });
}
