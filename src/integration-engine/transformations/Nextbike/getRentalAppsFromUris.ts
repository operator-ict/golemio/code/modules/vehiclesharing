import { ISharedVehiclesRentalAppOutput } from "#sch";
import { INextbikeRentalUris } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";

export const getRentalAppsFromUris = (data: INextbikeRentalUris, id: string): ISharedVehiclesRentalAppOutput => {
    return {
        id,
        android_store_url: null,
        android_discovery_url: data.android,
        ios_store_url: null,
        ios_discovery_url: data.ios,
        web_url: data.web,
    };
};
