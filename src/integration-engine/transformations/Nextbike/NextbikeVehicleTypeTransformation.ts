import { NEXTBIKE_VEHICLE_TYPE_ID_PREFIX } from "#ie/transformations/Nextbike";
import { ISharedVehicleTypeOutput, Vehiclesharing } from "#sch";
import { INextbikeVehicleType, INextbikeVehicleTypeInput } from "#sch/datasources";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";

export class NextbikeVehicleTypeTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor(private sourceId: string) {
        super();
        this.name = Vehiclesharing.datasources.nextbikeVehicleTypeJsonSchema.name + sourceId + "Transformation";
    }

    public transform = async (data: { datasourceData: INextbikeVehicleTypeInput }): Promise<ISharedVehicleTypeOutput[]> => {
        return data.datasourceData.data.vehicle_types.map(this.transformElement);
    };

    protected transformElement = (element: INextbikeVehicleType): ISharedVehicleTypeOutput => {
        return {
            id: NEXTBIKE_VEHICLE_TYPE_ID_PREFIX + element.vehicle_type_id,
            form_factor: element.form_factor,
            propulsion_type: element.propulsion_type,
            max_range_meters: element.max_range_meters ?? null,
            name: element.name,
        };
    };
}
