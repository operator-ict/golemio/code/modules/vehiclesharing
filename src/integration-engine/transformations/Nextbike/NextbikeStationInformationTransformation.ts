import getUuidByString from "uuid-by-string";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { ISharedVehiclesRentalAppOutput, ISharedVehiclesStationInformationOutput, Vehiclesharing } from "#sch";
import { INextbikeStationInformation, INextbikeStationInformationInput } from "#sch/datasources";
import { getWhereGeometry } from "#ie/transformations/helpers/getWhereGeometry";
import { NEXTBIKE_ID_PREFIX } from "#ie/transformations/Nextbike";
import { getRentalAppsFromUris } from "#ie/transformations/Nextbike/getRentalAppsFromUris";

export interface INextBikeStationInformationOutput {
    stationInfo: ISharedVehiclesStationInformationOutput[];
    rentalApps: ISharedVehiclesRentalAppOutput[];
}

export class NextbikeStationInformationTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly systemId: string;

    constructor(sourceId: string) {
        super();
        this.name = Vehiclesharing.datasources.nextbikeStationInfoJsonSchema.name + sourceId + "Transformation";
        this.systemId = getUuidByString(sourceId);
    }

    public transform = async (data: {
        datasourceData: INextbikeStationInformationInput;
        transformationDate: Date;
    }): Promise<INextBikeStationInformationOutput> => {
        const stationInfo: ISharedVehiclesStationInformationOutput[] = [];
        const rentalApps: ISharedVehiclesRentalAppOutput[] = [];

        for (const item of data.datasourceData.data.stations) {
            const transformedData = this.transformElement({ item, transformationDate: data.transformationDate });
            stationInfo.push(transformedData.stationInfo);
            rentalApps.push(transformedData.rentalApps);
        }

        return {
            stationInfo,
            rentalApps,
        };
    };

    protected transformElement = (element: {
        item: INextbikeStationInformation;
        transformationDate: Date;
    }): {
        stationInfo: ISharedVehiclesStationInformationOutput;
        rentalApps: ISharedVehiclesRentalAppOutput;
    } => {
        const rentalAppId = getUuidByString(JSON.stringify(element.item.rental_uris));

        return {
            stationInfo: {
                id: NEXTBIKE_ID_PREFIX + element.item.station_id,
                system_id: this.systemId,
                name: element.item.name,
                short_name: element.item.short_name,
                point: getWhereGeometry(element.item.lat, element.item.lon),
                address: null,
                post_code: null,
                cross_street: null,
                region_id: element.item.region_id,
                rental_methods: null,
                is_virtual_station: element.item.is_virtual_station,
                station_area: null,
                capacity: element.item.capacity,
                vehicle_capacity: null,
                vehicle_type_capacity: null,
                is_valet_station: null,
                rental_app_id: rentalAppId,
                processed_at: element.transformationDate,
            },
            rentalApps: getRentalAppsFromUris(element.item.rental_uris, rentalAppId),
        };
    };
}
