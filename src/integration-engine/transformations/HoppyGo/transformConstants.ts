export const HOPPYGO_SYSTEM_ID = "ef63ac2e-4c71-4c28-a496-f0dc010d630b";
export const HOPPYGO_ID_PREFIX = "hoppygo-";
export const HOPPYGO_VEHICLE_TYPE_ELECTRIC = "hoppygo_car_electric";
export const HOPPYGO_VEHICLE_TYPE_COMBUSTION = "hoppygo_car_combustion";
export const HOPPYGO_PRICING_PLAN_PREFIX = "hoppygo_individual_rides_";
export const HOPPYGO_RENTAL_APP_ID = "hoppygo_rental_apps";
