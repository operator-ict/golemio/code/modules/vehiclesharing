import { ModuleContainerToken } from "#ie/ioc";
import { PricingModel } from "#ie/models/PricingModel";
import { PricingPlansModel } from "#ie/models/PricingPlansModel";
import { RentalAppsModel } from "#ie/models/RentalAppsModel";
import { SystemInformationModel } from "#ie/models/SystemInformationModel";
import { VehicleTypesModel } from "#ie/models/VehicleTypesModel";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola/transformConstants";
import { ISharedVehiclesRentalAppOutput, ISharedVehiclesSystemInformationOutput } from "#sch";
import { IMobilityOperatorRentalAppMap } from "#sch/datasources/MobilityOperator";
import { ResourceTypeDict } from "#sch/definitions/models/interfaces/IStaticDataOutputDto";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RekolaStaticDataService {
    constructor(
        @inject(ModuleContainerToken.MobilityOperatorRentalAppProvider)
        private rentalAppProvider: MobilityOperatorRentalAppProvider,
        @inject(ModuleContainerToken.RentalAppsModel) private rentalAppsModel: RentalAppsModel,
        @inject(ModuleContainerToken.VehicleTypesModel)
        private vehicleTypesModel: VehicleTypesModel,
        @inject(ModuleContainerToken.SystemInformationModel) private systemInformationModel: SystemInformationModel,
        @inject(ModuleContainerToken.PricingModel) private pricingModel: PricingModel,
        @inject(ModuleContainerToken.PricingPlansModel) private pricingPlansModel: PricingPlansModel,
        @inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector
    ) {}

    public async saveRekolaStaticData(staticData: Partial<ResourceTypeDict>): Promise<void> {
        const processedAtDate = new Date();

        const connection = this.databaseConnector.getConnection();
        const transaction = await connection.transaction();

        const mobileOperatorRentalApps = await this.rentalAppProvider.getMobilityOperatorRentalApps();
        let systemInformation = staticData.systemInformation;
        if (systemInformation === undefined || !systemInformation.length) {
            throw new GeneralError("System information is undefined", this.constructor.name);
        }

        const { pricingPlans, pricings } = staticData;
        if (pricingPlans === undefined || pricings === undefined) {
            throw new GeneralError("Pricing plans or pricings are undefined", this.constructor.name);
        }

        const rentalApps = this.createRentalAppData(systemInformation, mobileOperatorRentalApps, staticData);

        try {
            //we dont want to clean system information
            await this.systemInformationModel.bulkSave([systemInformation[0]], undefined, false, false, transaction);

            // save all
            await this.rentalAppsModel.bulkSave(rentalApps, undefined, false, false, transaction);
            await this.pricingPlansModel.bulkSave(pricingPlans, undefined, false, false, transaction);
            await this.pricingModel.bulkSave(pricings, undefined, false, false, transaction);

            //clean old data
            await this.cleanOldData(processedAtDate, transaction);

            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw new GeneralError(
                `Database error: RekolaStaticDataService failed to save static data`,
                "RekolaStaticDataService",
                err
            );
        }
    }

    public async cleanOldData(processedAtDate: Date, transaction: Transaction): Promise<void> {
        await this.pricingModel.deleteDataBefore(processedAtDate, REKOLA_SYSTEM_ID, transaction);
        await this.pricingPlansModel.deleteDataBefore(processedAtDate, REKOLA_SYSTEM_ID, transaction);
    }

    private createRentalAppData(
        systemInformation: ISharedVehiclesSystemInformationOutput[],
        mobileOperatorRentalApps: IMobilityOperatorRentalAppMap,
        staticData: Partial<ResourceTypeDict>
    ): ISharedVehiclesRentalAppOutput[] {
        if (mobileOperatorRentalApps[REKOLA_SYSTEM_ID] !== undefined) {
            const rentalApp = mobileOperatorRentalApps[REKOLA_SYSTEM_ID];
            systemInformation[0].rental_app_id = rentalApp.id;
            return [rentalApp];
        } else {
            const rentalApps = staticData.rentalApps;
            if (rentalApps === undefined) {
                throw new GeneralError("Rental apps are undefined", this.constructor.name);
            }
            return rentalApps;
        }
    }
}
