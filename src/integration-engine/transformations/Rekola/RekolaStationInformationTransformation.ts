import { REKOLA_PREFIX, REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola/transformConstants";
import {
    IRekolaTrackablesDatasource,
    IRekolaTrackablesDatasourceRack,
    ISharedVehiclesStationInformationOutput,
    Vehiclesharing,
} from "#sch";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";

export class RekolaStationInformationTransformation extends BaseTransformation implements ITransformation {
    public name: string = Vehiclesharing.datasources.rekolaTrackables.name + "StationInformationTransformation";

    public transform = async (data: {
        trackables: IRekolaTrackablesDatasource;
        transformationDate: Date;
    }): Promise<ISharedVehiclesStationInformationOutput[]> => {
        const stations: ISharedVehiclesStationInformationOutput[] = [];

        for (const parking of data.trackables.parkings) {
            stations.push(
                this.transformElement({
                    parking,
                    transformationDate: data.transformationDate,
                })
            );
        }

        return stations;
    };

    protected transformElement = (element: {
        parking: IRekolaTrackablesDatasourceRack;
        transformationDate: Date;
    }): ISharedVehiclesStationInformationOutput => {
        return {
            id: REKOLA_PREFIX + element.parking.id,
            system_id: REKOLA_SYSTEM_ID,
            name: element.parking.name,
            point: {
                type: "Point",
                coordinates: [element.parking.position.lng, element.parking.position.lat],
            },
            address: null,
            post_code: null,
            cross_street: null,
            region_id: null,
            rental_methods: null,
            is_virtual_station: true,
            station_area: null,
            capacity: null,
            vehicle_capacity: null,
            vehicle_type_capacity: null,
            is_valet_station: null,
            rental_app_id: null,
            processed_at: element.transformationDate,
        };
    };
}
