import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import {
    IRekolaTrackablesDatasource,
    IRekolaTrackablesDatasourceRack,
    ISharedVehiclesStationStatusOutput,
    Vehiclesharing,
} from "#sch";
import { REKOLA_PREFIX } from "#ie/transformations/Rekola/transformConstants";

export class RekolaStationStatusTransformation extends BaseTransformation implements ITransformation {
    public name: string = Vehiclesharing.datasources.rekolaTrackables.name + "StationStatusTransformation";

    public transform = async (data: {
        trackables: IRekolaTrackablesDatasource;
        transformationDate: Date;
    }): Promise<ISharedVehiclesStationStatusOutput[]> => {
        const stations: {
            [key: number]: ISharedVehiclesStationStatusOutput;
        } = {};

        const isoDate = data.transformationDate.toISOString();

        for (const rack of data.trackables.racks) {
            stations[rack.id] = this.transformElement({ rack, transformationDate: isoDate });
        }

        for (const rack of data.trackables.parkings) {
            if (!(rack.id in stations)) {
                stations[rack.id] = this.transformElement({ rack, transformationDate: isoDate });
            }
        }

        return Object.values(stations);
    };

    protected transformElement = (element: {
        rack: IRekolaTrackablesDatasourceRack;
        transformationDate: string;
    }): ISharedVehiclesStationStatusOutput => {
        return {
            station_id: REKOLA_PREFIX + element.rack.id,
            num_bikes_available: element.rack.vehicles.length,
            num_bikes_disabled: null,
            num_docks_available: null,
            is_installed: true,
            is_renting: element.rack.vehicles.length > 0,
            is_returning: true,
            last_reported: element.transformationDate,
        };
    };
}
