/* ie/index.ts */
import { StaticDataWorker } from "./workers/StaticDataWorker";

export * from "./queueDefinitions";
export const workers = [StaticDataWorker];
