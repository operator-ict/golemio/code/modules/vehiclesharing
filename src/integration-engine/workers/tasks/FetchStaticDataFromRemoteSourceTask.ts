import { IStaticDataSourceFactory } from "#ie/dataSources/staticData/interfaces/IStaticDataSourceFactory";
import { ModuleContainerToken } from "#ie/ioc";
import { StaticDataRepository } from "#ie/repositories/StaticDataRepository";
import { StaticDataProvider } from "#sch/definitions/models/helpers/StaticDataProviderEnum";
import { StaticDataResourceType } from "#sch/definitions/models/helpers/StaticDataResourceTypeEnum";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { STATIC_DATA_WORKER_NAME } from "../constants";
import { IStaticDataHelper } from "../helpers/interfaces/IStaticDataHelper";

@injectable()
export class FetchStaticDataFromRemoteSourceTask extends AbstractEmptyTask {
    public readonly queueName = "fetchStaticDataFromRemoteSource";
    public readonly queueTtl = 59 * 60 * 1000; // 59min

    constructor(
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(ModuleContainerToken.StaticDataHelper) private staticDataHelper: IStaticDataHelper,
        @inject(ModuleContainerToken.StaticDataSourceFactory) private staticDataSourceFactory: IStaticDataSourceFactory,
        @inject(ModuleContainerToken.StaticDataRepository) private staticDataRepository: StaticDataRepository
    ) {
        super(STATIC_DATA_WORKER_NAME);
    }

    protected async execute(): Promise<void> {
        const resources = this.staticDataHelper.getStaticDataResources();

        for (const provider of Object.keys(resources)) {
            const resourceTypes = Object.values(resources[provider as StaticDataProvider]);

            if (resourceTypes.length === 0) {
                this.logger.warn(`${this.queueName}: no resources for ${provider}`);
                continue;
            }

            await this.processProviderResourceTypes(provider as StaticDataProvider, resourceTypes);
        }

        await QueueManager.sendMessageToExchange(this.queuePrefix, "refreshStaticDataInDb", {});
    }

    private async processProviderResourceTypes(provider: StaticDataProvider, resources: StaticDataResourceType[]): Promise<void> {
        for (const resourceType of resources) {
            const dataSource = this.staticDataSourceFactory.getDataSource(provider, resourceType);

            try {
                let data = await dataSource.getAll();
                if (!Array.isArray(data)) {
                    data = [data];
                }

                await this.staticDataRepository.saveData(provider, resourceType, data);
            } catch (err) {
                if (err instanceof AbstractGolemioError) {
                    const errorInfo = err.toObject().error_info ?? err.message;
                    this.logger.error(`${this.queueName}: error while refreshing data ${provider}/${resourceType}: ${errorInfo}`);
                    continue;
                }

                throw new GeneralError("Static data refresh error", this.constructor.name, err);
            }
        }
    }
}
