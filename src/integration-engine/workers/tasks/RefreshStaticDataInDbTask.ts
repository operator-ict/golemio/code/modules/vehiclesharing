import { CeskyCarsharingStaticDataService } from "#ie/CeskyCarsharingStaticDataService";
import { HoppyGoStaticDataService } from "#ie/HoppyGoStaticDataService";
import { ModuleContainerToken } from "#ie/ioc";
import { StaticDataRepository } from "#ie/repositories/StaticDataRepository";
import { RekolaStaticDataService } from "#ie/transformations/Rekola/RekolaStaticDataService";
import { StaticDataProvider } from "#sch/definitions/models/helpers/StaticDataProviderEnum";
import { StaticDataResourceType } from "#sch/definitions/models/helpers/StaticDataResourceTypeEnum";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { STATIC_DATA_WORKER_NAME } from "../constants";
import { IStaticDataHelper } from "../helpers/interfaces/IStaticDataHelper";

@injectable()
export class RefreshStaticDataInDbTask extends AbstractEmptyTask {
    public readonly queueName = "refreshStaticDataInDb";
    public readonly queueTtl = 59 * 60 * 1000; // 59min

    constructor(
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(ModuleContainerToken.StaticDataHelper) private staticDataHelper: IStaticDataHelper,
        @inject(ModuleContainerToken.StaticDataRepository) private staticDataRepository: StaticDataRepository,
        @inject(ModuleContainerToken.CeskyCarsharingStaticDataService)
        private ceskyCarsharingDataService: CeskyCarsharingStaticDataService,
        @inject(ModuleContainerToken.HoppyGoStaticDataService) private hoppyGoDataService: HoppyGoStaticDataService,
        @inject(ModuleContainerToken.RekolaStaticDataService) private rekolaDataService: RekolaStaticDataService
    ) {
        super(STATIC_DATA_WORKER_NAME);
    }

    protected async execute(): Promise<void> {
        const resources = this.staticDataHelper.getStaticDataResources();

        for (const provider of Object.keys(resources)) {
            const resourceTypes = Object.values(resources[provider as StaticDataProvider]);

            if (resourceTypes.length === 0) {
                this.logger.warn(`${this.queueName}: no resources for ${provider}`);
                continue;
            }

            try {
                await this.processProviderResourceTypes(provider as StaticDataProvider, resourceTypes);
            } catch (err) {
                if (err instanceof AbstractGolemioError) {
                    const errorInfo = err.toObject().error_info ?? err.message;
                    this.logger.error(`${this.queueName}: error while refreshing data ${provider}: ${errorInfo}`);
                    continue;
                }

                throw new GeneralError(`Static data refresh error: ${provider}`, this.constructor.name, err);
            }
        }
    }

    private async processProviderResourceTypes(provider: StaticDataProvider, resources: StaticDataResourceType[]): Promise<void> {
        const staticData = await this.staticDataRepository.getData(provider, resources);

        if (Object.keys(staticData).length === 0) {
            this.logger.warn(`${this.queueName}: no data for ${provider}`);
            return;
        }

        switch (provider) {
            case StaticDataProvider.CeskyCarsharing:
                await this.ceskyCarsharingDataService.saveSystemsStaticData(staticData);
                break;
            case StaticDataProvider.HoppyGo:
                await this.hoppyGoDataService.saveSystemBasics(staticData);
                break;
            case StaticDataProvider.Rekola:
                await this.rekolaDataService.saveRekolaStaticData(staticData);
                break;
            default:
                this.logger.warn(`${this.queueName}: unknown provider ${provider}`);
        }
    }
}
