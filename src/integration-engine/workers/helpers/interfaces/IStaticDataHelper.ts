import { StaticDataProvider } from "#sch/definitions/models/helpers/StaticDataProviderEnum";
import { StaticDataResourceType } from "#sch/definitions/models/helpers/StaticDataResourceTypeEnum";

export type ProviderResourceDict = {
    [key in StaticDataProvider]: {
        [s: string]: StaticDataResourceType;
    };
};

export interface IStaticDataHelper {
    getBaseUrl(): string;
    getStaticDataResources(): ProviderResourceDict;
}
