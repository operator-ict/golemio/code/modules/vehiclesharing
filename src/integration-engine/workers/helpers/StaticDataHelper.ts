import { StaticDataProvider } from "#sch/definitions/models/helpers/StaticDataProviderEnum";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IStaticDataHelper, ProviderResourceDict } from "./interfaces/IStaticDataHelper";

@injectable()
export class StaticDataHelper implements IStaticDataHelper {
    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getBaseUrl(): string {
        return this.config.getValue<string>("old.datasources.vehiclesharing.staticData.baseUrl");
    }

    public getStaticDataResources(): ProviderResourceDict {
        const defaultResources: ProviderResourceDict = {
            [StaticDataProvider.CeskyCarsharing]: {},
            [StaticDataProvider.HoppyGo]: {},
            [StaticDataProvider.Rekola]: {},
        };

        return this.config.getValue(
            "old.datasources.vehiclesharing.staticData.resources",
            defaultResources
        ) as ProviderResourceDict;
    }
}
