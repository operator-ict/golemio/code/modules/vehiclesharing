import { ModuleContainerToken, SharedVehiclesContainer } from "#ie/ioc";
import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine";
import { STATIC_DATA_WORKER_NAME } from "./constants";

export class StaticDataWorker extends AbstractWorker {
    protected readonly name = STATIC_DATA_WORKER_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(SharedVehiclesContainer.resolve(ModuleContainerToken.FetchStaticDataFromRemoteSourceTask));
        this.registerTask(SharedVehiclesContainer.resolve(ModuleContainerToken.RefreshStaticDataInDbTask));
    }

    public registerTask = (task: AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
