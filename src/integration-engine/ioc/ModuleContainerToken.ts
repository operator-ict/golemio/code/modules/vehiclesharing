const ModuleContainerToken = {
    //#region Static data
    StaticDataHelper: Symbol(),
    StaticDataSourceFactory: Symbol(),
    StaticDataRepository: Symbol(),
    FetchStaticDataFromRemoteSourceTask: Symbol(),
    RefreshStaticDataInDbTask: Symbol(),
    //#endregion

    RentalAppsModel: Symbol(),
    VehicleStatusModel: Symbol(),
    SystemInformationModel: Symbol(),
    StationInformationHistoryModel: Symbol(),
    MobilityOperatorTransformation: Symbol(),
    MobilityOperatorDataSource: Symbol(),
    MobilityOperatorRentalAppProvider: Symbol(),

    CeskyCarsharingGeofencingTransformation: Symbol(),
    CeskyCarsharingVehicleStatusTransformation: Symbol(),
    CeskyCarsharingPricingPlanMatcher: Symbol(),

    CeskyCarsharingStaticDataService: Symbol(),
    RekolaStaticDataService: Symbol(),
    HoppyGoStaticDataService: Symbol(),

    GeoFilter: Symbol(),
    GeofencingZonesModel: Symbol(),

    OldDataCleanerHelper: Symbol(),
    PricingModel: Symbol(),
    PricingPlansModel: Symbol(),
    VehicleTypesModel: Symbol(),
    StationStatusVehicleTypeModel: Symbol(),
};

export { ModuleContainerToken };
