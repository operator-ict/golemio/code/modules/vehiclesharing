import { CeskyCarsharingStaticDataService } from "#ie/CeskyCarsharingStaticDataService";
import { HoppyGoStaticDataService } from "#ie/HoppyGoStaticDataService";
import { MobilityOperatorDataSourceFactory } from "#ie/dataSources/MobilityOperatorDataSourceFactory";
import { StaticDataSourceFactory } from "#ie/dataSources/staticData/StaticDataSourceFactory";
import { GeoFilter } from "#ie/helpers/GeoFilter";
import { OldDataCleanerHelper } from "#ie/helpers/OldDataCleanerHelper";
import { GeofencingZonesModel } from "#ie/models/GeofencingZonesModel";
import { RentalAppsModel } from "#ie/models/RentalAppsModel";
import { StationInformationHistoryModel } from "#ie/models/StationInformationHistoryModel";
import { SystemInformationModel } from "#ie/models/SystemInformationModel";
import { VehicleStatusModel } from "#ie/models/VehicleStatusModel";
import { StaticDataRepository } from "#ie/repositories/StaticDataRepository";
import { CeskyCarsharingGeofencingTransformation } from "#ie/transformations";
import { CeskyCarsharingVehicleStatusTransformation } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingVehicleStatusTransformation";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { MobilityOperatorTransformation } from "#ie/transformations/MobilityOperator/MobilityOperatorTransformation";
import { RekolaStaticDataService } from "#ie/transformations/Rekola/RekolaStaticDataService";
import { StaticDataHelper } from "#ie/workers/helpers/StaticDataHelper";
import { FetchStaticDataFromRemoteSourceTask } from "#ie/workers/tasks/FetchStaticDataFromRemoteSourceTask";
import { RefreshStaticDataInDbTask } from "#ie/workers/tasks/RefreshStaticDataInDbTask";
import { DataSource } from "@golemio/core/dist/integration-engine";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer, instanceCachingFactory } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainerToken";
import { CeskyCarsharingPricingPlanMatcher } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingPricingPlanMatcher";
import { PricingModel } from "#ie/models/PricingModel";
import { PricingPlansModel } from "#ie/models/PricingPlansModel";
import { VehicleTypesModel } from "#ie/models/VehicleTypesModel";
import { StationStatusVehicleTypeModel } from "#ie/models/StationStatusVehicleTypeModel";

//#region Initialization
const SharedVehiclesContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Helpers
SharedVehiclesContainer.registerSingleton<GeoFilter>(ModuleContainerToken.GeoFilter, GeoFilter);
SharedVehiclesContainer.registerSingleton<OldDataCleanerHelper>(ModuleContainerToken.OldDataCleanerHelper, OldDataCleanerHelper);
SharedVehiclesContainer.registerSingleton(ModuleContainerToken.StaticDataHelper, StaticDataHelper);
SharedVehiclesContainer.registerSingleton(
    ModuleContainerToken.CeskyCarsharingPricingPlanMatcher,
    CeskyCarsharingPricingPlanMatcher
);
//#endregion

//#region Datasources
SharedVehiclesContainer.register(ModuleContainerToken.StaticDataSourceFactory, StaticDataSourceFactory);
SharedVehiclesContainer.register<DataSource>(ModuleContainerToken.MobilityOperatorDataSource, {
    useFactory: instanceCachingFactory<DataSource>(() => MobilityOperatorDataSourceFactory.getDataSource()),
});
//#endregion

//#region Transformations
SharedVehiclesContainer.registerSingleton<MobilityOperatorTransformation>(
    ModuleContainerToken.MobilityOperatorTransformation,
    MobilityOperatorTransformation
);
SharedVehiclesContainer.registerSingleton<MobilityOperatorRentalAppProvider>(
    ModuleContainerToken.MobilityOperatorRentalAppProvider,
    MobilityOperatorRentalAppProvider
);
SharedVehiclesContainer.registerSingleton<CeskyCarsharingGeofencingTransformation>(
    ModuleContainerToken.CeskyCarsharingGeofencingTransformation,
    CeskyCarsharingGeofencingTransformation
);
SharedVehiclesContainer.registerSingleton<CeskyCarsharingVehicleStatusTransformation>(
    ModuleContainerToken.CeskyCarsharingVehicleStatusTransformation,
    CeskyCarsharingVehicleStatusTransformation
);
//#endregion

//#region Repositories
SharedVehiclesContainer.registerSingleton(ModuleContainerToken.StaticDataRepository, StaticDataRepository);
SharedVehiclesContainer.registerSingleton<SystemInformationModel>(
    ModuleContainerToken.SystemInformationModel,
    SystemInformationModel
);
SharedVehiclesContainer.registerSingleton<RentalAppsModel>(ModuleContainerToken.RentalAppsModel, RentalAppsModel);
SharedVehiclesContainer.registerSingleton<VehicleStatusModel>(ModuleContainerToken.VehicleStatusModel, VehicleStatusModel);
SharedVehiclesContainer.registerSingleton<VehicleTypesModel>(ModuleContainerToken.VehicleTypesModel, VehicleTypesModel);
SharedVehiclesContainer.registerSingleton<StationInformationHistoryModel>(
    ModuleContainerToken.StationInformationHistoryModel,
    StationInformationHistoryModel
);
SharedVehiclesContainer.registerSingleton<GeofencingZonesModel>(ModuleContainerToken.GeofencingZonesModel, GeofencingZonesModel);
SharedVehiclesContainer.registerSingleton<PricingModel>(ModuleContainerToken.PricingModel, PricingModel);
SharedVehiclesContainer.registerSingleton<PricingPlansModel>(ModuleContainerToken.PricingPlansModel, PricingPlansModel);
SharedVehiclesContainer.registerSingleton<StationStatusVehicleTypeModel>(
    ModuleContainerToken.StationStatusVehicleTypeModel,
    StationStatusVehicleTypeModel
);
//#endregion

//#region Services
SharedVehiclesContainer.registerSingleton<CeskyCarsharingStaticDataService>(
    ModuleContainerToken.CeskyCarsharingStaticDataService,
    CeskyCarsharingStaticDataService
);
SharedVehiclesContainer.registerSingleton<RekolaStaticDataService>(
    ModuleContainerToken.RekolaStaticDataService,
    RekolaStaticDataService
);
SharedVehiclesContainer.registerSingleton(ModuleContainerToken.HoppyGoStaticDataService, HoppyGoStaticDataService);
//#endregion

//#region Tasks
SharedVehiclesContainer.registerSingleton(
    ModuleContainerToken.FetchStaticDataFromRemoteSourceTask,
    FetchStaticDataFromRemoteSourceTask
);
SharedVehiclesContainer.registerSingleton(ModuleContainerToken.RefreshStaticDataInDbTask, RefreshStaticDataInDbTask);
//#endregion

export { SharedVehiclesContainer };
