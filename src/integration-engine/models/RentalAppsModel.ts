import { Vehiclesharing } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
@injectable()
export class RentalAppsModel extends PostgresModel implements IModel {
    constructor() {
        super(
            Vehiclesharing.definitions.rentalApps.name + "Model",
            {
                outputSequelizeAttributes: Vehiclesharing.definitions.rentalApps.outputSequelizeAttributes,
                pgTableName: Vehiclesharing.definitions.rentalApps.pgTableName,
                pgSchema: Vehiclesharing.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                Vehiclesharing.definitions.rentalApps.name + "ModelValidator",
                Vehiclesharing.definitions.rentalApps.outputJsonSchema
            )
        );
    }

    public deleteOldData = async (): Promise<void> => {
        try {
            await this.sequelizeModel.sequelize?.query(`CALL ${Vehiclesharing.pgSchema}.rental_apps_data_retention()`);
        } catch (err) {
            throw new GeneralError("Error while deleting old data" + err.message, this.constructor.name, err);
        }
    };
}
