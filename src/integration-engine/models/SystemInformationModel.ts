import { ISharedVehiclesSystemInformationOutput, Vehiclesharing } from "#sch";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Model } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class SystemInformationModel extends PostgresModel implements IModel {
    constructor() {
        super(
            Vehiclesharing.definitions.systemInformation.name + "Model",
            {
                outputSequelizeAttributes: Vehiclesharing.definitions.systemInformation.outputSequelizeAttributes,
                pgTableName: Vehiclesharing.definitions.systemInformation.pgTableName,
                pgSchema: Vehiclesharing.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                Vehiclesharing.definitions.systemInformation.name + "ModelValidator",
                Vehiclesharing.definitions.systemInformation.outputJsonSchema
            )
        );
    }

    public deleteBySystemId = async (systemId: string): Promise<void> => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            const models = await this.sequelizeModel.findAll<Model>({
                where: {
                    system_id: systemId,
                },
                transaction: t,
            });

            for (const model of models) {
                await model.destroy({ transaction: t });
            }

            await t.commit();
        } catch (err) {
            await t.rollback();
            throw new GeneralError("Error while deleting system_information", this.constructor.name, err);
        }
    };

    public GetOne(id: string): Promise<ISharedVehiclesSystemInformationOutput> {
        return this.sequelizeModel.findOne({
            where: {
                system_id: id,
            },
            raw: true,
        });
    }
}
