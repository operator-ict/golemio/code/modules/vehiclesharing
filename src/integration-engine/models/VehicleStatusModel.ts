import { GeoFilter } from "#ie/helpers/GeoFilter";
import { SharedVehiclesContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ISharedVehicleStatusOutput, Vehiclesharing } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { Transaction } from "@golemio/core/dist/shared/sequelize";

@injectable()
export class VehicleStatusModel extends PostgresModel implements IModel {
    private readonly geoFilter: GeoFilter;

    constructor() {
        super(
            Vehiclesharing.definitions.vehicleStatus.name + "Model",
            {
                outputSequelizeAttributes: Vehiclesharing.definitions.vehicleStatus.outputSequelizeAttributes,
                pgTableName: Vehiclesharing.definitions.vehicleStatus.pgTableName,
                pgSchema: Vehiclesharing.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                Vehiclesharing.definitions.vehicleStatus.name + "ModelValidator",
                Vehiclesharing.definitions.vehicleStatus.outputJsonSchema
            )
        );

        this.geoFilter = SharedVehiclesContainer.resolve<GeoFilter>(ModuleContainerToken.GeoFilter);
    }

    /**
     * Delete data processed before a given `processed_at` date/time
     *
     * @param processedAt The date/time before which the data should be deleted
     * @param systemId The ID of the system to delete data from
     */
    public async deleteDataBefore(processedAt: Date, systemId: string): Promise<number> {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    system_id: {
                        [Sequelize.Op.eq]: systemId,
                    },
                    processed_at: {
                        [Sequelize.Op.lt]: processedAt,
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    }

    public saveIfInCzechia = async (
        vehicles: ISharedVehicleStatusOutput[],
        transaction: Transaction | null = null
    ): Promise<void> => {
        const filtered = vehicles.filter((vehicle) =>
            this.geoFilter.isInCzechia(vehicle.point.coordinates[1], vehicle.point.coordinates[0])
        );
        await this.bulkSave(filtered, undefined, false, false, transaction);
    };
}
