import { GeoFilter } from "#ie/helpers/GeoFilter";
import { SharedVehiclesContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ISharedVehiclesStationInformationOutput, Vehiclesharing } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction } from "@golemio/core/dist/shared/sequelize";

export class StationInformationModel extends PostgresModel implements IModel {
    private readonly geoFilter: GeoFilter;

    constructor() {
        super(
            Vehiclesharing.definitions.stationInformation.name + "Model",
            {
                outputSequelizeAttributes: Vehiclesharing.definitions.stationInformation.outputSequelizeAttributes,
                pgTableName: Vehiclesharing.definitions.stationInformation.pgTableName,
                pgSchema: Vehiclesharing.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                Vehiclesharing.definitions.stationInformation.name + "ModelValidator",
                Vehiclesharing.definitions.stationInformation.outputJsonSchema
            )
        );

        this.geoFilter = SharedVehiclesContainer.resolve<GeoFilter>(ModuleContainerToken.GeoFilter);
    }

    public saveIfInCzechia = async (
        station: ISharedVehiclesStationInformationOutput[],
        transaction: Transaction | null = null
    ): Promise<void> => {
        const filtered = station.filter((station) =>
            this.geoFilter.isInCzechia(station.point.coordinates[1], station.point.coordinates[0])
        );
        await this.bulkSave(filtered, undefined, false, false, transaction);
    };
}
