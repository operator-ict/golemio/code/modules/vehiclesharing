import { Vehiclesharing } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
@injectable()
export class VehicleTypesModel extends PostgresModel implements IModel {
    constructor() {
        super(
            Vehiclesharing.definitions.vehicleTypes.name + "Model",
            {
                outputSequelizeAttributes: Vehiclesharing.definitions.vehicleTypes.outputSequelizeAttributes,
                pgTableName: Vehiclesharing.definitions.vehicleTypes.pgTableName,
                pgSchema: Vehiclesharing.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                Vehiclesharing.definitions.vehicleTypes.name + "ModelValidator",
                Vehiclesharing.definitions.vehicleTypes.outputJsonSchema
            )
        );
    }

    public async deleteOrphans(idPrefix: string): Promise<number> {
        Transaction;
        try {
            const query =
                `DELETE FROM "${Vehiclesharing.pgSchema}"."${this.tableName}" vt ` +
                `WHERE vt.id IN ( ` +
                `  SELECT vt.id ` +
                `  FROM "${Vehiclesharing.pgSchema}"."${this.tableName}" vt ` +
                `  LEFT JOIN "${Vehiclesharing.pgSchema}".vehicle_status vs ` +
                `    ON vt.id = vs.vehicle_type_id ` +
                `  LEFT JOIN "${Vehiclesharing.pgSchema}".station_status_vehicle_type ssvt ` +
                `    ON vt.id = ssvt.vehicle_type_id ` +
                `  WHERE vs.vehicle_type_id IS NULL ` +
                `    AND ssvt.vehicle_type_id IS NULL ` +
                `    AND vt.id LIKE '${idPrefix}%' ` +
                `);`;
            const result: any = await this.query(query);
            return result[1].rowCount;
        } catch (err) {
            throw new GeneralError("Error while deleting orphaned data: " + err.message, this.constructor.name, err);
        }
    }

    /**
     * Delete old data by systemId processed before a given `processed_at` date/time
     *
     * @param processedAt The date/time before which the data should be deleted
     * @param systemId  The ID of the provider to delete data from
     * @param transaction  The running sequelize transaction
     */
    public async deleteDataBefore(processedAt: Date, systemId: string, transaction?: Transaction): Promise<number> {
        try {
            const query =
                `DELETE FROM "${Vehiclesharing.pgSchema}"."${this.tableName}" vt ` +
                `WHERE vt.id IN ( ` +
                `  SELECT vt.id ` +
                `  FROM "${Vehiclesharing.pgSchema}"."${this.tableName}" vt ` +
                `  INNER JOIN "${Vehiclesharing.pgSchema}".vehicle_status vs ` +
                `    ON vt.id = vs.vehicle_type_id ` +
                `  WHERE vs.system_id = '${systemId}'` +
                `    AND vt.updated_at < '${processedAt.toISOString()}'` +
                `);`;
            const result: any = await this.sequelizeModel.sequelize!.query(query, { transaction });
            return result[1].rowCount;
        } catch (err) {
            throw new GeneralError("Error while deleting data: " + err.message, this.constructor.name, err);
        }
    }
}
