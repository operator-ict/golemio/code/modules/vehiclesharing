import { Vehiclesharing } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { Transaction } from "@golemio/core/dist/shared/sequelize";

export class PricingPlansModel extends PostgresModel implements IModel {
    constructor() {
        super(
            Vehiclesharing.definitions.pricingPlans.name + "Model",
            {
                outputSequelizeAttributes: Vehiclesharing.definitions.pricingPlans.outputSequelizeAttributes,
                pgTableName: Vehiclesharing.definitions.pricingPlans.pgTableName,
                pgSchema: Vehiclesharing.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                Vehiclesharing.definitions.pricingPlans.name + "ModelValidator",
                Vehiclesharing.definitions.pricingPlans.outputJsonSchema
            )
        );
    }

    /**
     * Delete old data by systemId processed before a given `processed_at` date/time
     *
     * @param processedAt The date/time before which the data should be deleted
     * @param systemId  The ID of the provider to delete data from
     * @param transaction  The running sequelize transaction
     */
    public async deleteDataBefore(processedAt: Date, systemId: string, transaction: Transaction): Promise<number> {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    system_id: {
                        [Sequelize.Op.eq]: systemId,
                    },
                    updated_at: {
                        [Sequelize.Op.lt]: processedAt,
                    },
                },
                transaction: transaction,
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    }
}
