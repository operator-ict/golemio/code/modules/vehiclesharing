import { Vehiclesharing } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { StationStatusModel } from "./StationStatusModel";
import { VehicleStatusModel } from "./VehicleStatusModel";

@injectable()
export class StationInformationHistoryModel extends PostgresModel implements IModel {
    private stationStatusModel: StationStatusModel;
    private vehicleStatusModel: VehicleStatusModel;

    constructor() {
        super(
            Vehiclesharing.definitions.stationInformation.name + "Model",
            {
                outputSequelizeAttributes: Vehiclesharing.definitions.stationInformation.outputSequelizeAttributes,
                pgTableName: Vehiclesharing.definitions.stationInformation.pgTableName,
                pgSchema: Vehiclesharing.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                Vehiclesharing.definitions.stationInformation.name + "ModelValidator",
                Vehiclesharing.definitions.stationInformation.outputJsonSchema
            )
        );

        this.stationStatusModel = new StationStatusModel();
        this.vehicleStatusModel = new VehicleStatusModel();

        // Create associations
        this.stationStatusModel["sequelizeModel"].belongsTo(this.sequelizeModel, {
            foreignKey: "station_id",
        });
        this.sequelizeModel.hasOne(this.stationStatusModel["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "station_id",
        });

        this.vehicleStatusModel["sequelizeModel"].belongsTo(this.sequelizeModel, {
            foreignKey: "station_id",
        });
        this.sequelizeModel.hasMany(this.vehicleStatusModel["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "station_id",
            as: "bike_status",
        });
    }

    /**
     * Delete data processed before a given `processed_at` date/time
     *
     * @param processedAt The date/time before which the data should be deleted
     * @param systemId The ID of the system to delete data from
     */
    public async deleteDataBefore(processedAt: Date, systemId: string): Promise<void> {
        try {
            await this.sequelizeModel.sequelize?.query(`CALL ${Vehiclesharing.pgSchema}.station_data_retention($1,$2)`, {
                bind: [processedAt.toISOString(), systemId],
                plain: true,
                type: QueryTypes.SELECT,
            });
        } catch (err) {
            throw new GeneralError("Error while deleting old data" + err.message, this.constructor.name, err);
        }
    }
}
