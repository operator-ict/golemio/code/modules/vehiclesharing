import { Vehiclesharing } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { Transaction } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PricingModel extends PostgresModel implements IModel {
    constructor() {
        super(
            Vehiclesharing.definitions.pricings.name + "Model",
            {
                outputSequelizeAttributes: Vehiclesharing.definitions.pricings.outputSequelizeAttributes,
                pgTableName: Vehiclesharing.definitions.pricings.pgTableName,
                pgSchema: Vehiclesharing.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                Vehiclesharing.definitions.pricings.name + "ModelValidator",
                Vehiclesharing.definitions.pricings.outputJsonSchema
            )
        );
    }

    /**
     * Delete old data by systemId processed before a given `processed_at` date/time
     *
     * @param processedAt The date/time before which the data should be deleted
     * @param systemId  The ID of the provider to delete data from
     * @param transaction  The running sequelize transaction
     */
    public async deleteDataBefore(processedAt: Date, systemId: string, transaction: Transaction): Promise<number> {
        try {
            const query =
                `DELETE FROM "${Vehiclesharing.pgSchema}"."${this.tableName}" pr ` +
                `WHERE pr.pricing_plan_id IN ( ` +
                `  SELECT pr.pricing_plan_id ` +
                `  FROM "${Vehiclesharing.pgSchema}"."${this.tableName}" pr ` +
                `  INNER JOIN "${Vehiclesharing.pgSchema}".pricing_plans pp ` +
                `    ON pp.id = pr.pricing_plan_id ` +
                `  WHERE pp.system_id = '${systemId}'` +
                `    AND pr.updated_at < '${processedAt.toISOString()}'` +
                `);`;
            const result: any = await this.sequelizeModel.sequelize!.query(query, { transaction });

            return result[1].rowCount;
        } catch (err) {
            throw new GeneralError("Error while deleting data by systemId", this.constructor.name, err);
        }
    }
}
