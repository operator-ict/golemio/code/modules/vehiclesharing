import { Vehiclesharing } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class StationStatusVehicleTypeModel extends PostgresModel implements IModel {
    constructor() {
        super(
            Vehiclesharing.definitions.stationsStatusVehicleType.name + "Model",
            {
                outputSequelizeAttributes: Vehiclesharing.definitions.stationsStatusVehicleType.outputSequelizeAttributes,
                pgTableName: Vehiclesharing.definitions.stationsStatusVehicleType.pgTableName,
                pgSchema: Vehiclesharing.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                Vehiclesharing.definitions.stationsStatusVehicleType.name + "ModelValidator",
                Vehiclesharing.definitions.stationsStatusVehicleType.outputJsonSchema
            )
        );
    }

    /**
     * Delete data processed before a given `processed_at` date/time
     *
     * @param processedAt The date/time before which the data should be deleted
     * @param systemId The ID of the system to delete data from
     */
    public async deleteDataBefore(processedAt: Date, systemId: string, transaction: Transaction): Promise<number> {
        try {
            const query =
                `DELETE FROM "${Vehiclesharing.pgSchema}"."${this.tableName}" ssvt ` +
                `WHERE ssvt.station_id IN ( ` +
                `  SELECT ssvt.station_id ` +
                `  FROM "${Vehiclesharing.pgSchema}"."${this.tableName}" ssvt ` +
                `  INNER JOIN "${Vehiclesharing.pgSchema}".station_information si ` +
                `    ON si.id = ssvt.station_id ` +
                `  WHERE si.system_id = '${systemId}'` +
                `    AND ssvt.processed_at < '${processedAt.toISOString()}'` +
                `) AND ssvt.processed_at < '${processedAt.toISOString()}';`;
            const result: any = await this.sequelizeModel.sequelize!.query(query, { transaction });
            return result[1].rowCount;
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    }

    public deleteOrphans = async (idPrefix: string): Promise<number> => {
        try {
            const query =
                `delete from ${Vehiclesharing.pgSchema}.station_status_vehicle_type svt where svt.station_id in (` +
                `select ss.station_id from ${Vehiclesharing.pgSchema}.station_status ss ` +
                `left join ${Vehiclesharing.pgSchema}.station_status_vehicle_type ssvt on ss.station_id = ssvt.station_id ` +
                `where ss.num_bikes_available = 0 and ssvt.num_bikes_available > 0 and ssvt.station_id like '${idPrefix}%');`;
            const result: any = await this.query(query);
            return result[1].rowCount;
        } catch (err) {
            throw new GeneralError("Error while deleting orphaned data: " + err.message, this.constructor.name, err);
        }
    };
}
