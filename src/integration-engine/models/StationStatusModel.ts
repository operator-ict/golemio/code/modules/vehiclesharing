import { Vehiclesharing } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction } from "@golemio/core/dist/shared/sequelize";

export class StationStatusModel extends PostgresModel implements IModel {
    constructor() {
        super(
            Vehiclesharing.definitions.stationStatus.name + "Model",
            {
                outputSequelizeAttributes: Vehiclesharing.definitions.stationStatus.outputSequelizeAttributes,
                pgTableName: Vehiclesharing.definitions.stationStatus.pgTableName,
                pgSchema: Vehiclesharing.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                Vehiclesharing.definitions.stationStatus.name + "ModelValidator",
                Vehiclesharing.definitions.stationStatus.outputJsonSchema
            )
        );
    }

    /**
     * Delete data processed before a given `processed_at` date/time
     *
     * @param processedAt The date/time before which the data should be deleted
     * @param systemId The ID of the system to delete data from
     */
    public async deleteDataBefore(processedAt: Date, systemId: string, transaction: Transaction): Promise<number> {
        try {
            const query =
                `DELETE FROM "${Vehiclesharing.pgSchema}"."${this.tableName}" ss ` +
                `WHERE ss.station_id IN ( ` +
                `  SELECT ss.station_id ` +
                `  FROM "${Vehiclesharing.pgSchema}"."${this.tableName}" ss ` +
                `  INNER JOIN "${Vehiclesharing.pgSchema}".station_information si ` +
                `    ON si.id = ss.station_id ` +
                `  WHERE si.system_id = '${systemId}'` +
                `    AND ss.processed_at < '${processedAt.toISOString()}'` +
                `);`;
            const result: any = await this.sequelizeModel.sequelize!.query(query, { transaction });
            return result[1].rowCount;
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    }
}
