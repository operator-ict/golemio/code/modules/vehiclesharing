import { IGeoJSONFeatureCollection } from "@golemio/core/dist/output-gateway";

export interface IGeofencingZonesJson {
    geofencing_zones: IGeoJSONFeatureCollection;
}

export interface IGeofencingZoneProperties {
    name: string | null;
    note: string | null;
    source: string | null;
    priority: number;
    start: string | null;
    end: string | null;
    rules: IGeofencingZoneRules[];
}

interface IGeofencingZoneRules {
    system_id: string;
    ride_allowed: boolean;
    ride_through_allowed: boolean;
    maximum_speed_kph: number | null;
    parking_allowed: boolean;
    price: number | null;
}
