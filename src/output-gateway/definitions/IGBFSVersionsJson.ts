export interface IGBFSVersionsJson {
    versions: [
        {
            version: string;
            url: string;
        }
    ];
}
