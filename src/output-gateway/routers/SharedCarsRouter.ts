/**
 *
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */
import { VehicleSharingContainer } from "#og/ioc/Di";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { parseCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc/ContainerToken";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { carsFeatureCollectionBuilder } from "#og/helpers/buildGeoFeature";
import { ISharedVehiclesQueryParams } from "#og/interfaces/ISharedVehiclesQueryParams";
import { models } from "#og/models";
import { VehicleStatusModel } from "#og/models/VehicleStatusModel";

export class SharedCarsRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    private bikeStatusModel: VehicleStatusModel;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;

    public constructor() {
        super();
        this.bikeStatusModel = models.VehicleStatusModel;
        this.cacheHeaderMiddleware = VehicleSharingContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.initRoutes();
    }

    private GetAll = async (
        req: Request<unknown, unknown, unknown, ISharedVehiclesQueryParams>,
        res: Response,
        next: NextFunction
    ) => {
        let companyNames = req.query.companyNames;

        if (companyNames) {
            companyNames = this.ConvertToArray(companyNames);
        } else {
            companyNames = undefined;
        }

        try {
            let coords: { lat: number | undefined; lng: number | undefined; range: number | undefined } = {
                lat: undefined,
                lng: undefined,
                range: undefined,
            };
            if (req.query.latlng) {
                coords = await parseCoordinates(req.query.latlng, req.query.range || "0");
            }
            const data = await this.bikeStatusModel.GetAll({
                lat: coords.lat,
                lng: coords.lng,
                range: coords.range,
                limit: req.query.limit,
                offset: req.query.offset,
                updatedSince: req.query.updatedSince,
                companyNames: companyNames,
                vehicleTypes: ["car"],
            });

            res.status(200).send(carsFeatureCollectionBuilder.buildFeatureCollection(data));
        } catch (err) {
            next(err);
        }
    };

    private GetOne = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.bikeStatusModel.GetOne(req.params.vehicleId);
            if (!data) {
                throw new GeneralError("not_found", "SharedCarsRouter", undefined, 404);
            }

            res.status(200).send(carsFeatureCollectionBuilder.buildFeatureItem(data));
        } catch (err) {
            next(err);
        }
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     * @param {number|string} expire TTL for the caching middleware
     */
    private initRoutes = (): void => {
        this.router.get(
            "/",
            [
                query("latlng").optional().isLatLong().not().isArray(),
                query("range").optional().isNumeric().toInt().not().isArray(),
                query("limit").optional().isNumeric().toInt().not().isArray(),
                query("offset").optional().isNumeric().toInt().not().isArray(),
                query("companyName").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                query("companyNames").optional().not().isEmpty({ ignore_whitespace: true }),
                query("updatedSince").optional().isISO8601().not().isArray(),
                query("districts").optional().not().isEmpty({ ignore_whitespace: true }),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SharedCarsRouter"),
            this.cacheHeaderMiddleware.getMiddleware(60, 5),
            this.GetAll
        );

        this.router.get(
            "/:vehicleId",
            [param("vehicleId").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SharedCarsRouter"),
            this.cacheHeaderMiddleware.getMiddleware(60, 5),
            this.GetOne
        );
    };
}

const sharedCarsRouter: Router = new SharedCarsRouter().router;

export { sharedCarsRouter };
