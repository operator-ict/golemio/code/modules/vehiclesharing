import { VehicleSharingContainer } from "#og/ioc/Di";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { checkErrors } from "@golemio/core/dist/output-gateway/Validation";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { query } from "@golemio/core/dist/shared/express-validator";
import { models } from "#og/models";
import { VehicleStatusModel } from "#og/models/VehicleStatusModel";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { VectorTile } from "@mapbox/vector-tile";
import Pbf from "pbf";

export class MVTSRouter extends AbstractRouter {
    private bikeStatusModel: VehicleStatusModel;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;

    public constructor() {
        super("v2", "vehiclesharing/mvts");
        this.bikeStatusModel = models.VehicleStatusModel;
        this.cacheHeaderMiddleware = VehicleSharingContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.initRoutes();
    }

    protected initRoutes = (): void => {
        this.router.get(
            "/free_vehicle_status",
            [
                query("zoom").exists().isNumeric().not().isArray(),
                query("tileX").exists().isNumeric().not().isArray(),
                query("tileY").exists().isNumeric().not().isArray(),
                query("systemId").optional(),
            ],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(60, 5),
            this.GetFreeBikeStatusMVTS
        );
    };

    private GetFreeBikeStatusMVTS = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const tileX = parseInt(req.query.tileX as string);
            const tileY = parseInt(req.query.tileY as string);
            const zoom = parseInt(req.query.zoom as string);
            const systemId = req.query.systemId
                ? req.query.systemId instanceof Array
                    ? (req.query.systemId as string[])
                    : [req.query.systemId as string]
                : undefined;

            const mvt = await this.bikeStatusModel.GetAllMVTS(tileX, tileY, zoom, systemId);

            res.format({
                "application/vnd.mapbox-vector-tile": () => {
                    res.setHeader("Content-Type", "application/vnd.mapbox-vector-tile");
                    res.send(mvt);
                },
                // accepts both application/json and application/json; charset=utf-8
                // returns 406 for charsets we cannot provide
                "application/json; charset=utf-8": () => {
                    res.setHeader("Content-Type", "application/json");
                    res.send(new VectorTile(new Pbf(new Uint8Array(mvt))));
                },
            });
        } catch (err) {
            next(err);
        }
    };
}

const mvtsRouter: AbstractRouter = new MVTSRouter();

export { mvtsRouter };
