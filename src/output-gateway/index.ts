import { mvtsRouter } from "#og/routers/MVTSRouter";

export * from "./routers/VehiclesharingRouter";
export * from "./routers/GBFSRouter";
export * from "./routers/SharedCarsRouter";

export const routers = [mvtsRouter];
