import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { Vehiclesharing } from "#sch";

/**
 * Custom Postgres model for station status vehicle type
 */
export class StationStatusVehicleTypeModel extends SequelizeModel {
    constructor() {
        super(
            Vehiclesharing.definitions.stationStatusVehicleTypeWithSystemId.name + "Model",
            Vehiclesharing.definitions.stationStatusVehicleTypeWithSystemId.pgTableName,
            Vehiclesharing.definitions.stationStatusVehicleTypeWithSystemId.outputSequelizeAttributes,
            {
                schema: Vehiclesharing.pgSchema,
            }
        );
    }

    async GetAll(options?: any): Promise<any[]> {
        throw new Error("Method not implemented.");
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
