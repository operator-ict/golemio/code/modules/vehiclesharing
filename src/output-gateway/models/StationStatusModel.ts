import { IStationStatusJson } from "#og/definitions";
import { removeEmptyOrNull } from "#og/helpers";
import { getLatestDataLiteral } from "#og/helpers/getLatestDataLiteral";
import { Vehiclesharing } from "#sch";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { IGBFSModels } from ".";
import { StationInformationModel } from "./StationInformationModel";
import { StationStatusVehicleTypeModel } from "./StationStatusVehicleTypeModel";
import { VehicleStatusModel } from "./VehicleStatusModel";

/**
 * Custom Postgres model for station status
 */
export class StationStatusModel extends SequelizeModel {
    private stationStatusVehicleTypeModel: StationStatusVehicleTypeModel | undefined;
    private bikeStatusModel: VehicleStatusModel | undefined;
    private stationInformationModel: StationInformationModel | undefined;

    constructor() {
        super(
            Vehiclesharing.definitions.stationStatusWithSystemId.name + "Model",
            Vehiclesharing.definitions.stationStatusWithSystemId.pgTableName,
            Vehiclesharing.definitions.stationStatusWithSystemId.outputSequelizeAttributes,
            {
                schema: Vehiclesharing.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.stationStatusVehicleTypeModel = models.StationStatusVehicleTypeModel;
        this.bikeStatusModel = models.VehicleStatusModel;
        this.stationInformationModel = models.StationInformationModel;

        this.sequelizeModel.hasMany(models.StationStatusVehicleTypeModel.sequelizeModel, {
            sourceKey: "station_id",
            foreignKey: "station_id",
            as: "vehicle_types_available",
        });
        this.sequelizeModel.hasMany(models.VehicleStatusModel.sequelizeModel, {
            sourceKey: "station_id",
            foreignKey: "station_id",
            as: "bikes",
        });
        this.sequelizeModel.belongsTo(models.StationInformationModel.sequelizeModel, {
            targetKey: "id",
            foreignKey: "station_id",
        });
    };

    async GetAll(systemId: string): Promise<IStationStatusJson> {
        const results = await this.sequelizeModel.findAll({
            attributes: {
                include: [
                    [
                        Sequelize.literal(
                            `extract(epoch from date_trunc('second', "${this.sequelizeModel.tableName}".last_reported))::int`
                        ),
                        "last_reported",
                    ],
                ],
                exclude: ["processed_at", "system_id"],
            },
            include: [
                {
                    as: "vehicle_types_available",
                    model: this.stationStatusVehicleTypeModel?.sequelizeModel,
                    attributes: ["vehicle_type_id", ["num_bikes_available", "count"]],
                    required: false,
                    where: {
                        [Sequelize.Op.and]: [
                            getLatestDataLiteral(
                                this.stationStatusVehicleTypeModel!.sequelizeModel.tableName,
                                "vehicle_types_available"
                            ),
                            { num_bikes_available: { [Sequelize.Op.gt]: 0 } },
                        ],
                    },
                },
                {
                    as: "bikes",
                    model: this.bikeStatusModel?.sequelizeModel,
                    attributes: [],
                },
                {
                    model: this.stationInformationModel?.sequelizeModel,
                    attributes: [],
                    where: {
                        system_id: systemId,
                    },
                },
            ],
            where: {
                [Sequelize.Op.and]: getLatestDataLiteral(this.sequelizeModel.tableName),
            },
        });

        return {
            stations: results.map((res) => removeEmptyOrNull(res.toJSON())),
        };
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
