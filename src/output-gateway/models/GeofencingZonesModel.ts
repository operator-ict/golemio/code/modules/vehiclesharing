import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { Vehiclesharing } from "#sch";
import { IGeofencingZonesJson } from "#og/definitions/IGeofencingZonesJson";
import { removeEmptyOrNull } from "#og/helpers";

/**
 * Custom Postgres model for geofencing zones
 */
export class GeofencingZonesModel extends SequelizeModel {
    constructor() {
        super(
            Vehiclesharing.definitions.geofencingZones.name + "Model",
            Vehiclesharing.definitions.geofencingZones.pgTableName,
            Vehiclesharing.definitions.geofencingZones.outputSequelizeAttributes,
            {
                schema: Vehiclesharing.pgSchema,
            }
        );
    }

    async GetAll(systemId: string): Promise<IGeofencingZonesJson> {
        const zones = await this.sequelizeModel.findAll({
            attributes: {
                include: [
                    ["system_id", "rules.system_id"],
                    ["price", "rules.price"],
                    ["ride_allowed", "rules.ride_allowed"],
                    ["ride_through_allowed", "rules.ride_through_allowed"],
                    ["parking_allowed", "rules.parking_allowed"],
                    ["maximum_speed_kph", "rules.maximum_speed_kph"],
                ],
                exclude: [
                    "id",
                    "system_id",
                    "price",
                    "ride_allowed",
                    "ride_through_allowed",
                    "parking_allowed",
                    "maximum_speed_kph",
                ],
            },
            where: {
                system_id: systemId,
            },
            raw: true,
            nest: true,
        });

        const results: IGeofencingZonesJson = {
            geofencing_zones: {
                type: "FeatureCollection",
                features: zones.map((zone) => {
                    const { geom, ...properties } = zone;
                    return {
                        type: "Feature",
                        geometry: {
                            coordinates: geom.coordinates,
                            type: geom.type,
                        },
                        properties: {
                            ...removeEmptyOrNull(properties),
                            rules: [
                                {
                                    ...removeEmptyOrNull(properties.rules),
                                    system_id: [properties.rules.system_id],
                                },
                            ],
                        },
                    };
                }),
            },
        };

        return results;
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
