import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { Vehiclesharing } from "#sch";
import { IVehicleType, IVehicleTypesJson } from "#og/definitions";
import { IGBFSModels } from ".";
import { VehicleStatusModel } from "./VehicleStatusModel";
import { removeEmptyOrNull } from "#og/helpers";

/**
 * Custom Postgres model for vehicle types
 */
export class VehicleTypesModel extends SequelizeModel {
    private vehicleStatusModel: VehicleStatusModel | undefined;

    constructor() {
        super(
            Vehiclesharing.definitions.vehicleTypes.name + "Model",
            Vehiclesharing.definitions.vehicleTypes.pgTableName,
            Vehiclesharing.definitions.vehicleTypes.outputSequelizeAttributes,
            {
                schema: Vehiclesharing.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.vehicleStatusModel = models.VehicleStatusModel;

        this.sequelizeModel.belongsTo(models.VehicleStatusModel.sequelizeModel, {
            targetKey: "vehicle_type_id",
            foreignKey: "id",
        });
    };

    async GetAll(systemId: string): Promise<IVehicleTypesJson> {
        const results = await this.sequelizeModel.findAll({
            attributes: [
                [Sequelize.fn("DISTINCT", Sequelize.col("vehicle_types.id")), "vehicle_type_id"],
                "form_factor",
                "propulsion_type",
                "max_range_meters",
                "name",
            ],
            include: [
                {
                    model: this.vehicleStatusModel?.sequelizeModel,
                    attributes: [],
                    where: {
                        system_id: systemId,
                    },
                },
            ],
            raw: true,
        });

        return {
            vehicle_types: (results as IVehicleType[]).map((res) => removeEmptyOrNull(res)),
        };
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
