import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { Vehiclesharing } from "#sch";

/**
 * Custom Postgres model for rental apps
 */
export class RentalAppModel extends SequelizeModel {
    constructor() {
        super(
            Vehiclesharing.definitions.rentalApps.name + "Model",
            Vehiclesharing.definitions.rentalApps.pgTableName,
            Vehiclesharing.definitions.rentalApps.outputSequelizeAttributes,
            {
                schema: Vehiclesharing.pgSchema,
            }
        );
    }

    async GetAll(options?: any): Promise<any[]> {
        throw new Error("Method not implemented.");
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
