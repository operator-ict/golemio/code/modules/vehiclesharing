import { IStationInformation, IStationInformationsJson } from "#og/definitions";
import { removeEmptyOrNull } from "#og/helpers";
import { getLatestDataLiteral } from "#og/helpers/getLatestDataLiteral";
import { Vehiclesharing } from "#sch";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { IGBFSModels } from "./";
import { RentalAppModel } from "./RentalAppModel";

/**
 * Custom Postgres model for station information
 */
export class StationInformationModel extends SequelizeModel {
    private rentalAppModel: RentalAppModel | undefined;

    constructor() {
        super(
            Vehiclesharing.definitions.stationInformation.name + "Model",
            Vehiclesharing.definitions.stationInformation.pgTableName,
            Vehiclesharing.definitions.stationInformation.outputSequelizeAttributes,
            {
                schema: Vehiclesharing.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.rentalAppModel = models.RentalAppModel;

        this.sequelizeModel.belongsTo(models.RentalAppModel.sequelizeModel, {
            as: "rental_uris",
            targetKey: "id",
            foreignKey: "rental_app_id",
        });
    };

    async GetAll(systemId: string): Promise<IStationInformationsJson> {
        const results = await this.sequelizeModel.findAll({
            attributes: {
                include: [
                    ["id", "station_id"],
                    [Sequelize.literal("ST_Y(point)"), "lat"],
                    [Sequelize.literal("ST_X(point)"), "lon"],
                ],
                exclude: ["id", "system_id", "point", "rental_app_id", "processed_at"],
            },
            include: {
                as: "rental_uris",
                model: this.rentalAppModel?.sequelizeModel,
                attributes: [
                    ["android_discovery_url", "android"],
                    ["ios_discovery_url", "ios"],
                    ["web_url", "web"],
                ],
            },
            where: {
                system_id: systemId,
                [Sequelize.Op.and]: getLatestDataLiteral(this.sequelizeModel.tableName),
            },
            nest: true,
        });

        return {
            stations: results.map((res) => removeEmptyOrNull(res.toJSON())) as IStationInformation[],
        };
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
