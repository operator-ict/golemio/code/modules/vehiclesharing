import { IFreeBikeStatus, IFreeBikeStatusJson } from "#og/definitions";
import { removeEmptyOrNull } from "#og/helpers";
import { getLatestDataLiteral } from "#og/helpers/getLatestDataLiteral";
import { Vehiclesharing } from "#sch";
import { SequelizeModel, TGeoCoordinates } from "@golemio/core/dist/output-gateway";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { FindAttributeOptions, Op, QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { IGBFSModels } from "./";
import { PricingPlanModel } from "./PricingPlanModel";
import { RentalAppModel } from "./RentalAppModel";
import { SystemInformationModel } from "./SystemInformationModel";
import { VehicleTypesModel } from "./VehicleTypesModel";

export interface IVehicleStatus {
    id: string;
    station_id: number | null;
    description: string;
    name: string;
    res_url: string;
    company_name: string;
    company_web: string;
    type: string;
    type_id: string;
    last_reported: number;
    point: TGeoCoordinates;
}

const attributesToInclude: FindAttributeOptions = {
    include: [
        [Sequelize.literal(`"system_information"."name"`), "company_name"],
        [Sequelize.literal(`"system_information"."purchase_url"`), "company_web"],
        "id",
        "point",
        "description",
        [Sequelize.literal(`"vehicle_type"."id"`), "type_id"],
        [Sequelize.literal(`"vehicle_type"."name"`), "name"],
        [Sequelize.literal(`"vehicle_type"."form_factor"`), "type"],
    ],
    exclude: ["processed_at"],
};

/**
 * Custom Postgres model for shared bikes
 */
export class VehicleStatusModel extends SequelizeModel {
    private systemInformationModel: SystemInformationModel | undefined;
    private vehicleTypesModel: VehicleTypesModel | undefined;
    private rentalAppModel: RentalAppModel | undefined;
    private pricingPlanModel: PricingPlanModel | undefined;

    constructor() {
        super(
            Vehiclesharing.definitions.vehicleStatus.name + "Model",
            Vehiclesharing.definitions.vehicleStatus.pgTableName,
            Vehiclesharing.definitions.vehicleStatus.outputSequelizeAttributes,
            {
                schema: Vehiclesharing.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.systemInformationModel = models.SystemInformationModel;
        this.vehicleTypesModel = models.VehicleTypesModel;
        this.rentalAppModel = models.RentalAppModel;
        this.pricingPlanModel = models.PricingPlanModel;

        this.sequelizeModel.belongsTo(models.SystemInformationModel.sequelizeModel, {
            targetKey: "system_id",
            foreignKey: "system_id",
        });
        this.sequelizeModel.belongsTo(models.VehicleTypesModel.sequelizeModel, {
            foreignKey: "vehicle_type_id",
            as: "vehicle_type",
        });
        this.sequelizeModel.belongsTo(models.RentalAppModel.sequelizeModel, {
            as: "rental_uris",
            targetKey: "id",
            foreignKey: "rental_app_id",
        });
        this.sequelizeModel.belongsTo(models.PricingPlanModel.sequelizeModel, {
            targetKey: "system_id",
            foreignKey: "system_id",
        });
    };

    GetAll(options: {
        lat?: number;
        lng?: number;
        range?: number;
        limit?: number;
        offset?: number;
        updatedSince?: string;
        companyNames?: string[];
        vehicleTypes?: string[];
    }): Promise<IVehicleStatus[]> {
        const { lat, lng, range, limit, offset, updatedSince, companyNames, vehicleTypes } = options;
        return this.sequelizeModel.findAll({
            attributes: attributesToInclude,
            include: [
                {
                    as: "system_information",
                    model: this.systemInformationModel?.sequelizeModel,
                    ...(companyNames && companyNames.length
                        ? {
                              where: {
                                  name: {
                                      [Op.in]: companyNames,
                                  },
                              },
                          }
                        : {}),
                    attributes: [],
                },
                {
                    as: "vehicle_type",
                    model: this.vehicleTypesModel?.sequelizeModel,
                    where: {
                        form_factor: {
                            [Op.in]: vehicleTypes,
                        },
                    },
                    attributes: [],
                },
            ],
            where: {
                [Sequelize.Op.and]: [
                    ...(lat && lng && range
                        ? [Sequelize.literal(`ST_DWithin(point, 'POINT(${lng} ${lat})'::geography, ${range})`)]
                        : []),
                    ...(updatedSince
                        ? [
                              {
                                  last_reported: {
                                      [Sequelize.Op.gt]: updatedSince,
                                  },
                              },
                          ]
                        : []),
                    getLatestDataLiteral(this.sequelizeModel.tableName),
                ],
            },
            limit,
            offset,
            raw: true,
        });
    }

    GetOne(id: string): Promise<IVehicleStatus> {
        return this.sequelizeModel.findOne({
            attributes: attributesToInclude,
            include: [
                {
                    as: "system_information",
                    model: this.systemInformationModel?.sequelizeModel,
                    attributes: [],
                },
                {
                    as: "vehicle_type",
                    model: this.vehicleTypesModel?.sequelizeModel,
                    attributes: [],
                },
            ],
            where: { id },
            raw: true,
        });
    }

    async GetLatestLastReportedAsUnix(): Promise<{ last_reported: number }> {
        const lastUpdated = await this.sequelizeModel.findOne({
            attributes: [[Sequelize.literal("extract(epoch from date_trunc('second', last_reported))::int"), "last_reported"]],
            where: {
                last_reported: {
                    [Sequelize.Op.not]: null,
                },
            },
            order: [["last_reported", "DESC"]],
            limit: 1,
            raw: true,
        });

        if (!lastUpdated) {
            throw new GeneralError("Error getting last_reported", this.constructor.name, undefined, 404);
        }

        return lastUpdated;
    }

    async GetAllGTFS(systemId: string): Promise<IFreeBikeStatusJson> {
        const results = await this.sequelizeModel.findAll({
            attributes: {
                include: [
                    ["id", "bike_id"],
                    [Sequelize.literal("ST_Y(point)"), "lat"],
                    [Sequelize.literal("ST_X(point)"), "lon"],
                    [Sequelize.literal("extract(epoch from date_trunc('second', last_reported))::int"), "last_reported"],
                ],
                exclude: ["id", "point", "rental_app_id", "processed_at"],
            },
            include: [
                {
                    as: "rental_uris",
                    model: this.rentalAppModel?.sequelizeModel,
                    attributes: [
                        ["android_discovery_url", "android"],
                        ["ios_discovery_url", "ios"],
                        ["web_url", "web"],
                    ],
                },
                {
                    model: this.vehicleTypesModel?.sequelizeModel,
                    as: "vehicle_type",
                },
            ],
            where: {
                system_id: systemId,
                [Sequelize.Op.and]: getLatestDataLiteral(this.sequelizeModel.tableName),
            },
            nest: true,
        });

        return {
            bikes: results.map((res) => removeEmptyOrNull(res.toJSON())) as IFreeBikeStatus[],
        };
    }

    async GetAllMVTS(tileX: number, tileY: number, zoom: number, systemId: string[] | null = null): Promise<Uint8Array> {
        const result = await this.sequelizeModel.sequelize!.query<{ mvt: Uint8Array }>(
            `SELECT * FROM ${Vehiclesharing.pgSchema}.mvt_free_vehicle_status($zoom, $tilex, $tiley, $systemid);`,
            {
                type: QueryTypes.SELECT,
                bind: {
                    zoom,
                    tilex: tileX,
                    tiley: tileY,
                    systemid: systemId,
                },
            }
        );

        return result[0].mvt;
    }
}
