import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { Vehiclesharing } from "#sch";
import { IGBFSModels } from ".";

/**
 * Custom Postgres model for pricing
 */
export class PricingModel extends SequelizeModel {
    constructor() {
        super(
            Vehiclesharing.definitions.pricings.name + "Model",
            Vehiclesharing.definitions.pricings.pgTableName,
            Vehiclesharing.definitions.pricings.outputSequelizeAttributes,
            {
                schema: Vehiclesharing.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.sequelizeModel.belongsTo(models.PricingPlanModel.sequelizeModel, {
            foreignKey: "pricing_plan_id",
        });
    };

    async GetAll(options?: any): Promise<any[]> {
        throw new Error("Method not implemented.");
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
