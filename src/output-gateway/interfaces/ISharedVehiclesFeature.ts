import { TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";

export interface ISharedVehiclesFeature<T> {
    geometry: TGeoCoordinates;
    properties: T;
    type: string;
}

export interface ISharedBikesFeatureProperties {
    company: {
        email?: string;
        name: string;
        phone?: string;
        web: string;
    };
    estimated_trip_length_in_km?: number | null;
    id: string;
    in_rack?: boolean;
    label: string;
    location_note?: string;
    name?: string;
    res_url: string;
    type: {
        description?: string;
        id: number;
    };
    updated_at: number;
}

export interface ISharedCarsFeatureProperties {
    availability: {
        description: string;
        id: number;
    };
    company: {
        email?: string;
        name: string;
        phone?: string;
        web: string;
    };
    fuel: {
        description: string;
        id: number;
    };
    id: string;
    name?: string;
    res_url: string;
    updated_at: number;
}
