import { Vehiclesharing } from "#sch";
import Sequelize from "@golemio/core/dist/shared/sequelize";

/**
 * Get a Sequelize literal for filtering the latest data from a given table
 *
 * @param tableName The name of the table to get the latest data from
 * @param alias An optional alias used for the relation to the table to get the latest data from
 */
export function getLatestDataLiteral(tableName: string, alias?: string): Sequelize.Utils.Literal {
    const table = alias ?? tableName;
    return Sequelize.literal(
        `("${table}".processed_at, "${table}".system_id) IN (` +
            `SELECT max(t.processed_at), t.system_id` +
            ` FROM "${Vehiclesharing.pgSchema}"."${tableName}" t` +
            ` GROUP BY t.system_id)`
    );
}
