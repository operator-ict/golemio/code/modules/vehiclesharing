import { IPricing, ISystemPricingPlan } from "#og/definitions";
import { removeEmptyOrNull } from "#og/helpers/removeEmptyOrNull";

/**
 * Transform plan.pricings array to properties by pricing_type
 * @param plans pure result from database
 * @return ISystemPricingPlan[]
 */
export function buildPricingPlans(plans: any): ISystemPricingPlan[] {
    const results: ISystemPricingPlan[] = [];

    for (let plan of plans) {
        const pricingsToObject: { [type: string]: Array<IPricing | Partial<IPricing>> } = {};
        let { pricings, ...result } = plan.toJSON();
        for (const pricing of pricings) {
            const { pricing_plan_id, pricing_type, pricing_order, ...rest } = pricing;
            if (typeof pricingsToObject[pricing_type] === "undefined") {
                pricingsToObject[pricing_type] = [];
            }
            delete rest.id; // remove pricing id
            pricingsToObject[pricing_type].push(removeEmptyOrNull(rest));
            result = { ...result, ...pricingsToObject };
        }
        results.push(result);
    }

    return results;
}
