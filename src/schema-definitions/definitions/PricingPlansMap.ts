import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface IPricingPlanMap {
    system_id: string;
    pricing_plan_id: string;
    car_name_expression: string;
}

export const IPricingPlansMapJsonSchema: JSONSchemaType<IPricingPlanMap[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            system_id: {
                type: "string",
            },
            pricing_plan_id: {
                type: "string",
            },
            car_name_expression: {
                type: "string",
            },
        },
        required: ["system_id", "pricing_plan_id", "car_name_expression"],
        additionalProperties: false,
    },
};
