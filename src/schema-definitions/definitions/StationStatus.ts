import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { stationInformation } from "./StationInformation";

interface ISharedVehiclesStationStatusOutput {
    station_id: string;
    num_bikes_available: number;
    num_bikes_disabled: number | null;
    num_docks_available: number | null;
    is_installed: boolean | null;
    is_renting: boolean | null;
    is_returning: boolean | null;
    last_reported: string | null;
    processed_at?: Date;
    [audit: string]: unknown;
}

const outputSequelizeAttributes: ModelAttributes<any, ISharedVehiclesStationStatusOutput> = {
    station_id: {
        primaryKey: true,
        type: DataTypes.TEXT,
    },
    num_bikes_available: DataTypes.INTEGER,
    num_bikes_disabled: DataTypes.INTEGER,
    num_docks_available: DataTypes.INTEGER,
    is_installed: DataTypes.BOOLEAN,
    is_renting: DataTypes.BOOLEAN,
    is_returning: DataTypes.BOOLEAN,
    last_reported: DataTypes.DATE,
    processed_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },

    // Audit fields
    create_batch_id: DataTypes.BIGINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.STRING,
    update_batch_id: DataTypes.BIGINT,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.STRING,
};

const outputJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            station_id: {
                type: "string",
            },
            num_bikes_available: {
                type: ["null", "number"],
            },
            num_bikes_disabled: {
                type: ["null", "number"],
            },
            num_docks_available: {
                type: ["null", "number"],
            },
            is_installed: {
                type: ["null", "boolean"],
            },
            is_renting: {
                type: ["null", "boolean"],
            },
            is_returning: {
                type: ["null", "boolean"],
            },
            last_reported: {
                type: ["null", "string"],
            },
        },
    },
};

export const stationStatus = {
    name: "SharedVehiclesStationStatus",
    outputSequelizeAttributes,
    outputJsonSchema,
    pgTableName: "station_status",
};

const stationStatusWithSystemIdSequelizeAttributes = {
    system_id: stationInformation.outputSequelizeAttributes["system_id"],
    ...outputSequelizeAttributes,
};

export const stationStatusWithSystemId = {
    name: "SharedVehiclesStationStatusWithSystemId",
    outputSequelizeAttributes: stationStatusWithSystemIdSequelizeAttributes,
    pgTableName: "v_station_status_with_system_id",
};

export type { ISharedVehiclesStationStatusOutput };
