import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

interface ISharedVehiclesRentalAppOutput {
    id: string;
    android_store_url: string | null;
    android_discovery_url: string | null;
    ios_store_url: string | null;
    ios_discovery_url: string | null;
    web_url: string | null;
    [audit: string]: unknown;
}

const outputSequelizeAttributes: ModelAttributes<any, ISharedVehiclesRentalAppOutput> = {
    id: {
        type: DataTypes.STRING(50),
        primaryKey: true,
    },
    android_store_url: DataTypes.TEXT,
    android_discovery_url: DataTypes.TEXT,
    ios_store_url: DataTypes.TEXT,
    ios_discovery_url: DataTypes.TEXT,
    web_url: DataTypes.TEXT,

    // Audit fields
    create_batch_id: DataTypes.BIGINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.STRING,
    update_batch_id: DataTypes.BIGINT,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.STRING,
};

const outputJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: {
                type: "string",
            },
            android_store_url: {
                type: ["null", "string"],
            },
            android_discovery_url: {
                type: ["null", "string"],
            },
            ios_store_url: {
                type: ["null", "string"],
            },
            ios_discovery_url: {
                type: ["null", "string"],
            },
            web_url: {
                type: ["null", "string"],
            },
        },
    },
};

export const rentalApps = {
    name: "SharedBikesRentalApps",
    outputSequelizeAttributes,
    outputJsonSchema,
    pgTableName: "rental_apps",
};

export type { ISharedVehiclesRentalAppOutput };
