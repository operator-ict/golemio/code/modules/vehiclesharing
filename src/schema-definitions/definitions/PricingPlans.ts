import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

interface ISharedVehiclesPricingPlanOutput {
    id: string;
    system_id: string;
    url: string | null;
    last_updated: string;
    name: string;
    currency: string;
    price: number;
    is_taxable: boolean;
    description: string;
    surge_pricing: boolean | null;
    [audit: string]: unknown;
}

const outputSequelizeAttributes: ModelAttributes<any, ISharedVehiclesPricingPlanOutput> = {
    id: {
        type: DataTypes.STRING(50),
        primaryKey: true,
    },
    system_id: DataTypes.STRING(50),
    url: DataTypes.TEXT,
    last_updated: DataTypes.DATE,
    name: DataTypes.TEXT,
    currency: DataTypes.STRING(8),
    price: DataTypes.FLOAT,
    is_taxable: DataTypes.BOOLEAN,
    description: DataTypes.TEXT,
    surge_pricing: DataTypes.BOOLEAN,

    // Audit fields
    create_batch_id: DataTypes.BIGINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.STRING,
    update_batch_id: DataTypes.BIGINT,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.STRING,
};

const outputJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: {
                type: "string",
            },
            system_id: {
                type: "string",
            },
            url: {
                type: ["null", "string"],
            },
            last_updated: {
                type: "string",
            },
            name: {
                type: "string",
            },
            currency: {
                type: "string",
            },
            price: {
                type: "number",
            },
            is_taxable: {
                type: "boolean",
            },
            description: {
                type: "string",
            },
            surge_pricing: {
                type: ["null", "boolean"],
            },
        },
    },
};

export const pricingPlans = {
    name: "SharedVehiclesPricingPlans",
    outputSequelizeAttributes,
    outputJsonSchema,
    pgTableName: "pricing_plans",
};

export type { ISharedVehiclesPricingPlanOutput };
