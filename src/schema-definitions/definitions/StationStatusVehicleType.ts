import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { stationInformation } from "./StationInformation";

interface ISharedVehiclesStationStatusVehicleTypeOutput {
    station_id: string;
    vehicle_type_id: string;
    count: number | null;
    num_bikes_available: number;
    num_bikes_disabled: number | null;
    vehicle_docks_available: number | null;
    processed_at?: Date;
    [audit: string]: unknown;
}

const outputSequelizeAttributes: ModelAttributes<any, ISharedVehiclesStationStatusVehicleTypeOutput> = {
    station_id: {
        type: DataTypes.TEXT,
        primaryKey: true,
    },
    vehicle_type_id: {
        type: DataTypes.TEXT,
        primaryKey: true,
    },
    count: DataTypes.INTEGER,
    num_bikes_available: DataTypes.INTEGER,
    num_bikes_disabled: DataTypes.INTEGER,
    vehicle_docks_available: DataTypes.INTEGER,
    processed_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },

    // Audit fields
    create_batch_id: DataTypes.BIGINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.STRING,
    update_batch_id: DataTypes.BIGINT,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.STRING,
};

const outputJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            station_id: {
                type: "string",
            },
            vehicle_type_id: {
                type: "string",
            },
            count: {
                type: ["null", "number"],
            },
            num_bikes_available: {
                type: "number",
            },
            num_bikes_disabled: {
                type: ["null", "number"],
            },
            vehicle_docks_available: {
                type: ["null", "number"],
            },
        },
    },
};

export const stationsStatusVehicleType = {
    name: "SharedVehiclesStationStatusVehicleType",
    outputSequelizeAttributes,
    outputJsonSchema,
    pgTableName: "station_status_vehicle_type",
};

const stationStatusVehicleTypeWithSystemIdSequelizeAttributes = {
    system_id: stationInformation.outputSequelizeAttributes["system_id"],
    ...outputSequelizeAttributes,
};

export const stationStatusVehicleTypeWithSystemId = {
    name: "SharedVehiclesStationStatusVehicleTypeWithSystemId",
    outputSequelizeAttributes: stationStatusVehicleTypeWithSystemIdSequelizeAttributes,
    pgTableName: "v_station_status_vehicle_type_with_system_id",
};

export type { ISharedVehiclesStationStatusVehicleTypeOutput };
