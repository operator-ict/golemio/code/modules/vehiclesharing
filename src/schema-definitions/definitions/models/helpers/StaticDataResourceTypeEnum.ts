export enum StaticDataResourceType {
    PricingPlans = "pricingPlans",
    Pricings = "pricings",
    RentalApps = "rentalApps",
    SystemInformation = "systemInformation",
    VehicleTypes = "vehicleTypes",
    PricingPlanMap = "pricingPlanMap",
}
