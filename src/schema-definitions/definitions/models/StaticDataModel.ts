import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { StaticDataProvider } from "./helpers/StaticDataProviderEnum";
import { StaticDataResourceType } from "./helpers/StaticDataResourceTypeEnum";
import { IStaticDataOutputDto, ResourceTypeDict } from "./interfaces/IStaticDataOutputDto";

export class StaticDataModel extends Model<IStaticDataOutputDto> implements IStaticDataOutputDto {
    declare provider: StaticDataProvider;
    declare resource_type: StaticDataResourceType;
    declare data: ResourceTypeDict[StaticDataResourceType];

    public static attributeModel: ModelAttributes<StaticDataModel, IStaticDataOutputDto> = {
        provider: {
            type: DataTypes.STRING(30),
            primaryKey: true,
        },
        resource_type: {
            type: DataTypes.STRING(30),
            primaryKey: true,
        },
        data: {
            type: DataTypes.JSON,
            allowNull: false,
        },
    };

    public static jsonSchema: JSONSchemaType<IStaticDataOutputDto> = {
        type: "object",
        properties: {
            provider: { type: "string" },
            resource_type: { type: "string" },
            data: {
                type: "array",
                items: { type: "object" },
            },
        },
        required: ["provider", "resource_type", "data"],
    };
}
