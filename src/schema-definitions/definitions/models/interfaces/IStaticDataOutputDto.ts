import { ISharedVehiclesPricingPlanOutput } from "#sch/definitions/PricingPlans";
import { ISharedVehiclesPricingOutput } from "#sch/definitions/Pricings";
import { ISharedVehiclesRentalAppOutput } from "#sch/definitions/RentalApps";
import { ISharedVehiclesSystemInformationOutput } from "#sch/definitions/SystemInformation";
import { ISharedVehicleTypeOutput } from "#sch/definitions/VehicleTypes";
import { StaticDataProvider } from "../helpers/StaticDataProviderEnum";
import { StaticDataResourceType } from "../helpers/StaticDataResourceTypeEnum";
import { IPricingPlanMap } from "#sch/definitions/PricingPlansMap";

export interface IStaticDataOutputDto {
    provider: StaticDataProvider;
    resource_type: StaticDataResourceType;
    data: ResourceTypeDict[StaticDataResourceType];
}

export type ResourceTypeDict = {
    [StaticDataResourceType.PricingPlans]: ISharedVehiclesPricingPlanOutput[];
    [StaticDataResourceType.Pricings]: ISharedVehiclesPricingOutput[];
    [StaticDataResourceType.RentalApps]: ISharedVehiclesRentalAppOutput[];
    [StaticDataResourceType.SystemInformation]: ISharedVehiclesSystemInformationOutput[];
    [StaticDataResourceType.VehicleTypes]: ISharedVehicleTypeOutput[];
    [StaticDataResourceType.PricingPlanMap]: IPricingPlanMap[];
};

export type ResourceTypeMappedDto = {
    [s: string]: ResourceTypeDict[StaticDataResourceType];
};
