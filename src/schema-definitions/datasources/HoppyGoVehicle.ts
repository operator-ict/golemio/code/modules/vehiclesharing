export interface IHoppyGoVehicle {
    id: number;
    hash_code: string;
    url: string;
    vehicle_hash_code: string;
    manufacturer_name: string;
    model_name: string;
    fuel_description: string;
    price_per_day: number;
    localization: string;
    photo_hash_code: string;
    photo_size: number;
    photo_mime: string;
    average_rating: number;
}
