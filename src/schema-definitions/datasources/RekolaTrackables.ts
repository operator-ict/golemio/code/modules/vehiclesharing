import { RekolaGeoPoint, geoPointJsonSchema } from "./shared";

const vehicleJsonSchema = {
    type: "object",
    properties: {
        id: {
            type: "number",
        },
        type: {
            type: "string",
        },
        isVisible: {
            type: "boolean",
        },
        isBorrowed: {
            type: "boolean",
        },
        isBorrowedByMe: {
            type: "boolean",
        },
        name: {
            type: "string",
        },
        label: {
            type: "string",
        },
        position: geoPointJsonSchema,
        positionNote: {
            type: ["null", "string"],
        },
        pin: {
            type: "object",
        },
    },
};

export const rekolaTrackables = {
    name: "SharedBikesRekolaTrackablesDataSource",
    jsonSchema: {
        type: "object",
        properties: {
            isDiff: {
                type: "boolean",
            },
            racks: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        id: {
                            type: "number",
                        },
                        is_visible: {
                            type: "boolean",
                        },
                        name: {
                            type: "string",
                        },
                        position: geoPointJsonSchema,
                        pin: {
                            type: "object",
                        },
                        vehicles: {
                            type: "array",
                            items: vehicleJsonSchema,
                        },
                    },
                },
            },
            vehicles: {
                type: "array",
                items: vehicleJsonSchema,
            },
        },
    },
};

export interface IRekolaTrackablesDatasourceVehicle {
    id: number;
    type: string;
    isVisible: boolean;
    isBorrowed: boolean;
    isBorrowedByMe: boolean;
    name: string;
    label: string;
    position: RekolaGeoPoint;
    positionNote: string | null;
    pin: Record<string, any>;
}

export interface IRekolaTrackablesDatasourceRack {
    id: number;
    isVisible: boolean;
    name: string;
    position: RekolaGeoPoint;
    pin: Record<string, any>;
    vehicles: IRekolaTrackablesDatasourceVehicle[];
}

export interface IRekolaTrackablesDatasource {
    isDiff: boolean;
    racks: IRekolaTrackablesDatasourceRack[];
    parkings: IRekolaTrackablesDatasourceRack[];
    vehicles: IRekolaTrackablesDatasourceVehicle[];
}
