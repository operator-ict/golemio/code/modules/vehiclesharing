import { IHoppyGoVehicle } from "#sch/datasources/HoppyGoVehicle";
import { ISchemaDefinition } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";

export const HoppyGoVehiclesJsonSchema: ISchemaDefinition<IHoppyGoVehicle[]> = {
    name: "HoppyGoVehiclesDataSource",
    jsonSchema: {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: {
                    type: "number",
                },
                hash_code: {
                    type: "string",
                },
                url: {
                    type: "string",
                },
                vehicle_hash_code: {
                    type: "string",
                },
                manufacturer_name: {
                    type: "string",
                },
                model_name: {
                    type: "string",
                },
                fuel_description: {
                    type: "string",
                },
                price_per_day: {
                    type: "number",
                },
                localization: {
                    type: "string",
                },
                photo_hash_code: {
                    type: "string",
                },
                photo_size: {
                    type: "number",
                },
                photo_mime: {
                    type: "string",
                },
                average_rating: {
                    type: "number",
                },
            },
            required: ["id"],
            additionalProperties: true,
        },
    },
};
