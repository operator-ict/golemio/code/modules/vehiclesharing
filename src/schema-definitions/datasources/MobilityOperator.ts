import { ISharedVehiclesRentalAppOutput } from "#sch";

export interface IMobilityOperator {
    id: string;
    name: string;
    isOnDemand: boolean | null;
    isParkingProvider: boolean;
    transportModes: string[];
    system_id: string | null;
    androidAppUrl: string | null;
    androidDiscoveryUrl: string | null;
    androidPackageName: string | null;
    iosAppUrl: string | null;
    iosDiscoveryUrl: string | null;
    iosPackageName: string | null;
    webUrl: string | null;
    pricingUrl: string | null;
    termsOfUseUrl: string | null;
    paymentMethods: string[];
    email: string | null;
    orderByPhone: string | null;
    color: string | null;
}

export interface IMobilityOperatorRentalAppMap {
    [operator: string]: ISharedVehiclesRentalAppOutput;
}
