import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { ISchemaDefinition } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";

export interface INextbikeVehicleType {
    vehicle_type_id: string;
    form_factor: string;
    propulsion_type: string;
    max_range_meters?: number | null;
    name: string;
}

export interface INextbikeVehicleTypeInput {
    last_updated: number;
    ttl: number;
    version: string;
    data: {
        vehicle_types: INextbikeVehicleType[];
    };
}

const VehicleTypeJsonSchema: JSONSchemaType<INextbikeVehicleType> = {
    type: "object",
    properties: {
        vehicle_type_id: {
            type: "string",
        },
        form_factor: {
            type: "string",
        },
        propulsion_type: {
            type: "string",
        },
        max_range_meters: {
            type: "number",
            nullable: true,
        },
        name: {
            type: ["string"],
        },
    },
    required: ["vehicle_type_id", "form_factor", "propulsion_type", "name"],
    additionalProperties: true,
};

export const nextbikeVehicleTypeJsonSchema: ISchemaDefinition<INextbikeVehicleTypeInput> = {
    name: "SharedBikesNextbikeVehicleTypeDataSource",
    jsonSchema: {
        type: "object",
        properties: {
            last_updated: {
                type: "number",
            },
            ttl: {
                type: "number",
            },
            version: {
                type: "string",
            },
            data: {
                type: "object",
                properties: {
                    vehicle_types: {
                        type: "array",
                        items: VehicleTypeJsonSchema,
                    },
                },
                required: ["vehicle_types"],
                additionalProperties: false,
            },
        },
        required: ["last_updated", "ttl", "version", "data"],
        additionalProperties: false,
    },
};
