import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { ISchemaDefinition } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";

export interface IVehicleTypesAvailableItem {
    vehicle_type_id: string;
    count: number;
}

export interface INextbikeStationStatus {
    station_id: string;
    num_bikes_available: number;
    vehicle_types_available: IVehicleTypesAvailableItem[] | null;
    num_docks_available: number;
    is_installed: boolean;
    is_renting: boolean;
    is_returning: boolean;
    last_reported: number;
}

export interface INextbikeStationStatusInput {
    last_updated: number;
    ttl: number;
    version: string;
    data: {
        stations: INextbikeStationStatus[];
    };
}

const StationStatusJsonSchema: JSONSchemaType<INextbikeStationStatus> = {
    type: "object",
    properties: {
        station_id: {
            type: "string",
        },
        num_bikes_available: {
            type: "number",
        },
        vehicle_types_available: {
            description: "Array of objects displaying the total number of each vehicle type at the station (added in v2.1-RC).",
            oneOf: [
                { type: "null", nullable: true },
                {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            vehicle_type_id: {
                                description: "The vehicle_type_id of vehicle at the station (added in v2.1-RC).",
                                type: "string",
                            },
                            count: {
                                description:
                                    // eslint-disable-next-line max-len
                                    "A number representing the total amount of this vehicle type at the station (added in v2.1-RC).",
                                type: "integer",
                                minimum: 0,
                            },
                        },
                        required: ["vehicle_type_id", "count"],
                    },
                },
            ],
        },
        num_docks_available: {
            type: "number",
        },
        is_installed: {
            type: "boolean",
        },
        is_renting: {
            type: "boolean",
        },
        is_returning: {
            type: "boolean",
        },
        last_reported: {
            type: "number",
        },
    },
    required: [
        "station_id",
        "num_bikes_available",
        "num_docks_available",
        "is_installed",
        "is_renting",
        "is_returning",
        "last_reported",
    ],
    additionalProperties: false,
};

export const nextbikeStationStatusJsonSchema: ISchemaDefinition<INextbikeStationStatusInput> = {
    name: "SharedBikesNextbikeStationStatusDataSource",
    jsonSchema: {
        type: "object",
        properties: {
            last_updated: {
                type: "number",
            },
            ttl: {
                type: "number",
            },
            version: {
                type: "string",
            },
            data: {
                type: "object",
                properties: {
                    stations: {
                        type: "array",
                        items: StationStatusJsonSchema,
                    },
                },
                required: ["stations"],
                additionalProperties: false,
            },
        },
        required: ["last_updated", "ttl", "version", "data"],
        additionalProperties: false,
    },
};
