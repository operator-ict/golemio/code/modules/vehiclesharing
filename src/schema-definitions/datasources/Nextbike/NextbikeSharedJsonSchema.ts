import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface ISchemaDefinition<T> {
    name: string;
    jsonSchema: JSONSchemaType<T>;
}

export interface INextbikeRentalUris {
    android: string;
    ios: string;
    web: string;
}

export const NextbikeRentalUrisJsonSchema: JSONSchemaType<INextbikeRentalUris> = {
    type: "object",
    properties: {
        android: {
            type: "string",
        },
        ios: {
            type: "string",
        },
        web: {
            type: "string",
        },
    },
    required: ["android", "ios", "web"],
    additionalProperties: false,
};
