const CeskyCarsharingPolygonSchema = {
    company_id: {
        type: "number",
    },
    polygon: {
        type: "array",
        items: {
            type: "object",
            properties: {
                lat: {
                    type: "string",
                },
                lon: {
                    type: "string",
                },
            },
        },
    },
};

export const CeskyCarsharingGeofencingJsonSchema = {
    name: "CeskyCarsharingGeofencingDataSource",
    jsonSchema: {
        type: "object",
        properties: {
            message: {
                type: "string",
            },
            status: {
                type: "number",
            },
            polygons: {
                type: "array",
                items: {
                    type: "object",
                    properties: CeskyCarsharingPolygonSchema,
                },
            },
        },
    },
};
