export interface ICeskyCarsharingCar {
    car_name: string;
    company_email: string;
    company_id: number;
    company_name: string;
    company_phone: string;
    company_web: string;
    fuel: number;
    fuel_type: string;
    latitude: number;
    longitude: number;
    res_url: string;
    rz: string;
    status: string;
}

export interface ICeskyCarsharingVehicles {
    message: string;
    status: number;
    cars: ICeskyCarsharingCar[];
}
