export interface ICeskyCarsharingPolygonPoint {
    lat: string;
    lon: string;
}

export interface ICeskyCarsharingPolygon {
    company_id: number;
    polygon: ICeskyCarsharingPolygonPoint[];
}

export interface ICeskyCarsharingGeofencingJson {
    message: string;
    status: number;
    polygons: ICeskyCarsharingPolygon[];
}
