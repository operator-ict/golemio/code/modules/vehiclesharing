import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

// liberal schema for static data - will be validated later when used in DB
export const iptBlobStorageJsonSchema: JSONSchemaType<object | object[]> = {
    oneOf: [
        {
            type: "object",
        },
        {
            type: "array",
            items: {
                type: "object",
            },
        },
    ],
};
