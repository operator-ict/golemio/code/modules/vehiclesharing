export const geoPointJsonSchema = {
    type: "object",
    properties: {
        lat: {
            type: "number",
        },
        lng: {
            type: "number",
        },
    },
};

export const BasicTypes = {
    array: "array",
    boolean: "boolean",
    number: "number",
    null: "null",
    object: "object",
    string: "string",
    url: "string",
};

export type RekolaGeoPoint = {
    lat: number;
    lng: number;
};
