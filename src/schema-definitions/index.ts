import "@golemio/core/dist/shared/sequelize"; // Workaround for inferred type of forExport
import {
    rekolaGeofencingZones,
    rekolaTrackables,
    nextbikeDefinitionJsonSchema,
    nextbikeSystemInfoJsonSchema,
    nextbikeFreeBikeStatusJsonSchema,
    nextbikeStationInfoJsonSchema,
    nextbikeStationStatusJsonSchema,
    nextbikeSystemPricingPlansJsonSchema,
    nextbikeVehicleTypeJsonSchema,
    HoppyGoVehiclesJsonSchema,
    CeskyCarsharingGeofencingJsonSchema,
    CeskyCarsharingVehiclesJsonSchema,
} from "./datasources";
import {
    systemInformation,
    rentalApps,
    geofencingZones,
    pricingPlans,
    pricings,
    stationInformation,
    stationStatus,
    stationStatusWithSystemId,
    vehicleTypes,
    stationsStatusVehicleType,
    stationStatusVehicleTypeWithSystemId,
    vehicleStatus,
} from "./definitions";
import { MobilityOperatorJsonSchema } from "#sch/datasources/MobilityOperatorJsonSchema";

const nextbikeDataSources = {
    nextbikeDefinitionJsonSchema,
    nextbikeFreeBikeStatusJsonSchema,
    nextbikeStationInfoJsonSchema,
    nextbikeStationStatusJsonSchema,
    nextbikeSystemInfoJsonSchema,
    nextbikeSystemPricingPlansJsonSchema,
    nextbikeVehicleTypeJsonSchema,
};

export type TNextbikeDataSources = keyof typeof nextbikeDataSources;

const forExport = {
    name: "vehiclesharing",
    pgSchema: "vehiclesharing",
    datasources: {
        rekolaGeofencingZones,
        rekolaTrackables,
        ...nextbikeDataSources,
        HoppyGoVehiclesJsonSchema,
        CeskyCarsharingGeofencingJsonSchema,
        CeskyCarsharingVehiclesJsonSchema,
        MobilityOperatorJsonSchema,
    },
    definitions: {
        systemInformation,
        rentalApps,
        geofencingZones,
        pricingPlans,
        pricings,
        stationInformation,
        stationStatus,
        stationStatusWithSystemId,
        vehicleTypes,
        stationsStatusVehicleType,
        stationStatusVehicleTypeWithSystemId,
        vehicleStatus,
    },
};

export { forExport as Vehiclesharing };

export type {
    IRekolaGeofencingDatasourceVehicleType,
    IRekolaGeofencingDatasourceZone,
    IRekolaGeofencingDatasourceItem,
    IRekolaTrackablesDatasourceRack,
    IRekolaTrackablesDatasourceVehicle,
    IRekolaTrackablesDatasource,
} from "./datasources";

export type {
    ISharedVehiclesSystemInformationOutput,
    ISharedVehiclesRentalAppOutput,
    ISharedVehiclesGeofencingZoneOutput,
    ISharedVehiclesPricingPlanOutput,
    ISharedVehiclesPricingOutput,
    ISharedVehiclesStationInformationOutput,
    ISharedVehiclesStationStatusOutput,
    ISharedVehicleTypeOutput,
    ISharedVehiclesStationStatusVehicleTypeOutput,
    ISharedVehicleStatusOutput,
} from "./definitions";
